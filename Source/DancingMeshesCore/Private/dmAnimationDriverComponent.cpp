// Copyright Alexander Demets

#include "dmAnimationDriverComponent.h"

//#include "dm_ozz.h"
#include "dm_core.h"
#include "dm_logging.h"

#include "ozz/base/maths/soa_transform.h"
#include "ozz/base/maths/soa_float4x4.h"
#include "ozz/animation/runtime/skeleton.h"
#include "ozz/animation/runtime/animation.h"

DECLARE_CYCLE_STAT(TEXT("DancingMeshes ~ dmAnimationDriverComponent::Update"), STAT_AnimationUpdate, STATGROUP_DancingMeshes);
DECLARE_CYCLE_STAT(TEXT("DancingMeshes ~ dmAnimationDriverComponent::AnimationUpdateOnUpdate"), STAT_AnimationUpdateOnUpdate, STATGROUP_DancingMeshes);
DECLARE_CYCLE_STAT(TEXT("DancingMeshes ~ dmAnimationDriverComponent::AnimationUpdateMatrices"), STAT_AnimationUpdateMatrices, STATGROUP_DancingMeshes);
DECLARE_CYCLE_STAT(TEXT("DancingMeshes ~ dmAnimationDriverComponent::AnimationUpdateUpdateSkinndedMeshes"), STAT_AnimationUpdateUpdateSkinndedMeshes, STATGROUP_DancingMeshes);
DECLARE_CYCLE_STAT(TEXT("DancingMeshes ~ dmAnimationDriverComponent::OzzUpdatePlusInstUpdate"), STAT_OzzUpdatePlusInstUpdate, STATGROUP_DancingMeshes);
DECLARE_CYCLE_STAT(TEXT("DancingMeshes ~ dmAnimationDriverComponent::InstUpdate"), STAT_InstUpdate, STATGROUP_DancingMeshes);


// Sets default values for this component's properties
UdmAnimationDriverComponent::UdmAnimationDriverComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UdmAnimationDriverComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UdmAnimationDriverComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (is_playing && auto_update) {
		Update(DeltaTime, reskin_mesh);
	}
}

void UdmAnimationDriverComponent::Setup(UdmSceneDriverComponent* SceneDriver_, bool AutoUpdate)
{
	dm_log_msg("animation driver setup start.");
	Reset();
	//this->Settings = SceneDriver_->GetSceneSettings();
	Settings.Scene = SceneDriver_->GetScene();
	Settings.BaseActor = SceneDriver_->GetOwner();
	Settings.RootComponent = SceneDriver_->GetRoot();

	auto_update = AutoUpdate;

	//this->SceneDriver = (UdmSceneDriverComponent*)Settings.BaseActor->GetComponentByClass(UdmSceneDriverComponent::StaticClass());
	SceneDriver = SceneDriver_;

	dm_log_msg("animation driver: scene to ozz.");
	//this->Settings.Scene;
	if (dm_scene_has_animations(Settings.Scene) && dm_scene_has_skins(Settings.Scene)) {
		ozz_data = dm_ozz_init({});
		dm_scene_to_ozz(this->Settings.Scene, ozz_data);
		//dm_ozz_from_scene(ozz_data, this->Settings.Scene);
	}
	dm_log_msg("animation driver end");
}

UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void UdmAnimationDriverComponent::Reset()
{
	time_ratio = 0.0f;
	previous_time_ratio = 0.0f;
	playback_speed = 1.0f;
	is_playing = false;
	auto_update = true;
	dm_ozz_free(ozz_data);
}

UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void UdmAnimationDriverComponent::Update(float dt, bool ReskinMesh)
{
	SCOPE_CYCLE_COUNTER(STAT_AnimationUpdate);
	//dm_log_msg("Entering UdmAnimationDriverComponent::Update... (ReskinMesh: " + dm_string_from_bool(ReskinMesh));

	{
		SCOPE_CYCLE_COUNTER(STAT_AnimationUpdateOnUpdate);
		float new_time_ratio = time_ratio;
		if (is_playing) {
			new_time_ratio = time_ratio + dt * playback_speed / (anim->length / anim->frames_per_second);
		}
		if (this->anim && anim->_ozz_animation && anim->_ozz_skeleton) {
			on_update(time_ratio, *(ozz::animation::Skeleton*)(anim->_ozz_skeleton),
				*(ozz::animation::Animation*)(anim->_ozz_animation), cache_, locals_, models_);
		}
		SetTimeRatio(new_time_ratio);
	}
	{
		SCOPE_CYCLE_COUNTER(STAT_AnimationUpdateMatrices);
	// update viewport
	//joint_locations
		int i = 0;
		for (auto m : models_) {
			FMatrix matrix = internal::dm_mat4_from_ozz(m);
			joint_locations[i].SetFromMatrix(matrix); // = FTransform::Matri//FTransform(matrix);
			//joint_locations[i].ScaleTranslation(100.0f);
			//joint_locations[i].SetLocation(joint_locations[i].GetLocation() + FMath::VRand() * 10.0f);
			++i;
		}
	}
	{
		SCOPE_CYCLE_COUNTER(STAT_AnimationUpdateUpdateSkinndedMeshes);
		//ozz::animation::Skeleton* sk = (ozz::animation::Skeleton*)anim->_ozz_skeleton;
		//update_skinned_mesh()
		if (!ReskinMesh)
			return;

		TArray<UdmInstanceComponent*> Instances = SceneDriver->GetInstanceComponents();
		for (auto Inst : Instances) {
			if ((Inst->InstanceNode->instance->mesh != nullptr) && (Inst->InstanceNode->skin != nullptr)) {
				auto instance_mesh = Inst->InstanceNode->instance->mesh;
				//dm_log_msg("Instance: " + instance_mesh->name);
				TArray<TArray<FVector>> Vertices;
				Vertices.SetNum(instance_mesh->primitives.Num());
				int section = 0;
				for (auto& v : Vertices) {
					v.SetNum(instance_mesh->primitives[section].vertices.Num());
					++section;
				}
				dm_mesh m;
				//dm_checkf(this->anim, dm_text("this->anim == nullptr"));
				//dm_checkf(anim->_ozz_skeleton, dm_text("anim->_ozz_skeleton == nullptr"));
				//dm_checkf(anim->_ozz_animation, dm_text("anim->_ozz_animation == nullptr"));
				if (this->anim && anim->_ozz_animation && anim->_ozz_skeleton) {
					//dm_log_msg("before update.. but with valid data.");
					SCOPE_CYCLE_COUNTER(STAT_OzzUpdatePlusInstUpdate);
					if (update_skinned_mesh((ozz::animation::Skeleton*)anim->_ozz_skeleton, Settings.Scene, *instance_mesh, models_, Vertices, m)) {
						//dm_log_msg("update_skinned_mesh was successfull... now updating ue4 mesh");
						SCOPE_CYCLE_COUNTER(STAT_InstUpdate);
						Inst->UpdateMesh(Vertices);
					}
				}
			}
		}
	}
}

UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void UdmAnimationDriverComponent::Play(FString AnimationId)
{
	anim = dm_scene_find_animation(Settings.Scene, AnimationId);
	
	ozz::animation::Skeleton* skeleton = (ozz::animation::Skeleton*)anim->_ozz_skeleton;
	locals_.SetNum(skeleton->num_soa_joints());
	models_.SetNum(skeleton->num_joints());
	cache_.Resize(skeleton->num_joints());

	joint_locations.SetNum(skeleton->num_joints());
	is_playing = true;
}

UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void UdmAnimationDriverComponent::PlayByIndex(int AnimationId)
{
	dm_log_msg("entering UdmAnimationDriverComponent::PlayByIndex...");
	if (AnimationId >= Settings.Scene->animations.Num())
		return;

	anim = Settings.Scene->animations[AnimationId];
	if (anim == nullptr)
		return;

	dm_log_msg("accessing skeleton. (reserving memory.)");
	ozz::animation::Skeleton* skeleton = (ozz::animation::Skeleton*)anim->_ozz_skeleton;
	if (skeleton == nullptr) {
		dm_log_msg("skeleton not valid.");
		return;
	}
	dm_log_msg("soa joints");
	locals_.SetNum(skeleton->num_soa_joints());
	dm_log_msg("num_joints");
	models_.SetNum(skeleton->num_joints());
	dm_log_msg("num_joints cache");
	cache_.Resize(skeleton->num_joints());
	dm_log_msg("num joints joint locations");
	joint_locations.SetNum(skeleton->num_joints());
	is_playing = true;
}

UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void UdmAnimationDriverComponent::Stop(bool ResetTime)
{
	is_playing = false;
	if (ResetTime) {
		SetTimeRatio(0.0f);
	}
}

UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void UdmAnimationDriverComponent::SetTimeRatio(float Time)
{
	previous_time_ratio = time_ratio;
	if (is_looping) {
		// Wraps in the unit interval [0:1], even for negative values (the reason
		// for using floorf).
		time_ratio =  Time - floorf(Time);
	}
	else {
		// Clamps in the unit interval [0:1].
		time_ratio = FMath::Clamp(Time, 0.0f, 1.0f);
	}
}

