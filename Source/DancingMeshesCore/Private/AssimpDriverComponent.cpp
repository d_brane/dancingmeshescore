// Copyright Alexander Demets


#include "AssimpDriverComponent.h"

#include "dmSceneConstructor.h"

#include "dm_assimp.h"
#include "dm_unreal_helper.h"
#include "dm_scene_io.h"

//USceneComponent* SpawnNewComponentA(AActor* Owner, USceneComponent* ParentComponent, UClass* ComponentClassToSpawn, const FTransform& SpawnLocation, const FString& ComponentName)
//{
//	//Make sure it is a scene component since we are going to attach it to our root component  
//	check(ComponentClassToSpawn->IsChildOf(USceneComponent::StaticClass()));
//
//	// Construct an object of the class passed to us but return a point to USceneComponent  
//	USceneComponent* Component = NewObject<USceneComponent>(Owner, ComponentClassToSpawn, (*ComponentName));
//
//	//Attach the component and position it at the location given  
//	Component->SetupAttachment(ParentComponent);
//	Component->SetRelativeTransform(SpawnLocation);
//	//Component->SetWorldTransform(SpawnLocation);
//	Component->RegisterComponent();
//
//	return Component;
//}

// Sets default values for this component's properties
UAssimpDriverComponent::UAssimpDriverComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	//USceneComponent* ParentComponent = nullptr;
	//if (ParentMeshComponent != nullptr) {
	//	ParentComponent = ParentMeshComponent;
	//}
	//MeshComponent = Cast<UProceduralMeshComponent>(SpawnNewComponentA(this->GetOwner(), ParentComponent, UProceduralMeshComponent::StaticClass(), FTransform::Identity, "Myyy Maesh"));
}


// Called when the game starts
void UAssimpDriverComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UAssimpDriverComponent::processNode(aiNode* Node, const aiScene* Scene)
{
	for (uint32 i = 0; i < Node->mNumMeshes; i++) {

		//node* n = internal::dm_node_create_from_assimp(Node);
		//dm_scene_add_node(&_scene, n);

		aiMesh* mesh = Scene->mMeshes[Node->mMeshes[i]];
		processMesh(mesh, Scene);
		
		if (mesh->HasBones()) {
			processBones(mesh, Scene);
		}
		++_meshCurrentlyProcessed;
	}
	uint32 nodes = Node->mNumMeshes;
	// do the same for all of its children
	for (uint32 i = 0; i < Node->mNumChildren; i++) {
		processNode(Node->mChildren[i], Scene);
	}
}

void UAssimpDriverComponent::postprocessNodes(const aiScene* Scene)
{
	internal::dm_scene_from_assimp(Scene, &_scene);
	FString filepath = FPaths::Combine(*FPaths::ProjectDir(), *FString("Export/dmcore"));
	FString file1 = filepath + "/assimp_skin_test0.dm";
	dm_scene_save(&_scene, file1);
	//save_scene(file1, &_scene, false);
	//dm_scene_fix_node_hierarchy(&_scene, true, true);
}

void UAssimpDriverComponent::processMesh(aiMesh* Mesh, const aiScene* Scene)
{
	// the very first time this method runs, we'll need to create the empty arrays
	// we can't really do that in the class constructor because we don't know how many meshes we'll read, and this data can change between imports
	if (_vertices.Num() <= _meshCurrentlyProcessed) {
		_vertices.AddZeroed();
		_normals.AddZeroed();
		_uvs.AddZeroed();
		_tangents.AddZeroed();
		_vertexColors.AddZeroed();
		_indices.AddZeroed();

		_boneNames.AddZeroed();
		_inverseBindMatrices.AddZeroed();
		_boneTransform.AddZeroed();
		_boneWeightIndices.AddZeroed();
		_boneWeights.AddZeroed();
	}

	// we check whether the current data to read has a different amount of vertices compared to the last time we generated the mesh
	// if so, it means we'll need to recreate the mesh and resupply new indices.
	if (Mesh->mNumVertices != _vertices[_meshCurrentlyProcessed].Num())
		_requiresFullRecreation = true;

	// we reinitialize the arrays for the new data we're reading
	_vertices[_meshCurrentlyProcessed].Empty();
	_normals[_meshCurrentlyProcessed].Empty();
	_uvs[_meshCurrentlyProcessed].Empty();
	// this if actually seems useless, seeing what it does without it
	//if (_requiresFullRecreation) {
	_tangents[_meshCurrentlyProcessed].Empty();
	_vertexColors[_meshCurrentlyProcessed].Empty();
	_indices[_meshCurrentlyProcessed].Empty();
	//}

	_boneNames[_meshCurrentlyProcessed].Empty();
	_inverseBindMatrices[_meshCurrentlyProcessed].Empty();
	_boneTransform[_meshCurrentlyProcessed].Empty();
	_boneWeightIndices[_meshCurrentlyProcessed].Empty();
	_boneWeights[_meshCurrentlyProcessed].Empty();

	for (unsigned int i = 0; i < Mesh->mNumVertices; i++) {
		FVector vertex, normal;
		// process vertex positions, normals and UVs
		vertex = internal::dm_vec3_from_assimp(Mesh->mVertices[i]);
		//vertex.X = Mesh->mVertices[i].x;
		//vertex.Y = Mesh->mVertices[i].y;
		//vertex.Z = Mesh->mVertices[i].z;

		normal = internal::dm_vec3_from_assimp(Mesh->mNormals[i]);
		//normal.X = Mesh->mNormals[i].x;
		//normal.Y = Mesh->mNormals[i].y;
		//normal.Z = Mesh->mNormals[i].z;

		// if the mesh contains tex coords
		if (Mesh->mTextureCoords[0]) {
			FVector2D uvs;
			//uvs = internal::dm_vec3_from_assimp(Mesh->mTextureCoords[0][i]);
			uvs.X = Mesh->mTextureCoords[0][i].x;
			uvs.Y = Mesh->mTextureCoords[0][i].y;
			_uvs[_meshCurrentlyProcessed].Add(uvs);
		}
		else {
			_uvs[_meshCurrentlyProcessed].Add(FVector2D(0.f, 0.f));
		}
		_vertices[_meshCurrentlyProcessed].Add(vertex);
		_normals[_meshCurrentlyProcessed].Add(normal);

		if (Mesh->HasVertexColors(0)) {
			FLinearColor color = internal::dm_color_from_assimp(Mesh->mColors[0][i]);
			//FLinearColor color{ FLinearColor(Mesh->mColors[0][i].r, Mesh->mColors[0][i].g, Mesh->mColors[0][i].b, Mesh->mColors[0][i].a) };
			_vertexColors[_meshCurrentlyProcessed].Add(color);
		}
	}

	if (_requiresFullRecreation) {
		// process indices
		for (uint32 i = 0; i < Mesh->mNumFaces; i++) {
			aiFace face = Mesh->mFaces[i];
			_indices[_meshCurrentlyProcessed].Add(face.mIndices[0]);
			_indices[_meshCurrentlyProcessed].Add(face.mIndices[1]);
			_indices[_meshCurrentlyProcessed].Add(face.mIndices[2]);
			//_indices[_meshCurrentlyProcessed].Add(face.mIndices[2]);
			//_indices[_meshCurrentlyProcessed].Add(face.mIndices[1]);
			//_indices[_meshCurrentlyProcessed].Add(face.mIndices[0]);
		}
	}
}

void UAssimpDriverComponent::processBones(aiMesh* Mesh, const aiScene* Scene)
{
	//if (_boneNames.Num() <= _meshCurrentlyProcessed) {
	//	_boneNames.AddZeroed();
	//	_inverseBindMatrices.AddZeroed();
	//	_boneTransform.AddZeroed();
	//	_boneWeightIndices.AddZeroed();
	//	_boneWeights.AddZeroed();
	//}
	
	for (unsigned int i = 0; i < Mesh->mNumBones; ++i) {
		aiBone* bone = Mesh->mBones[i];
		FString bone_name = bone->mName.C_Str();
		
		_boneNames[_meshCurrentlyProcessed].Add(bone_name);
		//FMatrix boneMatrix = internal::dm_mat4_from_assimp(bone->mOffsetMatrix);
		FMatrix boneMatrix = FMatrix::Identity;
		aiNode* bNode = Scene->mRootNode->FindNode(bone->mName.C_Str());
		if (bNode) {
			//boneMatrix = internal::dm_mat4_from_assimp(bNode->mTransformation);
			boneMatrix = internal::dm_mat4_from_assimp(bone->mOffsetMatrix);
		}
		_inverseBindMatrices[_meshCurrentlyProcessed].Add(boneMatrix);
		_boneTransform[_meshCurrentlyProcessed].Add(FTransform(boneMatrix));

		_boneWeightIndices[_meshCurrentlyProcessed].SetNum(bone->mNumWeights);
		_boneWeights[_meshCurrentlyProcessed].SetNum(bone->mNumWeights);
		for (unsigned int j = 0; j < bone->mNumWeights; ++j) {
			_boneWeightIndices[_meshCurrentlyProcessed][j] = bone->mWeights[j].mVertexId;
			_boneWeights[_meshCurrentlyProcessed][j] = bone->mWeights[j].mWeight;
		}
	}
}

// Called every frame
void UAssimpDriverComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UAssimpDriverComponent::Clear()
{
	_vertices.Empty();
	_indices.Empty();
	_normals.Empty();
	_uvs.Empty();
	_tangents.Empty();
	_vertexColors.Empty();
	_meshCurrentlyProcessed = 0;
}

bool UAssimpDriverComponent::Save(const FString& File, FString& ErrorCode)
{
	return false;
}

bool UAssimpDriverComponent::Load(const FString& File, int& NumberOfSections, FString& ErrorCode)
{
	Assimp::Importer importer;
	std::string filename(TCHAR_TO_UTF8(*File));
	const aiScene* scene = importer.ReadFile(filename, aiProcessPreset_TargetRealtime_MaxQuality /* | aiProcess_MakeLeftHanded */ );
	if (!scene)
	{
		ErrorCode = importer.GetErrorString();
		return false;
	}
	_meshCurrentlyProcessed = 0;
	processNode(scene->mRootNode, scene);
	NumberOfSections = _meshCurrentlyProcessed;
	_numberOfMeshSections = _meshCurrentlyProcessed;

	postprocessNodes(scene);

	return true;
}

bool UAssimpDriverComponent::GetSection(int SectionIndex, TArray<FVector>& Vertices, TArray<int32>& Faces, TArray<FVector>& Normals, TArray<FVector2D>& UV, TArray<FLinearColor>& VertexColors, TArray<FVector>& Tangents)
{
	if (SectionIndex >= _meshCurrentlyProcessed)
	{
		return false;
	}
	Vertices = _vertices[SectionIndex];
	Faces = _indices[SectionIndex];
	Normals = _normals[SectionIndex];
	UV = _uvs[SectionIndex];
	VertexColors = _vertexColors[SectionIndex];
	Tangents = _tangents[SectionIndex];
	return true;
}

void UAssimpDriverComponent::UpDownSection(int SectionId, float Time, TArray<FVector>& Vertices)
{
	TArray<FVector> transformedVertices;
	transformedVertices = _vertices[SectionId];

	for (int i = 0; i < _vertices[SectionId].Num(); ++i) {
		transformedVertices[i].Z += 100.0f * FMath::Sin(Time);
	}
	Vertices = transformedVertices;
}

UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") UProceduralMeshComponent* UAssimpDriverComponent::DoMesh(AActor* Actor_, USceneComponent* ParentComponent, const FString& Name)
{
	MeshComponent = Cast<UProceduralMeshComponent>(SpawnNewComponent(Actor_, ParentComponent, UProceduralMeshComponent::StaticClass(), FTransform::Identity, Name));
	for (int i = 0; i < _meshCurrentlyProcessed; ++i) {
		TArray<FVector> vertices;
		TArray<int32> indices;
		TArray<FVector> normals;
		TArray<FVector2D> uvs;
		TArray<FLinearColor> colors;
		TArray<FVector> tan;
		GetSection(i, vertices, indices, normals, uvs, colors, tan);
		MeshComponent->CreateMeshSection_LinearColor(i, vertices, indices, normals, uvs, colors, TArray<FProcMeshTangent>(), true);
		//MeshComponent->ContainsPhysicsTriMeshData(true);
	}
	return MeshComponent;
}

UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") void UAssimpDriverComponent::GetBones(int SectionIndex, TArray<FTransform>& BoneTransforms)
{
	if (_boneTransform.IsValidIndex(SectionIndex) == true)
		BoneTransforms = _boneTransform[SectionIndex];
}

UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") void UAssimpDriverComponent::GetBoneNames(int SectionIndex, TArray<FString>& BoneNames)
{
	if (_boneNames.IsValidIndex(SectionIndex) == true) {
		BoneNames = _boneNames[SectionIndex];
	}
}

void UAssimpDriverComponent::ConstructScene(USceneComponent* RootComponent, int SectionId, float AtTime)
{
	//dmSceneConstructor sc;
}

