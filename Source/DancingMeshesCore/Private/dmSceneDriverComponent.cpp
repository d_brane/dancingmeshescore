// Copyright Alexander Demets

#include "dmSceneDriverComponent.h"

#include "dmSceneConstructor.h"

#include "dm_core.h"
#include "dm_scene_io.h"




UdmSceneDriverComponent::UdmSceneDriverComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

UdmSceneDriverComponent::~UdmSceneDriverComponent()
{
	dm_scene_remove(Scene);
}

void 
UdmSceneDriverComponent::BeginPlay()
{
	Super::BeginPlay();
}

void 
UdmSceneDriverComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool 
UdmSceneDriverComponent::Load(const FString& FileIn)
{
	dm_scene_remove(Scene);
	dm_scene_create(&Scene);
	dm_scene_import(Scene, { FileIn });
	return true;
}

bool 
UdmSceneDriverComponent::Save(const FString& FileOut)
{
	//dm_scene_export(FileOut, Scene, false, dm_scene_type::st_from_file_extension);

	dm_scene_export(Scene, { FileOut });
	return true;
}

bool 
UdmSceneDriverComponent::Export(const FString& FileOut)
{
	dm_scene_export(Scene, { FileOut });

	return true;
}


bool
UdmSceneDriverComponent::Close(bool RemoveSceneComponents)
{
	if (RemoveSceneComponents) {
		if (dm_scene_has_nodes(Scene)) {
			FString RootSceneNodeName = Scene->nodes[0]->name;
			TArray<USceneComponent*> Childs;
			SceneRoot->GetChildrenComponents(false, Childs);
			for (auto Child : Childs) {
				if (Child->GetName().Equals(RootSceneNodeName)) {
					Child->DestroyComponent();
					break;
				}
			}
		}
	}
	dm_scene_remove(Scene);

	return true;
}

bool 
UdmSceneDriverComponent::ViewportToScene()
{
	return true;
}

bool 
UdmSceneDriverComponent::SceneToViewport(USceneComponent* RootComponent)
{
	if (Scene == nullptr) {
		return false;
	}
	FSceneConstructorSettings Settings;
	Settings.Scene = Scene;
	Settings.BaseActor = this->GetOwner();
	Settings.RootComponent = RootComponent != nullptr ? RootComponent : this->GetOwner()->GetRootComponent();
	SceneRoot = Settings.RootComponent;

	dmSceneConstructor builder;
	
	builder.CreateViewportFromScene(Scene, this->GetOwner(), RootComponent);

	return true;
}

TArray<UdmNodeComponent*> 
UdmSceneDriverComponent::GetNodeComponents()
{
	TArray<UdmNodeComponent*> NodeArray;
	this->GetOwner()->GetComponents(NodeArray, true);
	return NodeArray;
}

TArray<UdmNodeComponent*> 
UdmSceneDriverComponent::GetJointNodeComponents()
{
	TArray<UdmNodeComponent*> NodeArray;
	TArray<UdmNodeComponent*> JointArray;
	NodeArray = GetNodeComponents();
	for (auto Node : NodeArray) {
		if (Node->IsJoint()) {
			JointArray.Add(Node);
		}
	}
	return JointArray;
}

TArray<UdmInstanceComponent*>
UdmSceneDriverComponent::GetInstanceComponents()
{
	TArray<UdmInstanceComponent*> InstanceArray;
	this->GetOwner()->GetComponents(InstanceArray, true);
	return InstanceArray;
}

TArray<UdmColliderComponent*> 
UdmSceneDriverComponent::GetColliderComponents()
{
	TArray<UdmColliderComponent*> ColliderArray;

	TArray<UActorComponent*> FilteredActorComponents;
	//FilteredActorComponents = this->GetOwner()->GetComponentsByClass(UdmColliderComponent::StaticClass());
	this->GetOwner()->GetComponents(FilteredActorComponents);

	for (auto Collider : FilteredActorComponents) {
		ColliderArray.Add(Cast<UdmColliderComponent>(Collider));
	}
	return ColliderArray;
}

bool UdmSceneDriverComponent::HasAnimations()
{
	return dm_scene_has_animations(Scene);
}
