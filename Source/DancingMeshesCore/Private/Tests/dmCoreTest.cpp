
#include "CoreTypes.h"
#include "Containers/UnrealString.h"
#include "Misc/DateTime.h"
#include "Misc/AutomationTest.h"

#include "dm_core.h"
#include "dm_file.h"
#include "dm_scene.h"

#if WITH_DEV_AUTOMATION_TESTS

#define TestSecond(Desc, A, B) if ((A.GetSecond()) != (B)) AddError(FString::Printf(TEXT("%s - A=%d B=%d"), Desc, (A.GetSecond()), (B)));


IMPLEMENT_SIMPLE_AUTOMATION_TEST(FPlaceholderTest, "TestGroup.TestSubgroup.Placeholder Test", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
IMPLEMENT_SIMPLE_AUTOMATION_TEST(FDmCoreLoadFileTest, "DM.Core.LoadFile Test", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool FPlaceholderTest::RunTest(const FString& Parameters)
{
	const FDateTime UnixEpoch = FDateTime::FromUnixTimestamp(0);
	TestSecond(TEXT("Testing Unix Epoch Second"), UnixEpoch, 0);

	// Make the test pass by returning true, or fail by returning false.
	return true;
}

bool FDmCoreLoadFileTest::RunTest(const FString& Parameters)
{
	//dm_scene* scn1 = nullptr;
	//dm_status result = dm_scene_load(scn1, "test.dm");
	//else AddError(FString::Printf(TEXT("%s - not loaded"), "test.dm"));

	//result = dm_scene_save("test_out.dm", scn);
	//if (result.is_ok()) { return true; }
	//else AddError(FString::Printf(TEXT("%s - not saved"), "test_out.dm"));

	//dm_scene_add_node(scn, { .name = "hello", .transform = dm_identity_transform });

	//scn = dm_scene_create(scn);

	//return result.is_ok();
	return true;
}


#endif
