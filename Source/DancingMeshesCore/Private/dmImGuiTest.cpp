// Copyright by Alexander Demets

#include "dmImGuiTest.h"

#include <Runtime/Engine/Public/EngineUtils.h>




#include "ImGuiModule.h"
#include "ImGuiModuleProperties.h"

//FImGuiDelegateHandle UdmImGuiTest::ImGuiMultiContextTickHandle;

// Sets default values for this component's properties
UdmImGuiTest::UdmImGuiTest()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
		// Register static multi-context delegate (only for default object for symmetry with destruction).
	// Note that if module is not available at this point, registration can fail. This limitation of the API will be
	// fixed in one of the next few updates but for now, if necessary, a solution would be to retry registration
	// at a later stage (which I'm not doing here). 
#if DM_WITH_IMGUI && IMGUI_WITH_DELEGATES
	if (IsTemplate() && !ImGuiMultiContextTickHandle.IsValid() && FImGuiModule::IsAvailable())
	{
		ImGuiMultiContextTickHandle = FImGuiModule::Get().AddMultiContextImGuiDelegate(FImGuiDelegate::CreateStatic(&UdmImGuiTest::ImGuiMultiContextTick));
	}
#endif // DM_WITH_IMGUI
}


void UdmImGuiTest::BeginDestroy()
{
	Super::BeginDestroy();

	// Unregister static multi-context delegate. Failing to do so would result with multiplication of delegates during
	// hot reloading. And we do it only once for the default object to make sure that we unregister only when class is
	// not used anymore.
#if DM_WITH_IMGUI && IMGUI_WITH_DELEGATES
	if (IsTemplate() && ImGuiMultiContextTickHandle.IsValid() && FImGuiModule::IsAvailable())
	{
		FImGuiModule::Get().RemoveImGuiDelegate(ImGuiMultiContextTickHandle);
		ImGuiMultiContextTickHandle.Reset();
	}
#endif // DM_WITH_IMGUI
}

// Called when the game starts
void UdmImGuiTest::BeginPlay()
{
	Super::BeginPlay();

	// ...
#if DM_WITH_IMGUI && IMGUI_WITH_DELEGATES
	ImGuiTickHandle = FImGuiModule::Get().AddWorldImGuiDelegate(FImGuiDelegate::CreateUObject(this, &UdmImGuiTest::ImGuiTick));
#endif // DM_WITH_IMGUI
	
}

void UdmImGuiTest::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	// Unregister object's delegate.
#if DM_WITH_IMGUI && IMGUI_WITH_DELEGATES
	FImGuiModule::Get().RemoveImGuiDelegate(ImGuiTickHandle);
#endif // DM_WITH_IMGUI
}


// Called every frame
void UdmImGuiTest::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
		// Debug during world tick.
#if DM_WITH_IMGUI
	ImGui::Begin("ImGui Debug Order Test");
	ImGui::SetNextWindowSize(ImVec2(500, 500));

	ImGui::Text("Actor Tick: Actor = '%ls', World = '%ls', CurrentWorld = '%ls'",
		(wchar_t*)const_cast<TCHAR*>(*GetNameSafe(this)), (wchar_t*)const_cast<TCHAR*>(*GetNameSafe(GetWorld())), (wchar_t*)const_cast<TCHAR*>(*GetNameSafe(GWorld)));

	ImGui::End();
#endif // DM_WITH_IMGUI
}

void UdmImGuiTest::InitImGui()
{
	FImGuiModuleProperties Properties;
	//ImVec4 ClearColor = ImColor{ 114, 144, 154 };
	Properties.SetShowDemo(true);
	

	if (Properties.ShowDemo())
	{
		const int32 ContextBit = ContextIndex < 0 ? 0 : 1 << ContextIndex;

		// 1. Show a simple window
		// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
		{
			static float f = 0.0f;
			ImGui::Text("Hello, world!");
			ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
			ImGui::ColorEdit3("clear color", (float*)&ClearColor);

			if (ContextBit)
			{
				if (ImGui::Button("Demo Window")) ShowDemoWindowMask ^= ContextBit;
				if (ImGui::Button("Another Window")) ShowAnotherWindowMask ^= ContextBit;
			}
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		}

		// 2. Show another simple window, this time using an explicit Begin/End pair
		if (ShowAnotherWindowMask & ContextBit)
		{
			ImGui::SetNextWindowSize(ImVec2(500, 200), ImGuiCond_FirstUseEver);
			ImGui::Begin("Another Window");
			ImGui::Text("Hello");
			ImGui::End();
		}

		// 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
		if (ShowDemoWindowMask & ContextBit)
		{
			// If more than one demo window is opened display warning about running ImGui examples in multiple contexts.

			// For everything, but the first windows in this frame we assume warning.
			bool bWarning = true;
			if (GFrameNumber > LastDemoWindowFrameNumber)
			{
				// If this is the first window in this frame, then we need to look at the last frame to see whether
				// there were more than one windows. Higher frame distance automatically means that there were not.
				bWarning = ((GFrameNumber - LastDemoWindowFrameNumber) == 1) && (DemoWindowCounter > 1);

				LastDemoWindowFrameNumber = GFrameNumber;
				DemoWindowCounter = 0;
			}

			DemoWindowCounter++;

			if (bWarning)
			{
				ImGui::Spacing();

				ImGui::PushStyleColor(ImGuiCol_Text, { 1.f, 1.f, 0.5f, 1.f });
				ImGui::TextWrapped("Demo Window is opened in more than one context, some of the ImGui examples may not work correctly.");
				ImGui::PopStyleColor();

				if (ImGui::IsItemHovered())
				{
					ImGui::SetTooltip(
						"Some of the ImGui examples that use static variables may not work correctly\n"
						"when run concurrently in multiple contexts.\n"
						"If you have a problem with an example try to run it in one context only.");
				}
			}

			// Draw demo window.
			ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_FirstUseEver);
			ImGui::ShowDemoWindow();
		}
	}

	//bool my_tool_active = true;
	//float my_color[4];

	//// Create a window called "My First Tool", with a menu bar.
	//ImGui::SetNextWindowSize(ImVec2(750, 500));
	//ImGui::Begin("My First Tool", &my_tool_active, ImGuiWindowFlags_MenuBar);
	//ImGui::SetNextWindowSize(ImVec2(500, 500));
	//if (ImGui::BeginMenuBar())
	//{
	//	if (ImGui::BeginMenu("File"))
	//	{
	//		if (ImGui::MenuItem("Open..", "Ctrl+O")) { /* Do stuff */ }
	//		if (ImGui::MenuItem("Save", "Ctrl+S")) { /* Do stuff */ }
	//		if (ImGui::MenuItem("Close", "Ctrl+W")) { my_tool_active = false; }
	//		ImGui::EndMenu();
	//	}
	//	ImGui::EndMenuBar();
	//}

	//// Edit a color (stored as ~4 floats)
	//ImGui::ColorEdit4("Color", my_color);

	//// Plot some values
	//const float my_values[] = { 0.2f, 0.1f, 1.0f, 0.5f, 0.9f, 2.2f };
	//ImGui::PlotLines("Frame Times", my_values, IM_ARRAYSIZE(my_values));

	//// Display contents in a scrolling region
	//ImGui::TextColored(ImVec4(1, 1, 0, 1), "Important Stuff");
	//ImGui::BeginChild("Scrolling");
	//for (int n = 0; n < 50; n++)
	//	ImGui::Text("%04d: Some text", n);
	//ImGui::EndChild();
	//ImGui::End();

	//ImGui::Begin("My First Tool", &my_tool_active, ImGuiWindowFlags_MenuBar);
	//ImGui::Text("Hello, world %d", 123);
	//if (ImGui::Button("Save"))
	//	MySaveFunction();
	//ImGui::InputText("string", buf, IM_ARRAYSIZE(buf));
	//ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
	//ImGui::End();
}

void UdmImGuiTest::ShutdownImGui()
{
	return;
}

#if DM_WITH_IMGUI && IMGUI_WITH_DELEGATES
void UdmImGuiTest::ImGuiTick()
{
	ImGui::Begin("ImGui Debug Order Test");

	ImGui::Text("ImGui World Tick: Actor = '%ls', World = '%ls', CurrentWorld = '%ls'",
		(wchar_t*)const_cast<TCHAR*>(*GetNameSafe(this)), (wchar_t*)const_cast<TCHAR*>(*GetNameSafe(GetWorld())), (wchar_t*)const_cast<TCHAR*>(*GetNameSafe(GWorld)));

	ImGui::End();
}

void UdmImGuiTest::ImGuiMultiContextTick()
{
	ImGui::Begin("ImGui Debug Order Test");

	int32 Count = 0;
	//for (TActorIterator<UdmImGuiTest> It(GWorld); It; ++It, ++Count)
	for (TObjectIterator<UdmImGuiTest> It; It; ++It, ++Count)
	{
		UdmImGuiTest* Actor = *It;
		UWorld* World = Actor ? Actor->GetWorld() : nullptr;
		ImGui::Text("ImGui Multi-Context Tick: Actor = '%ls', World = '%ls', CurrentWorld = '%ls'",
			(wchar_t*)const_cast<TCHAR*>(*GetNameSafe(Actor)), (wchar_t*)const_cast<TCHAR*>(*GetNameSafe(World)), (wchar_t*)const_cast<TCHAR*>(*GetNameSafe(GWorld)));
	}

	ImGui::Text("ImGui Multi-Context Tick: %d actors in world '%ls'.", Count, (wchar_t*)const_cast<TCHAR*>(*GetNameSafe(GWorld)));

	ImGui::End();
}
#endif // DM_WITH_IMGUI
