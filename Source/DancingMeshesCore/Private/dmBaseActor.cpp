// Copyright Alexander Demets


#include "dmBaseActor.h"

// Sets default values
AdmBaseActor::AdmBaseActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AdmBaseActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AdmBaseActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

