// Copyright by Alexander Demets


#include "GltfRuntimeMesh.h"

#include "../External/dmcore/include/dm_scene.h"

// Sets default values for this component's properties
UGltfRuntimeMesh::UGltfRuntimeMesh()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGltfRuntimeMesh::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UGltfRuntimeMesh::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


bool load_mesh(cgltf_data* data, dm_scene* scene)
{
	return false;
}

bool UGltfRuntimeMesh::LoadFromFile(FString Filepath, bool LoadTextures, bool LoadAnimations)
{
	//cgltf_options options = { };
	//cgltf_data* data = NULL;
	//
	//cgltf_result result = cgltf_parse_file(&options, dm_string_to_cstr(Filepath), &data);
	//if (result == cgltf_result_success)
	//{
	//	// code here
	//	// ...
	//	// ...



	//	cgltf_free(data);
	//}
	//return true;
	return false;
}

void UGltfRuntimeMesh::LoadFromString(FString GltfString)
{
}

void UGltfRuntimeMesh::Unload()
{
}

void UGltfRuntimeMesh::UpdateMeshSection(int MeshSectionId, FString AnimationClip, float Time, bool MapTimeToDuration, TArray<FVector>& Vertices, TArray<int>& Indices, TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FLinearColor>& VertexColors, TArray<FVector>& Tangents)
{
}

void UGltfRuntimeMesh::UpdateProceduralMeshComponent(UProceduralMeshComponent* MeshComponent, FString AnimationClip, float AtTime, bool IsFirstInit)
{
}

