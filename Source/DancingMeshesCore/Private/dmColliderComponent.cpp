// Copyright Alexander Demets

#include "dmColliderComponent.h"

#include "Runtime/Engine/Classes/Engine/StaticMesh.h"


UdmColliderComponent::UdmColliderComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UdmColliderComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UdmColliderComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}


void UdmColliderComponent::SetupFromDataStruct(const dm_bounds Collider)
{
	//StaticMesh'/DancingMeshesCore/Geometry/Shapes/Basic/Meshes_Cube.Meshes_Cube'
	// if (Collider.type == bt_box) {
	UStaticMesh* ColliderMesh = LoadObject<UStaticMesh>(nullptr, TEXT("/DancingMeshesCore/Geometry/Shapes/Basic/Meshes_Cube.Meshes_Cube"));
	//}
	this->SetStaticMesh(ColliderMesh);
	//this->SetRelativeScale3D({ 0.05, 0.05, 0.5 });
	this->SetRelativeScale3D(dm_bounds_size(&Collider));
}
