// Copyright Alexander Demets


#include "dmInstanceComponent.h"

#include "dm_unreal_helper.h"
#include "dm_logging.h"

// Sets default values for this component's properties
UdmInstanceComponent::UdmInstanceComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UdmInstanceComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UdmInstanceComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UdmInstanceComponent::SetupFromDataStruct(const dm_node& InstanceNode_, dm_scene* Scene)
{
	InstanceNode = &InstanceNode_;
	FString MeshName = InstanceNode_.instance->name + "_mesh";
	ProceduralMesh = Cast<UProceduralMeshComponent>(SpawnNewComponent(this->GetOwner(), this, UProceduralMeshComponent::StaticClass(), InstanceNode_.transform, MeshName));
	int SectionId = 0;
	for (const dm_primitive& prim : InstanceNode_.instance->mesh->primitives) {
		const TArray<FVector>& vertices = prim.vertices;
		const TArray<int32>& indices = prim.indices;
		const TArray<FVector>& normals = prim.normals;
		const TArray<FVector2D>& uvs = prim.uvs;
		//const TArray<FLinearColor>& colors = prim.vertex_colors;
		const TArray<FLinearColor>& colors = prim.vertex_colors;

		//TArray<FProcMeshTangent> tangents;
		//for (FVector t : prim.tangents) {
		//	FProcMeshTangent pt = { t.X, t.Y, t.Z };
		//	tangents.Add(pt);
		//}
		//MeshComponent->CreateMeshSection_LinearColor(SectionId, prim.vertices, prim.indices, prim.normals, prim.normals, prim.uvs, prim.vertex_colors, prim.tangents, true);
		ProceduralMesh->ClearAllMeshSections();
		ProceduralMesh->CreateMeshSection_LinearColor(SectionId, vertices, indices, normals, uvs, colors, TArray<FProcMeshTangent>(), false);
		ProceduralMesh->bUseAsyncCooking = true;
		ProceduralMesh->ContainsPhysicsTriMeshData(false);

		++SectionId;
	}
}

void UdmInstanceComponent::UpdateMesh(TArray<TArray<FVector>>& Positions)
{
	//ProceduralMesh->UpdateMeshSection_LinearColor(0, )
	//int SectionId = 0;
	for (int SectionId = 0; SectionId < Positions.Num(); ++SectionId) {
		auto Section = ProceduralMesh->GetProcMeshSection(SectionId);
		ProceduralMesh->UpdateMeshSection_LinearColor(SectionId, Positions[SectionId], TArray<FVector>(),
			TArray<FVector2D>(), TArray<FLinearColor>(), TArray<FProcMeshTangent>());
	}
}

void UdmInstanceComponent::ApplyWeightsAsVertexColors()
{
	int section_number = 0;
	for (auto p : InstanceNode->instance->mesh->primitives) {

		uint32 number_of_joints = p.joints.Num();
		uint32 number_of_vertices = p.vertices.Num();

		TArray<FLinearColor> joint_colors;

		for (auto jnode : p.joints) {
			joint_colors.Add(jnode->joint->_color);
		}

		TArray<FLinearColor> vertex_colors;
		vertex_colors.SetNum(number_of_vertices);


		//for (uint32 weight_id = 0, joint_id = 0; weight_id < (number_of_vertices * number_of_weights); ++weight_id, ++joint_id) {
		//	if ((weight_id % number_of_joints) == 0) {
		//		joint_id = 0;
		//	}
		//}

		for (uint32 vertex_id = 0; vertex_id < number_of_vertices; ++vertex_id) {

			FLinearColor col = FLinearColor::Black;
			float highest_weight = 0.0f;
			int highest_weight_index = 0;

			float weight_sum = 0.0f;

			for (uint32 joint_index = 0; joint_index < number_of_joints; ++joint_index) {
				//int color_index = (i * number_of_colors) - j - 1;	//i  + j - 1;	//i * (j + 1) - 1;
				uint32 weight_index = ((vertex_id + 1) * number_of_joints - 1) - (number_of_joints - (joint_index + 1));
				if (p.weights.Num() <= (int)weight_index) {
					// oh noeee
					dm_log_msg("fuuuck.");
					weight_index = p.weights.Num() - 1;
				}
				col += joint_colors[joint_index] * p.weights[weight_index];
				weight_sum += p.weights[weight_index];
				//float current_weight = p.weights[weight_index];
				//if (current_weight > highest_weight) {
				//	highest_weight = current_weight;
				//	highest_weight_index = joint_index;
				//	col = joint_colors[highest_weight_index];
				//}
			}
			//if (weight_sum < 0.9f) {
			//	vertex_colors[vertex_id] = FLinearColor::Red;
			//}
			//else {
				vertex_colors[vertex_id] = col;
			//}
			
		}
		ProceduralMesh->UpdateMeshSection_LinearColor(section_number, p.vertices, p.normals, p.uvs, vertex_colors, TArray<FProcMeshTangent>());

		++section_number;
	}
}

UFUNCTION(BlueprintCallable) void UdmInstanceComponent::SetInstanceMaterial(UMaterialInterface *Material_)
{
	int number_of_sections = ProceduralMesh->GetNumSections();
	for (int i = 0; i < number_of_sections; ++i) {
		ProceduralMesh->SetMaterial(0, Material_);
	}
}

UFUNCTION(BlueprintCallable) void UdmInstanceComponent::SetInstanceMaterial_Section(UMaterialInterface* Material_, int SectionId)
{
	ProceduralMesh->SetMaterial(SectionId, Material_);
}

UFUNCTION(BlueprintCallable) FdmMaterialParameters UdmInstanceComponent::GetMaterialParameters(int SectionId)
{
	FdmMaterialParameters params;

	dm_material* mat = InstanceNode->instance->mesh->primitives[SectionId].material;

	if (mat && mat->diffuse_map)
		params.DiffuseMapUri = mat->diffuse_map->uri;

	return params;
}

