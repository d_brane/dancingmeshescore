// Copyright Alexander Demets

#include "dmSceneConstructor.h"

#include "Runtime/Core/Public/Misc/Paths.h"
#include "ProceduralMeshComponent.h"

#include "dm_core.h"
#include "dm_file.h"
#include "dm_unreal_helper.h"

UdmNodeComponent* createNodeComponent(const FSceneConstructorSettings& Settings, const dm_node& Node) {
	FString ParentNodeName = "";
	UdmNodeComponent* NodeComp = nullptr;
	USceneComponent* ParentNode = nullptr;
	ParentNode = (Settings.RootComponent != nullptr) ? Settings.RootComponent : Settings.BaseActor->GetRootComponent();

	if (Node.parent != nullptr) {
		ParentNodeName = Node.parent->name;
		USceneComponent* AttachToNode = FindComponent<USceneComponent>(Settings.BaseActor, ParentNodeName); //Cast<USceneComponent>(Settings.BaseActor->GetDefaultSubobjectByName((*ParentNodeName)));
		if (AttachToNode != nullptr) {
			ParentNode = AttachToNode;
			ParentNodeName = "";
		}
		else {
			ParentNode = AttachToNode;
		}
	}
	NodeComp = Cast<UdmNodeComponent>(SpawnNewComponent(Settings.BaseActor, ParentNode, UdmNodeComponent::StaticClass(), Node.transform, Node.name));
	NodeComp->_parent = ParentNodeName;	// this will contain parent name, if parent wasn't existing at spawn-time, so the hierarchy can be fixed later
	NodeComp->_node = &Node;
	return NodeComp;
}

UdmInstanceComponent* createInstanceComponent(const FSceneConstructorSettings& Settings, const dm_instance& Instance, const dm_node& InstanceNode) {
	UdmNodeComponent* ParentNode = nullptr;
	ParentNode = FindComponent<UdmNodeComponent>(Settings.BaseActor, InstanceNode.name); //Cast<UdmNodeComponent>(Settings.BaseActor->GetDefaultSubobjectByName((*MeshNode.name)));
	if (ParentNode == nullptr) {
		ParentNode = createNodeComponent(Settings, InstanceNode);	// careful, change, once createNodeComponent calls createMesh as to not create weirdness
	}
	UdmInstanceComponent* InstanceComponent = Cast<UdmInstanceComponent>(SpawnNewComponent(Settings.BaseActor, ParentNode, UdmInstanceComponent::StaticClass(), InstanceNode.transform, InstanceNode.instance->name));
	ParentNode->Instance = InstanceComponent;
	InstanceComponent->SetupFromDataStruct(InstanceNode, Settings.Scene);

	return InstanceComponent;
}




UdmNodeComponent* processNode(const FSceneConstructorSettings& Settings, const dm_node& dmNode) {
	return createNodeComponent(Settings, dmNode);
}

UdmNodeComponent* processInstance(const FSceneConstructorSettings& Settings, const dm_node& dmNode) {

	//[note][ademets] careful, not every instance has currently a parent! it should in the future though (just leave for now..)
	UdmInstanceComponent* Inst = createInstanceComponent(Settings, *dmNode.instance, dmNode);

	return Inst->GetOwningNode();
}

UdmNodeComponent* processMesh(const FSceneConstructorSettings& Settings, UdmNodeComponent* ueNode, const dm_node& dmNode) {
	return nullptr;
}
UdmNodeComponent* processCamera(const FSceneConstructorSettings& Settings, const dm_node& dmNode) {
	return nullptr;
}
UdmNodeComponent* processJoint(const FSceneConstructorSettings& Settings, const dm_node& dmNode) {
	UdmNodeComponent* joint_comp = processNode(Settings, dmNode);

	joint_comp->Joint = NewObject<UdmJoint>(joint_comp);
	joint_comp->Joint->SetupFromDataStruct(joint_comp, &dmNode, dmNode.joint, false);
	
	//[todo] add collider component 


	return joint_comp;
}


UdmNodeComponent* setupNodeComponent(const FSceneConstructorSettings& Settings, const dm_node& dmNode) {

	UdmNodeComponent* ueNode = nullptr;

	dm_node::dm_node_type type = dm_node_get_type(&dmNode);
	switch (type) {

		case dm_node::dm_node_type::nt_node:
			return processNode(Settings, dmNode);
			break;
		case dm_node::dm_node_type::nt_joint:
			return processJoint(Settings, dmNode); 
			break;
		//case node::node_type::nt_skinned_instance:
		//	return processInstance(Settings, dmNode);
		case dm_node::dm_node_type::nt_instance:
			return processInstance(Settings, dmNode);
			break;
		case dm_node::dm_node_type::nt_camera:
			return processCamera(Settings, dmNode);
			break;
		case dm_node::dm_node_type::nt_environment:
			return processNode(Settings, dmNode);
			break;
			break;
	}
	return processNode(Settings, dmNode);
}


dmSceneConstructor::dmSceneConstructor()
{

}

dmSceneConstructor::~dmSceneConstructor()
{
	if (Scene != nullptr)
		delete Scene;
	Scene = nullptr;
}

void dmSceneConstructor::AddNode(const FSceneConstructorSettings& Settings, const dm_node& Node)
{
	setupNodeComponent(Settings, Node);
}

void dmSceneConstructor::UpdateMesh(const FSceneConstructorSettings& Settings, dm_node& Node, TArray<TArray<FVector>>& Positions)
{

}

void dmSceneConstructor::CreateViewportFromScene(dm_scene* _Scene, AActor* Base, USceneComponent* Root)
{
	const auto fix_node_hierarchy = [](const FSceneConstructorSettings& Settings, bool flushParentIds) -> bool {
		TArray<UdmNodeComponent*> Nodes;
		Settings.BaseActor->GetComponents<UdmNodeComponent>(Nodes);

		for (auto Node : Nodes) {
			if (Node->_parent.Len() > 0) {
				//FString ParentName = Node->_parent;
				UdmNodeComponent* ParentNode = FindComponent<UdmNodeComponent>(Settings.BaseActor, Node->_parent); //Cast<UdmNodeComponent>(Settings.BaseActor->GetDefaultSubobjectByName((*Node->_parent)));
				if (ParentNode != nullptr) {
					//Node->AttachTo(ParentNode);
					Node->AttachToComponent(ParentNode, FAttachmentTransformRules::KeepRelativeTransform);
					if (flushParentIds) {
						Node->_parent.Empty();
					}
				}
			}
		}
		return true;
	};

	FSceneConstructorSettings Settings;
	Settings.Scene = _Scene;
	Settings.BaseActor = Base;
	Settings.RootComponent = Root;
	
	for (dm_node* Node: _Scene->nodes) {
		AddNode(Settings, *Node);
	}
	fix_node_hierarchy(Settings, true);
}

void dmSceneConstructor::CreateSceneFromViewport(const AActor* Base, const USceneComponent* Root, dm_scene*& _Scene)
{
}
