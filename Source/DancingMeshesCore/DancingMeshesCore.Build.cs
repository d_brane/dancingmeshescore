// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

using System.IO;

public class DancingMeshesCore : ModuleRules
{
	public DancingMeshesCore(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        //CppStandard = CppStandardVersion.Latest;
        //PCHUsage = PCHUsageMode.NoSharedPCHs;
        //PrivatePCHHeaderFile = "DancingMeshesCore_PCH.h";
        // don't enforce header include order
        

        //bUseRTTI = true;
        bEnableExceptions = true;

        PublicDefinitions.Add("_CRT_SECURE_NO_WARNINGS");
        if (Target.Platform == UnrealTargetPlatform.Android)
        {
            PublicDefinitions.Add("OZZ_BUILD_SIMD_REF");
            PublicDefinitions.Add("_M_IX86_FP 1");
        }
        
        //PrivateDependencyModuleNames.Add("FBX");

        PublicIncludePaths.Add(ModuleDirectory + "/External/dmcore/include");
        PublicIncludePaths.Add(ModuleDirectory + "/External/dmcore/external");

        PrivateIncludePaths.Add(ModuleDirectory + "/External/dmcore/external/ozz-animation/include/");
        PrivateIncludePaths.Add(ModuleDirectory + "/External/dmcore/external/ozz-animation/src/");

        PublicIncludePaths.Add(ModuleDirectory + "/External/dmcore/external/fmt/include/");
        PrivateIncludePaths.Add(ModuleDirectory + "/External/dmcore/external/fmt/src/");


        bool useAssimp = true;
        bool useAssimpDebug = false;
        bool useImGui = true;
        bool use_python = false;
        //bool use_cgltf = false;f
        if (useAssimp)
        {
          
            PublicIncludePaths.Add(ModuleDirectory + "/External/dmcore/external/assimp/include/");
            string BuildPath = Path.GetFullPath(ModuleDirectory + "/External/dmcore/external/assimp/lib/");

            if (Target.Platform == UnrealTargetPlatform.Win64)
            {
                if (useAssimpDebug)
                {
                    PublicAdditionalLibraries.Add(BuildPath + "win64/assimp-vc143-mtd.lib");
                    PublicAdditionalLibraries.Add(BuildPath + "win64/zlibstaticd.lib");
                    PublicAdditionalLibraries.Add(BuildPath + "win64/dracod.lib");
                }
                else
                {
                    PublicAdditionalLibraries.Add(BuildPath + "win64/assimp-vc143-mt.lib");
                    PublicAdditionalLibraries.Add(BuildPath + "win64/zlibstatic.lib");
                    PublicAdditionalLibraries.Add(BuildPath + "win64/draco.lib");
                }
            }
            else if (Target.Platform == UnrealTargetPlatform.Android)
            {
                if (useAssimpDebug)
                {
                    PublicAdditionalLibraries.Add(BuildPath + "android/libassimpd.a");
                    PublicAdditionalLibraries.Add(BuildPath + "android/libzlibstaticd.a");
                    PublicAdditionalLibraries.Add(BuildPath + "android/libdracod.a");
                }
                else
                {
                    PublicAdditionalLibraries.Add(BuildPath + "android/libassimp.a");
                    PublicAdditionalLibraries.Add(BuildPath + "android/libzlibstatic.a");
                    PublicAdditionalLibraries.Add(BuildPath + "android/libandroid_jniiosystem.a");
                    PublicAdditionalLibraries.Add(BuildPath + "android/libdraco.a");
                }
            }
        }

        
        if (use_python && (Target.Platform == UnrealTargetPlatform.Win64))
        {
            string python_home = "C:/Users/ademe/scoop/apps/python37/current";
            string python_include = python_home;
            string python_lib = python_home + "/libs/python37.lib";
            string python_dll = python_home + "/python37.dll";

            PublicIncludePaths.Add(python_home);
            PublicIncludePaths.Add(python_home + "/include");
            PublicAdditionalLibraries.Add(python_lib);
            PublicDelayLoadDLLs.Add(python_dll);
        }
        else
        {
            use_python = false;
        }
        PrivateDefinitions.Add(string.Format("DM_WITH_PYTHON={0}", use_python ? 1 : 0));


        if (useImGui == true)
        {
            PublicDependencyModuleNames.Add("ImGui");
        }


        PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
                "ProceduralMeshComponent"
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
