// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "dmJointComponent.generated.h"

/*
enum class EColliderPivotType : uint8 
{
	Origin,	// at 0,0,0 of mesh
	Center,	// at (max_bounds - min_bounds) / 2
	Custom
};

enum class EColliderType : uint8 
{
	None,
	Box,
	Sphere,
	Capsule,
	Mesh
};

struct SJointCollider 
{
	EColliderPivotType PivotType;
	FVector	CustomPivot;

	EColliderType ColliderType;
	FVector	ColliderScale;
};

enum class EJointType : uint8 
{
	Root,
	Normal,
	SubBase,	// Tree branching points \o/ (subscore being joint in question)
	EndPoint,
	EndEffector	// think about if actually a joint, or better as a UNodeComponent ??
};

//template<typename T, class B> struct AnimationKey { enum class KeyType { Linear, Step, Bezier } Type; TMap<T, B> Key; };
//struct SVectorKey {};
//struct SQuatKey {};
enum class EAnimationType { Linear, Step, Bezier };

struct SSparseJointAnimationData 
{
	EAnimationType Type;
	TSparseArray<FVector> Positions{};
	TBitArray<FDefaultBitArrayAllocator> PositionKeys{};
	TSparseArray<FQuat> Rotations{};
	TBitArray<FDefaultBitArrayAllocator> RotationKeys{};
	TSparseArray<FVector> Scales{};
	TBitArray<FDefaultBitArrayAllocator> ScaleKeys{};
};
struct SJointAnimationData 
{
	EAnimationType Type;
	TMap<uint32, FVector> Positions{};
	TMap<uint32, FQuat> Rotations{};
	TMap<uint32, FVector>Scales{};
};
struct SJointTimedAnimationData
{
	EAnimationType Type;
	TMap<float, FVector> Positions{};
	TMap<float, FQuat> Rotations{};
	TMap<float, FVector>Scales{};
};

enum class EJointRotationConstraint : uint8 
{
	None,
	Pivot,	// Neck, Spine (rot: Z, fixed: XY)
	Hinge,	// Elbow (rot: Y or X, fixed: Z & X or Y )
	Saddle,	// Wrist (rot: XY, fixed: Z)
	BallAndSocket, // Foot (rot: XYZ, fixed: pos)
};

enum class EJointTranslationContraint : uint8
{
	None,	// free translation around XYZ
	X,
	Y,
	Z,
	XY,
	XZ,
	YZ,
	XYZ		// full lock on all axis
};

struct SJointConstraint 
{
	EJointRotationConstraint RotationConstraint;
	FVector2D AngleX, AngleY, AngleZ;

	EJointTranslationContraint TranslationConstraint;
	FVector2D PositionX, PositionY, PositionZ;
};

// extra:
enum class EJointSimulationType : uint8
{
	Fixed, Animated, Simulated, AnimatedPlusSimulated
};


struct SJointData 
{
	FString Name;
	FTransform DefaultPose;

	SJointConstraint Constraint;
	SJointCollider Collider;
	SJointAnimationData AnimationData;
	EJointSimulationType SimulationType;

	// computed:
	float Length;
	int MaxFrameNumber;
	EJointType Type;
	uint32 Hash;
};

// class intern enums:
enum class EJointVisualizationMode : uint8
{
	Invisible,
	Joint,
	Bone,
	Collider,
	Mesh,
	JointBone,
	JointBoneCollider,
	JointBoneColliderMesh,
	JointBoneMesh,
	ColliderMesh
};

enum class EJointState : uint8
{
	DefaultPose,
	Playing,
	Editing,	// needs to be own state? would be activated when moving joint around, or live recording joint for example
	Stopped
};
*/

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UdmJointComponent : public USceneComponent
{
	GENERATED_BODY()

protected:

	//SJointData JointData;
	//EJointVisualizationMode VisualizationMode;
	//EJointState State;
	//int CurrentFrame;

public:
	// Sets default values for this component's properties
	UdmJointComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// [state]
	bool HasAnimations() { return true; }
	bool HasChildrenJoints() { return false; }
	bool HasCollider() { return false; }
	bool HasScripts() { return false; }
	bool HasBounds() { return false; }	// does not have bounding box, if simple node with no attachments

	bool HasMesh() { return false; }
	bool HasSkinnedMesh() { return false; }
	bool HasMorphedMesh() { return false; }
	bool HasAnimatedMesh() { return false; }
	bool HasProceduralMesh() { return false; }

	bool HasSplines() { return false; }
	bool HasAnimatedSplines() { return false; }

	bool HasVoxels() { return false; }
	bool HasSkinnedVoxels() { return false; }
	bool HasAnimatedVoxels() { return false; }

	bool HasSdf() { return false; }
	bool HasAnimatedSdf() { return false; }

	bool HasSprites() { return false; }
	bool HasAnimatedSprites() { return false; }

	bool HasKevlinets() { return false; }
	bool HasAnimatedKevlinets() { return false; }


	// [data accessors]
	UStaticMesh* GetJointMesh() { return nullptr; }
	//SJointData GetJointData() { return JointData; }

	//SJointAnimationData GetAnimationData() { return JointData.AnimationData; }
	//SJointConstraint GetJointConstraint() { return JointData.Constraint; }
	//SJointCollider GetJointCollider() { return JointData.Collider; }

	//FTransform GetDefaultPose() { return JointData.DefaultPose; }

	//// [setup]
	//void SetupFromDataStruct(SJointData Data) {}
	//void Setup(FString Name, SJointCollider Collider, FVector ColliderStartScale, EJointVisualizationMode VisMode) {}

	void UpdateDefaultPose() {}
	void ResetToDefaultPose() {}

	// [manipulation]
	FTransform SolveTailPositionWorldInput(FVector WorldPosition, bool FixedParentOffsetDelta = true) { return FTransform::Identity;  }
	
	FTransform SolveHeadPositionModelInput(FVector ModelPosition, bool FixedPosition = false) { return FTransform::Identity; }
	//FTransform SolveHeadPositionRelativeInput(FVector RelativePosition, bool LengthFixed = true) { }

	//void SolveTailPosition

	// [animation]
	void RemoveKey(int FrameNumber) {}
	void RemovePositionKey(int FrameNumber) {}
	void RemoveRotationKey(int FrameNumber) {}
	void RemoveScaleKey(int FrameNumber) {}

	void SetTime(float Time, int FramesPerSecond, bool Truncate) {}
	void SetFrame(int Frame, bool Truncate) {}
	void NextFrame(bool Truncate) {}
	void PreviousFrame(bool Truncate) {}
	
	void RecordFrame(int Frame) {}
	void RecordPosition(int Frame) {}
	void RecordRotation(int Frame) {}
	void RecordScale(int Frame) {}
	
	// [transform]
	void MoveJointTail(FVector NewPosition, bool MoveParentHead) {}
	void MoveJointHead(FVector NewPosition, bool MoveChildrenTail) {}
	void MoveJoint(FVector NewPosition, bool MoveChildren) {}
	void RotateJoint(FQuat NewRotation, bool RotateChildren) {}
	void RotateJointDelta(FQuat DeltaRotation, bool RotateChildren) {}
	void ScaleJoint(FVector NewScale, bool ScaleChildren) {}
	//void Move(FTransform NewTransform, bool UpdateChildren) {}

	// [visualization]
	//void SetVisualizationMode(EJointVisualizationMode Mode) { VisualizationMode = Mode; }
	//EJointVisualizationMode GetVisualizationMode() { return VisualizationMode; }

	void SetMaterials(float JointMaterial, float BoneMaterial, float ColliderMaterial) {}
	void SetJointMaterial(float JointMaterial) {}
	void SetBoneMaterial(float BoneMaterial) {}
	void SetColliderMaterial(float ColliderMaterial) {}

	// [computed values]
	UdmJointComponent* GetRootJoint() { return nullptr; }
	UdmJointComponent* GetParentJoint() { return nullptr; }

	TArray<UdmJointComponent*> GetChildrenJoints()
	{ 
		TArray<USceneComponent*> ChildrenAll; 
		this->GetChildrenComponents(false, ChildrenAll); 

		TArray<UdmJointComponent*> ChildrenJoints;
		for (int i = 0; i < ChildrenAll.Num(); ++i) { 
			if (ChildrenAll[i]->GetClass()->StaticClass == UdmJointComponent::StaticClass)
				ChildrenJoints.Add((UdmJointComponent*)ChildrenAll[i]);
		}
		return ChildrenJoints;
	}
	
	FTransform GetJointWorldSpace()  { return this->GetComponentTransform(); }
	FTransform GetJointObjectSpace() { return this->GetComponentTransform().GetRelativeTransform(this->GetRootJoint()->GetComponentTransform()); }
	FTransform GetJointLocalSpace()  { return this->GetRelativeTransform(); }

	FBoxSphereBounds GetJointColliderBounds() { return FBoxSphereBounds(); }

	float GetJointLength() { return 0.0f; }

	bool GetIsRootJoint() { return false; }
	bool GetIsEndJoint() { return false; }
	bool HasParentJoint() { return false; }

	int GetCurrentFrame() { return -1; }
	int GetLastFrame() { return -1; }
	float GetCurrentAnimationLength() { return 0.0f; }

};
