// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "Runtime/Engine/Classes/Components/InstancedStaticMeshComponent.h"

#include "dmNodeComponent.h"
#include "dm_scene.h"

#include "dmInstanceComponent.generated.h"

UENUM(BlueprintType)
enum class FdmMeshMode : uint8 {
	MM_Empty			UMETA(DisplayName = "Empty"),
	MM_ProceduralMesh	UMETA(DisplayName = "Procedural Mesh"),
	MM_StaticMesh		UMETA(DisplayName = "Static Mesh"),
	MM_InstancedMesh	UMETA(DisplayName = "Instanced Mesh")
};

UENUM(BlueprintType)
enum FdmSimplePrimitive {
	BP_Cube			UMETA(DisplayName = "Cube"),
	BP_Rounded_Cube UMETA(DisplayName = "Rounded Cube"),
	BP_Sphere		UMETA(DisplayName = "Sphere"),
	BP_Cylinder		UMETA(DisplayName = "Cylinder"),
	BP_Torus		UMETA(DisplayName = "Torus"),
	BP_Capsule		UMETA(DisplayName = "Capsule")
};

USTRUCT(BlueprintType)
struct FdmMaterialParameters
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Metallic;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Specular;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString DiffuseMapUri;

	FdmMaterialParameters() { Metallic = 0.0f; Specular = 0.0f; DiffuseMapUri = ""; }
};

//class UdmNodeComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UdmInstanceComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	const dm_node* InstanceNode{ nullptr };

	UProceduralMeshComponent* ProceduralMesh{ nullptr };
	UStaticMeshComponent* StaticMesh{ nullptr };
	UInstancedStaticMeshComponent* InstancedMesh{ nullptr };
	//USplineGroupComponent..
	//UVoxelMeshComponent...
	//USdfMeshComponent...

	bool EnableMeshCollision;	

public:		
	// Sets default values for this component's properties
	UdmInstanceComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// [setup]
	void SetupFromDataStruct(const dm_node& InstanceNode_, dm_scene* Scene);
	//void UpdateFromDataStruct() {};
	//void UpdateFromDataStruct(instance& Instance, dm_scene* Scene) {};
	//void SetupFromDataStruct(instance& Instance, dm_scene* Scene) {};
	//void UpdateFromDataStruct(instance& Instance, dm_scene* Scene) {}
	void UpdateMesh(TArray<TArray<FVector>>& Positions);

	// [state]
	UFUNCTION(BlueprintCallable) FdmMeshMode GetMeshMode() {
		if (ProceduralMesh != nullptr) return FdmMeshMode::MM_ProceduralMesh;
		if (StaticMesh != nullptr) return FdmMeshMode::MM_StaticMesh;
		if (InstancedMesh != nullptr) return FdmMeshMode::MM_InstancedMesh;
		return FdmMeshMode::MM_Empty;
	};
	UFUNCTION(BlueprintCallable) UdmNodeComponent* GetOwningNode() { return Cast<UdmNodeComponent>(this->GetAttachParent()); };

	UFUNCTION(BlueprintCallable) void SetAsBasicPrimitive(FdmSimplePrimitive Primitive, FTransform Transform) {};

	// [visualization]
	UFUNCTION(BlueprintCallable) void ApplyWeightsAsVertexColors();
	UFUNCTION(BlueprintCallable) void SetInstanceMaterial(UMaterialInterface* Material_);
	UFUNCTION(BlueprintCallable) void SetInstanceMaterial_Section(UMaterialInterface* Material_, int SectionId);

	UFUNCTION(BlueprintCallable) FdmMaterialParameters GetMaterialParameters(int SectionId);
	UFUNCTION(BlueprintCallable) int GetNumberOfMeshSections() { return ProceduralMesh->GetNumSections(); }
		
};
