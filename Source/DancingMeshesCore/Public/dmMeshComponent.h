// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "Runtime/Engine/Classes/Components/InstancedStaticMeshComponent.h"

#include "dm_scene.h"

#include "dmMeshComponent.generated.h"

//enum FdmMeshMode {
//	MM_ProceduralMesh,
//	MM_StaticMesh,
//	MM_InstancedMesh
//};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UdmMeshComponent : public UProceduralMeshComponent //public USceneComponent	// [todo] temporarily made it proceduralmeshcomponent again, to not break old code
{
	GENERATED_BODY()

public:

	UProceduralMeshComponent* ProceduralMesh{ nullptr };
	UStaticMeshComponent* StaticMesh{ nullptr };
	UInstancedStaticMeshComponent* InstancedMesh{ nullptr };

	bool EnableMeshCollision;
	

public:		
	// Sets default values for this component's properties
	//UdmMeshComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// [setup]
	void SetupFromDataStruct(dm_instance& Instance, dm_scene* Scene) {};
	void UpdateFromDataStruct(dm_instance& Instance, dm_scene* Scene) {};
	void UpdateVertices(dm_array<dm_array<dm_vec3>>& UpdatedVertices) {};

	// [state]
	//FdmMeshMode GetMeshMode() {
	//	if (ProceduralMesh != nullptr) return FdmMeshMode::MM_ProceduralMesh;
	//	if (StaticMesh != nullptr) return FdmMeshMode::MM_StaticMesh;
	//	if (InstancedMesh != nullptr) return FdmMeshMode::MM_InstancedMesh;
	//}
		
};
