// Copyright Alexander Demets

#pragma once

// ue4
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/ActorComponent.h"
// dmcore-ue4
#include "dmSceneConstructor.h"
#include "dmSceneDriverComponent.h"
// dmcore-c
#include "dm_ozz.h"
// ubs
#include "dmAnimationDriverComponent.generated.h"


//For UE4 Profiler ~ Stat Group
DECLARE_STATS_GROUP(TEXT("DancingMeshes"), STATGROUP_DancingMeshes, STATCAT_Advanced);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UdmAnimationDriverComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UdmAnimationDriverComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	FSceneConstructorSettings Settings;
	// Playback animation controller. This is a utility class that helps with
	// controlling animation playback time.
	//ozz::sample::PlaybackController controller_;
	// Runtime skeleton.
	//ozz::animation::Skeleton skeleton_;
	// Runtime animation.
	//ozz::animation::Animation animation_;
	// Sampling cache.
	ozz::animation::SamplingJob::Context cache_;
	// Buffer of local transforms as sampled from animation_.
	dm_array<ozz::math::SoaTransform> locals_;
	// Buffer of model space matrices.
	dm_array<ozz::math::Float4x4> models_;

	dm_array<FTransform> joint_locations;

	dm_ozz_data* ozz_data{ nullptr };

	float time_ratio{ 0.0f };
	float previous_time_ratio{ 0.0f };
	float playback_speed{ 1.0f };
	bool is_playing{ false };
	bool is_looping{ true };

	bool auto_update{ true };
	UPROPERTY()
	bool reskin_mesh{ true };

	dm_animation* anim;
	//instance* instance;
	UdmSceneDriverComponent* SceneDriver{ nullptr };

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void Setup(UdmSceneDriverComponent* SceneDriver, bool AutoUpdate);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void Reset();
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void Update(float Dt, bool ReskinMesh);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void Play(FString AnimationId);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void PlayByIndex(int AnimationId);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void Stop(bool ResetTime);


	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void SetTimeRatio(float Time);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") float GetTimeRatio() { return time_ratio; ; }

	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void SetPlaybackSpeed(float Speed) { playback_speed = Speed;  }
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") float GetPlaybackSpeed() { return playback_speed; }

	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") void SetLooped(bool Looped) { is_looping = Looped; }
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") bool IsLooped() { return is_looping; }

	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Animation") TArray<FTransform>& GetJoints() { return joint_locations; }

	UFUNCTION(BlueprintPure, Category = "DM|Driver|Animation") bool IsPlaying() { return is_playing; }

};
