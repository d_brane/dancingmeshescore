// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/SceneComponent.h"
#include "ProceduralMeshComponent.h"

//#include "dm_assimp.h"
#include "dm_core.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "AssimpDriverComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UAssimpDriverComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UAssimpDriverComponent();

	//UPROPERTY(VisibleAnywhere) USceneComponent* ParentMeshComponent;
	UPROPERTY(VisibleAnywhere) UProceduralMeshComponent* MeshComponent;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	int32 SelectedVertex;
	int32 _meshCurrentlyProcessed;
	bool _addModifier;
	int _lastModifiedTime;
	bool _requiresFullRecreation;

	int32 _numberOfMeshSections;

	TArray<TArray<FVector>> _vertices;
	TArray<TArray<int32>> _indices;
	TArray<TArray<FVector>> _normals;
	TArray<TArray<FVector2D>> _uvs;
	TArray<TArray<FVector>> _tangents;
	TArray<TArray<FLinearColor>> _vertexColors;
	
	// Bones
	TArray<TArray<FString>> _boneNames;
	TArray<TArray<FMatrix>> _inverseBindMatrices;
	TArray<TArray<FTransform>> _boneTransform;
	TArray<TArray<uint32>> _boneWeightIndices;
	TArray<TArray<float>> _boneWeights;

	dm_scene _scene;

	void processNode(aiNode* Node, const aiScene* Scene);
	void postprocessNodes(const aiScene* Scene);
	void processMesh(aiMesh* Mesh, const aiScene* Scene);
	void processBones(aiMesh* Mesh, const aiScene* Scene);


public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") void Clear();
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") bool Save(const FString& File, FString& ErrorCode);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") bool Load(const FString& File, int& NumberOfSections, FString& ErrorCode);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") bool GetSection(int SectionIndex, TArray<FVector>& Vertices, TArray<int32>& Faces, TArray<FVector>& Normals, TArray<FVector2D>& UV, TArray<FLinearColor>& VertexColors, TArray<FVector>& Tangents);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") int32  GetNumberOfSections() const { return _numberOfMeshSections; }
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") void UpDownSection(int SectionId, float Time, TArray<FVector> &Vertices);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") UProceduralMeshComponent* DoMesh(AActor *Actor_, USceneComponent* ParentComponent, const FString& Name);


	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") void GetBones(int SectionIndex, TArray<FTransform>& BoneTransforms);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") void GetBoneNames(int SectionIndex, TArray<FString>& BoneNames);
	
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Assimp") void ConstructScene(USceneComponent* RootComponent, int SectionId, float AtTime);

};
