// Copyright by Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "ProceduralMeshComponent.h"

#include "GltfRuntimeMesh.generated.h"

struct cgltf_data;
struct dm_scene;

struct FGltfMesh {
//	cgltf_data* GltfData;
//	dm_scene* DmScene;
};


// maybe rename UProceduralGltfMesh
// URuntimeGltfComponent
// UGltfMeshComponent .. ya
// UGltfComponent .. na
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UGltfRuntimeMesh : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGltfRuntimeMesh();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable)
	bool LoadFromFile(FString Filepath, bool LoadTextures, bool LoadAnimations);
	UFUNCTION(BlueprintCallable)
	void LoadFromString(FString GltfString);
	UFUNCTION(BlueprintCallable)
	void Unload();

	UFUNCTION(BlueprintCallable)
	int GetNumberOfAnimations() { return 0; }
	UFUNCTION(BlueprintCallable)
	TArray<FString> GetAnimationClipNames() { return TArray<FString>(); }
	UFUNCTION(BlueprintCallable)
	float GetAnimationClipLength(FString AnimationClip) { return 0.0f; } // Duration?

	UFUNCTION(BlueprintCallable)
	int GetNumberOfMeshSections() { return 0; }
	//UFUNCTION(BlueprintCallable)
	//void GetCurrentMeshData(int MeshSection, TArray<FVector>& Vertices);

	UFUNCTION(BlueprintCallable)
	bool SetAnimation(FString AnimationClip, float Time, bool UpdateMesh) { return true; }
	UFUNCTION(BlueprintCallable)
	void UpdateMesh(float DeltaTime) { ; }
	

	UFUNCTION(BlueprintCallable)
	void UpdateMeshSection(int MeshSectionId, FString AnimationClip, float Time, bool MapTimeToDuration, TArray<FVector>& Vertices, TArray<int>& Indices, TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FLinearColor>& VertexColors, TArray<FVector>& Tangents);

	UFUNCTION(BlueprintCallable)
	void UpdateProceduralMeshComponent(UProceduralMeshComponent* MeshComponent, FString AnimationClip, float AtTime, bool IsFirstInit);

protected:
	FGltfMesh GltfMesh;
};
