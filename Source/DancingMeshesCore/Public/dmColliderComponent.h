// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
//#include "Components/PrimitiveComponent.h"
#include "Components/StaticMeshComponent.h"
#include "dm_scene.h"

#include "dmColliderComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UdmColliderComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UdmColliderComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	

	void SetupFromDataStruct(const dm_bounds Collider);
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
