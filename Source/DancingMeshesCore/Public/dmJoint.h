// Copyright Alexander Demets

#pragma once
// ue4
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
// dmcore-ue4
#include "dmColliderComponent.h"
//#include "dmNodeComponent.h"
// dmcore-c
#include "dm_scene.h"
// ubs
#include "dmJoint.generated.h"


enum class EColliderPivotType : uint8
{
	Origin,	// at 0,0,0 of mesh
	Center,	// at (max_bounds - min_bounds) / 2
	Custom
};

enum class EColliderType : uint8
{
	None,
	Box,
	Sphere,
	Capsule,
	Mesh
};

struct SJointCollider
{
	EColliderPivotType PivotType;
	FVector	CustomPivot;

	EColliderType ColliderType;
	FVector	ColliderScale;
};

enum class EJointType : uint8
{
	Root,
	Normal,
	SubBase,	// Tree branching points \o/ (subscore being joint in question)
	EndPoint,
	EndEffector	// think about if actually a joint, or better as a UNodeComponent ??
};

//template<typename T, class B> struct AnimationKey { enum class KeyType { Linear, Step, Bezier } Type; TMap<T, B> Key; };
//struct SVectorKey {};
//struct SQuatKey {};
enum class EAnimationType { Linear, Step, Bezier };

struct SSparseJointAnimationData
{
	EAnimationType Type;
	TSparseArray<FVector> Positions{};
	TBitArray<FDefaultBitArrayAllocator> PositionKeys{};
	TSparseArray<FQuat> Rotations{};
	TBitArray<FDefaultBitArrayAllocator> RotationKeys{};
	TSparseArray<FVector> Scales{};
	TBitArray<FDefaultBitArrayAllocator> ScaleKeys{};
};
struct SJointAnimationData
{
	EAnimationType Type;
	TMap<uint32, FVector> Positions{};
	TMap<uint32, FQuat> Rotations{};
	TMap<uint32, FVector>Scales{};
};
struct SJointTimedAnimationData
{
	EAnimationType Type;
	TMap<float, FVector> Positions{};
	TMap<float, FQuat> Rotations{};
	TMap<float, FVector>Scales{};
};

enum class EJointRotationConstraint : uint8
{
	None,
	Pivot,	// Neck, Spine (rot: Z, fixed: XY)
	Hinge,	// Elbow (rot: Y or X, fixed: Z & X or Y )
	Saddle,	// Wrist (rot: XY, fixed: Z)
	BallAndSocket, // Foot (rot: XYZ, fixed: pos)
};

enum class EJointTranslationContraint : uint8
{
	None,	// free translation around XYZ
	X,
	Y,
	Z,
	XY,
	XZ,
	YZ,
	XYZ		// full lock on all axis
};

struct SJointConstraint
{
	EJointRotationConstraint RotationConstraint;
	FVector2D AngleX, AngleY, AngleZ;

	EJointTranslationContraint TranslationConstraint;
	FVector2D PositionX, PositionY, PositionZ;
};

// extra:
enum class EJointSimulationType : uint8
{
	Fixed, Animated, Simulated, AnimatedPlusSimulated
};


struct SJointData
{
	FString Name;
	FTransform DefaultPose;

	SJointConstraint Constraint;
	SJointCollider Collider;
	SJointAnimationData AnimationData;
	EJointSimulationType SimulationType;

	// computed:
	float Length;
	int MaxFrameNumber;
	EJointType Type;
	uint32 Hash;
};

// class intern enums:
UENUM(BlueprintType)
enum class EJointVisualizationMode : uint8
{
	Invisible,
	Joint,
	Bone,
	Collider,
	Mesh,
	JointBone,
	JointBoneCollider,
	JointBoneColliderMesh,
	JointBoneMesh,
	ColliderMesh
};

enum class EJointState : uint8
{
	DefaultPose,
	Playing,
	Editing,	// needs to be own state? would be activated when moving joint around, or live recording joint for example
	Stopped
};


class UdmNodeComponent;

UCLASS()
class DANCINGMESHESCORE_API UdmJoint : public UObject
{
	GENERATED_BODY()

protected:
	//SJointData JointData;
	//EJointVisualizationMode VisualizationMode;
	//EJointState State;
	//int CurrentFrame;

public:
	UdmJoint() {}

	const dm_node* node_{ nullptr };
	const dm_joint* joint_{ nullptr };
	//UdmNodeComponent* owner_{ nullptr };

	UdmColliderComponent* Collider{ nullptr };

	// [setup]
	void SetupFromDataStruct(UdmNodeComponent* Owner, const dm_node* Node, const dm_joint* Joint, bool CreateBounds) {
		node_ = Node;
		joint_ = Joint;
		//owner_ = Owner;
		if (CreateBounds) {
			Collider = NewObject<UdmColliderComponent>(this);
			Collider->SetupAttachment((USceneComponent*)Owner);
			Collider->SetRelativeTransform(FTransform::Identity);
			Collider->RegisterComponent();
			Collider->SetupFromDataStruct(joint_->collision_bounds);
		}
	}

public:

	// [state]
	bool HasAnimations() { return true; }
	bool HasChildrenJoints() { return false; }
	bool HasCollider() { return false; }
	bool HasScripts() { return false; }
	bool HasBounds() { return false; }	// does not have bounding box, if simple node with no attachments

	bool HasMesh() { return false; }
	bool HasSkinnedMesh() { return false; }
	bool HasMorphedMesh() { return false; }
	bool HasAnimatedMesh() { return false; }
	bool HasProceduralMesh() { return false; }

	bool HasSplines() { return false; }
	bool HasAnimatedSplines() { return false; }

	bool HasVoxels() { return false; }
	bool HasSkinnedVoxels() { return false; }
	bool HasAnimatedVoxels() { return false; }

	bool HasSdf() { return false; }
	bool HasAnimatedSdf() { return false; }

	bool HasSprites() { return false; }
	bool HasAnimatedSprites() { return false; }

	bool HasKevlinets() { return false; }
	bool HasAnimatedKevlinets() { return false; }


	// [visualization]
	//void SetVisualizationMode(EJointVisualizationMode Mode) { VisualizationMode = Mode; }
	//EJointVisualizationMode GetVisualizationMode() { return VisualizationMode; }
	void SetMaterials(float JointMaterial, float BoneMaterial, float ColliderMaterial) {}
	void SetJointMaterial(float JointMaterial) {}
	void SetBoneMaterial(float BoneMaterial) {}
	void SetColliderMaterial(float ColliderMaterial) {}
	
};
