// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "dmNodeComponent.h"
#include "dmJointComponent.h"
#include "dmInstanceComponent.h"

#include "dmSceneConstructor.h"
#include "dmSceneDriverComponent.h"

#include "dmBaseActor.generated.h"

UCLASS()
class DANCINGMESHESCORE_API AdmBaseActor : public AActor
{
	GENERATED_BODY()

protected:
	//TArray<UdmNodeComponent *> RootNodes;
	//TArray<UdmJointComponent*> RootJoints;
	UdmJointComponent* BaseJoint;

public:	
	// Sets default values for this actor's properties
	AdmBaseActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	USceneComponent* createSceneComponent( UClass* CompClass, const FVector& Location, const FRotator& Rotation, const FName& AttachSocket = NAME_None)
	{
		FName YourObjectName("Hiiii");
		
		//CompClass can be a BP
		USceneComponent* NewComp = nullptr;
		//NewComp = ConstructObject<USceneComponent>(CompClass, Settings.BaseActor, YourObjectName);
		NewComp = NewObject<USceneComponent>(this);
		
		//NewComp = NewNamedObject<USceneComponent>(this, YourObjectName);
		if (!NewComp)
		{
			return nullptr;
		}
		//~~~~~~~~~~~~~

		//NewComp->RegisterComponent();        //You must ConstructObject with a valid Outer that has world, see above	 
		//NewComp->SetWorldLocation(Location);
		//NewComp->SetWorldRotation(Rotation);
		//NewComp->AttachTo(Settings.BaseActor->GetRootComponent(), NAME_None, EAttachLocation::KeepWorldPosition);
		//could use different than Root Comp
		return nullptr;
	}

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UdmJointComponent* GetBaseJoint() { return BaseJoint; }
	//TArray<UdmJointComponent*> GetRootJoints() { return RootJoints; }
	UdmSceneDriverComponent* GetSceneDriver() { return nullptr; }

	UdmJointComponent* CreateJointComponent(UdmJointComponent* Parent) { return nullptr; }
	UdmNodeComponent* CreateNodeComponent(UdmNodeComponent* Parent) { return nullptr; }

	bool HasSkinnedMeshes() { return false; }
	bool HasAnimations() { return false; }


};
