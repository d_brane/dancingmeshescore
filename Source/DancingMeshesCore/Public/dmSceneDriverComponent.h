// Copyright Alexander Demets

#pragma once

// ue4
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
// dmcore-ue4
#include "dmNodeComponent.h"
//#include "dmSceneConstructor.h"
// dmcore-c
//#include "dm_scene.h"
// ubs
#include "dmSceneDriverComponent.generated.h"

//struct FSceneConstructorSettings;

struct dm_scene;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UdmSceneDriverComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UdmSceneDriverComponent();
	~UdmSceneDriverComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	dm_scene* Scene{ nullptr };
	USceneComponent* SceneRoot{ nullptr };
	//FSceneConstructorSettings Settings;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") bool Load(const FString& FileIn);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") bool Save(const FString& FileOut);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") bool Export(const FString& FileOut);
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") bool Close(bool RemoveSceneComponents);

	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") bool ViewportToScene();
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") bool SceneToViewport(USceneComponent* RootComponent);

	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") TArray<UdmNodeComponent*> GetNodeComponents();
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") TArray<UdmNodeComponent*> GetJointNodeComponents();
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") TArray<UdmInstanceComponent*> GetInstanceComponents();
	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") TArray<UdmColliderComponent*> GetColliderComponents();

	UFUNCTION(BlueprintCallable, Category = "DM|Driver|Scene") bool HasAnimations();

	//FSceneConstructorSettings GetSceneSettings() { return Settings; };
	dm_scene*    GetScene() { return Scene; };
	USceneComponent* GetRoot()  { return SceneRoot; };
};
