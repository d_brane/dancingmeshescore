// Copyright Alexander Demets

#pragma once

// ue4
#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
// dmcore-ue4
#include "dmBaseActor.h"
#include "dmInstanceComponent.h"
// dmcore-c
#include "dm_scene.h"
// ubs
//#include "dmSceneConstrutor.generated.h"

/*
struct FNode : node {
	USceneComponent *ViewElement;
	// or
	UdmSceneElement *ViewElement;
};

struct FCamera : camera {
	UCameraComponent *ViewElement;
};

*/

// ISceneConstructor can be implemented and depending on implementation:
// 1) SceneGraphSceneConstructor: creates scene using default ue4 scenegraph: nice for editor mode
//    + flexible and editable scenegraph
//    - slower, more draw calls, more general overhead
// 2) BatchedSceneConstructor: creates geometry all in one batch and creates scene using one component
//    + super fast
//    - less flexibility on edits
//    + good for play mode
//    - bad for edit mode
//


//UENUM() namespace EConstructionMode { enum Mode { CM_OnlyUE4, CM_OnlyDM, CM_Both }; }
//
//USTRUCT(BlueprintType)
//struct FSceneConstructorSettings
//{
//	GENERATED_USTRUCT_BODY()
//	// should actions event only DM_RUNTIME_UNREAL scenegraph, only dm_scene or both?
//	//ConstructionMode Mode{ ConstructionMode::CM_Both };
//	UPROPERTY() TEnumAsByte<EConstructionMode::Mode> Mode_{ EConstructionMode::Mode::CM_Both };
//	dm_scene* Scene{ nullptr };
//	UPROPERTY() AActor* BaseActor{ nullptr };
//	UPROPERTY() USceneComponent* RootComponent{ nullptr };
//};

struct FSceneConstructorSettings
{
	// should actions event only DM_RUNTIME_UNREAL scenegraph, only dm_scene or both?
	enum ConstructionMode { CM_OnlyUE4, CM_OnlyDM, CM_Both };
	ConstructionMode Mode{ ConstructionMode::CM_Both };
	dm_scene* Scene{ nullptr };
	AActor* BaseActor { nullptr };
	USceneComponent* RootComponent { nullptr };
};

// interface for the basics of loading and constructing a dm_scene
class IdmSceneConstructor 
{
public:
	virtual void CreateSceneFromFile(const FString& File, USceneComponent& Base) = 0;
	virtual void CreateViewportFromScene(dm_scene* Scene, AActor* Base, USceneComponent* Root) = 0;
	virtual void CreateSceneFromViewport(const AActor* Base, const USceneComponent* Root, dm_scene*& Scene) = 0;

};
// inteface for dmcore like c++ api for editing scene
class IdmEditableSceneConstructor
{
public:
	void AddNode(FString& Name, FTransform NodeTransform, FString& Parent, int MeshType, FTransform MeshTransform, FString& OutNodeId) {};
	void AddNode(const FSceneConstructorSettings& Settings, const dm_node& Node) {};
	void DeleteNode(FString& Id, bool IncludeChildren) {};
};


class DANCINGMESHESCORE_API dmSceneConstructor // : public UObject
{
private:
	dm_scene* Scene = nullptr; // not sure if should hold dm_scene state or be static class
	// if static class, don't locally save, but copy by reference in function arguments
	//FSceneConstructorSettings Settings;

	//static USceneComponent* SpawnNewComponent(AActor* Owner, USceneComponent* ParentComponent, UClass* ComponentClassToSpawn, const FTransform& SpawnLocation, const FString& ComponentName);

	//static UdmNodeComponent* createNodeComponent(const FSceneConstructorSettings& Settings, const node& Node);
	//static UdmInstanceComponent* createInstanceComponent(const FSceneConstructorSettings& Settings, const node& InstanceNode);
	//static UdmMeshComponent* createMeshComponent(const FSceneConstructorSettings& Settings, const mesh& Mesh, const node& MeshNode);
	//static UdmJointComponent* createJointComponent(const FSceneConstructorSettings& Settings, const joint& Joint, const node& JointNode);

	//static USceneComponent* createSceneComponent(FSceneConstructorSettings* Settings, UClass* CompClass, const FVector& Location, const FRotator& Rotation, const FName& AttachSocket = NAME_None);

	//bool fix_node_hierarchy(dm_scene* scene, bool flushParentIds, bool recomputeChildrenPointer);
	//static bool fixNodeHierachy(const FSceneConstructorSettings& Settings, bool flushParentIds);

public:
	dmSceneConstructor();
	~dmSceneConstructor();

	void CreateSceneFromFile(const FString& File, USceneComponent& Base) {
		//Scene = Load(File);

		// for each Scene->Node create dmJoint/dmNode (depending on node type) a nd add to Base
		// solve hierachy based on Scene->Node hierachy
		// 
		// for each Scene->Mesh create dmMesh and add to dmNode/dmJoint
		//
		// for each Scene->SkinnedMesh
	}

	// NODES
	//void AddNode(node* Node) {};
	void AddNode(FString& Name, FTransform NodeTransform, FString& Parent, int MeshType, FTransform MeshTransform, FString& OutNodeId) {};
	void AddNode(const FSceneConstructorSettings& Settings, const dm_node& Node);
	void DeleteNode(FString& Id, bool IncludeChildren) {};
	void MoveNode(FString& Name, FTransform DeltaTransform, bool IncludeChildren) {};
	void CloneNode(FString& InNode, FString& OutNodeId, bool IncludeChildren) {};
	void FlipNode(FString& Name, int FlipAxis) {};

	// MESH
	void AddMesh() {};
	void DeleteMesh() {};
	void UpdateMesh(const FSceneConstructorSettings& Settings, dm_node& Node, TArray<TArray<FVector>>& Positions);
	void MoveMesh() {};

	// ANIMATION
	void AddBone() {};
	void DeleteBone() {};

	// IMPORT / EXPORT
	void CreateViewportFromScene(dm_scene* Scene, AActor* Base, USceneComponent* Root);
	void CreateSceneFromViewport(const AActor* Base, const USceneComponent* Root, dm_scene*& Scene);

	//void UpdateViewportFromScene();
	//void UpdateSceneFromViewport();

	//void ToScene()
	//void DmSceneToUe4(dm_scene* Scene, AActor *Base, USceneComponent* Root) {};
	//void Ue4ToDmScene(AActor* Base, USceneComponent* Root, dm_scene* Scene) {};

	// those fit maybe better in SceneSync or SceneDriver
	/** Creates dm_scene-graph view from dm_scene */
	//void CreateViewFromModel(dm_scene* Scene, USceneComponent* Parent, bool ShouldRemoveAllChildren) {};
	//void CreateModelFromView() {};
	//void UpdateNodesFromView(USceneComponent* StartNode, dm_scene* SceneToUpdate) {};
	//void UpdateMeshFromView()


	//void UpdateViewFromModel() {};

};
