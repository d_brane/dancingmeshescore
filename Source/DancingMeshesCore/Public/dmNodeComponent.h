// Copyright Alexander Demets

#pragma once

// ue4
// dmcore-ue4
// dmcore-c
// ubs

// ue4
#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
// dmcore-ue4
#include "dmJoint.h"
#include "dmSkin.h"
// dmcore-c
#include "dm_scene.h"
// ubs
#include "dmNodeComponent.generated.h"

//class UdmJointComponent;

// [note][ademets]
// does dmNodecomponent even need to know about node? or is it bad to isolate it away?

class UdmInstanceComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UdmNodeComponent : public USceneComponent
{
	GENERATED_BODY()

protected:
	//// -[data]---------------------------------------------------------------------------------------------------------
	//
	//UdmJoint* Joint{ nullptr };
	////UdmSkin* Skin { nullptr }; //skin* Skin_{ nullptr };
	
public:
	const dm_node* _node{ nullptr };

	UdmJoint* Joint{ nullptr };
	UdmSkin* Skin { nullptr }; //skin* Skin_{ nullptr };
	UdmInstanceComponent* Instance{ nullptr };


	FString _parent{};
	

	// -[ctor]---------------------------------------------------------------------------------------------------------
	// Sets default values for this component's properties
	UdmNodeComponent();
	//UdmNodeComponent(node* Node) {}

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// -[setup]--------------------------------------------------------------------------------------------------------
	void SetupFromDataStruct(dm_node& Node, dm_scene* Scene, bool AddChildren) {}
	void Setup(FName Name, FTransform Transform, UdmNodeComponent* ParentNode) {}
	//void SetupFromDataStruct(SJointData Data) {}
	//void Setup(FString Name, SJointCollider Collider, FVector ColliderStartScale, EJointVisualizationMode VisMode) {}
	void UpdateFromDataStruct(dm_node& Node, dm_scene* Scene, bool UpdateChildren) {}
	void RenameNode(const FString& Name) {}
	void ResetNode() {}
	void DeleteNode() {}

	// -[tick]---------------------------------------------------------------------------------------------------------
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// -[state]--------------------------------------------------------------------------------------------------------
	UFUNCTION(BlueprintPure) bool IsJoint() { if (_node) { if (_node->joint) return true; else return false; } else return false; }
	UFUNCTION(BlueprintPure) bool IsRoot() { if (_node) { if (_node->parent) return false; else return true; } else return false; }

	UFUNCTION(BlueprintPure) bool HasAnimations() { return true; }
	UFUNCTION(BlueprintPure) bool HasChildrenNodes() { return false; }
	UFUNCTION(BlueprintPure) bool HasCollider() { return false; }
	UFUNCTION(BlueprintPure) bool HasBounds() { return false; }	// does not have bounding box, if simple node with no attachments
	UFUNCTION(BlueprintPure) bool HasScripts() { return false; }

	UFUNCTION(BlueprintPure) bool HasInstance() { if (Instance) return true; else return false; }
	UFUNCTION(BlueprintPure) bool HasMesh() { return HasInstance(); }
//  UFUNCTION(BlueprintPure) bool HasBoundsMesh() { return false; }
	UFUNCTION(BlueprintPure) bool HasSkinnedMesh() { return false; }
	UFUNCTION(BlueprintPure) bool HasMorphedMesh() { return false; }
	UFUNCTION(BlueprintPure) bool HasAnimatedMesh() { return false; }
	UFUNCTION(BlueprintPure) bool HasProceduralMesh() { return false; }

	UFUNCTION(BlueprintPure) bool HasSplines() { return false; }
	UFUNCTION(BlueprintPure) bool HasAnimatedSplines() { return false; }

	UFUNCTION(BlueprintPure) bool HasVoxels() { return false; }
	UFUNCTION(BlueprintPure) bool HasSkinnedVoxels() { return false; }
	UFUNCTION(BlueprintPure) bool HasAnimatedVoxels() { return false; }

	UFUNCTION(BlueprintPure) bool HasSdf() { return false; }
	UFUNCTION(BlueprintPure) bool HasAnimatedSdf() { return false; }

	UFUNCTION(BlueprintPure) bool HasSprites() { return false; }
	UFUNCTION(BlueprintPure) bool HasAnimatedSprites() { return false; }

	UFUNCTION(BlueprintPure) bool HasKevlinets() { return false; }
	UFUNCTION(BlueprintPure) bool HasAnimatedKevlinets() { return false; }

	//-[data accessors]------------------------------------------------------------------------------------------------
	UFUNCTION(BlueprintPure) UdmJoint* GetJoint() { return Joint; }
	UFUNCTION(BlueprintPure) UdmInstanceComponent* GetInstance() { return Instance; }
	void GetCamera() { return; }
	void GetSkin() { return; }
	void GetEnvironment() { return; }
	
	//UStaticMesh* GetJointMesh() { return nullptr; }
	//SJointData GetJointData() { return JointData; }

	//SJointAnimationData GetAnimationData() { return JointData.AnimationData; }
	//SJointConstraint GetJointConstraint() { return JointData.Constraint; }
	//SJointCollider GetJointCollider() { return JointData.Collider; }

	//FTransform GetDefaultPose() { return JointData.DefaultPose; }

	void UpdateDefaultPose() {}
	void ResetToDefaultPose() {}

	// -[manipulation]-------------------------------------------------------------------------------------------------
	FTransform SolveTailPositionWorldInput(FVector WorldPosition, bool FixedParentOffsetDelta = true) { return FTransform::Identity; }

	FTransform SolveHeadPositionModelInput(FVector ModelPosition, bool FixedPosition = false) { return FTransform::Identity; }
	//FTransform SolveHeadPositionRelativeInput(FVector RelativePosition, bool LengthFixed = true) { }

	//void SolveTailPosition

	// -[animation]----------------------------------------------------------------------------------------------------
	void RemoveKey(int FrameNumber) {}
	void RemovePositionKey(int FrameNumber) {}
	void RemoveRotationKey(int FrameNumber) {}
	void RemoveScaleKey(int FrameNumber) {}

	void SetTime(float Time, int FramesPerSecond, bool Truncate) {}
	void SetFrame(int Frame, bool Truncate) {}
	void NextFrame(bool Truncate) {}
	void PreviousFrame(bool Truncate) {}

	void RecordFrame(int Frame) {}
	void RecordPosition(int Frame) {}
	void RecordRotation(int Frame) {}
	void RecordScale(int Frame) {}

	// -[transform]----------------------------------------------------------------------------------------------------
	void MoveJointTail(FVector NewPosition, bool MoveParentHead) {}
	void MoveJointHead(FVector NewPosition, bool MoveChildrenTail) {}
	void MoveJoint(FVector NewPosition, bool MoveChildren) {}
	void RotateJoint(FQuat NewRotation, bool RotateChildren) {}
	void RotateJointDelta(FQuat DeltaRotation, bool RotateChildren) {}
	void ScaleJoint(FVector NewScale, bool ScaleChildren) {}
	//void Move(FTransform NewTransform, bool UpdateChildren) {}

	// -[visualization]------------------------------------------------------------------------------------------------
	//void SetVisualizationMode(EJointVisualizationMode Mode) { VisualizationMode = Mode; }
	//EJointVisualizationMode GetVisualizationMode() { return VisualizationMode; }

	UFUNCTION(BlueprintCallable) void SetColor(FLinearColor NodeColor) { _node->joint->_color = NodeColor; }
	UFUNCTION(BlueprintPure) FLinearColor GetColor() { if (IsJoint()) return _node->joint->_color; else return FLinearColor::White; }
	void SetMaterials(float JointMaterial, float BoneMaterial, float ColliderMaterial) {}
	void SetJointMaterial(float JointMaterial) {}
	void SetBoneMaterial(float BoneMaterial) {}
	void SetColliderMaterial(float ColliderMaterial) {}

	// -[computed values]----------------------------------------------------------------------------------------------
	UdmNodeComponent* GetRootNode() { return nullptr; }
	UdmNodeComponent* GetParentNode() { return nullptr; }

	TArray<UdmNodeComponent*> GetChildrenNodes()
	{
		TArray<USceneComponent*> ChildrenAll;
		this->GetChildrenComponents(false, ChildrenAll);

		TArray<UdmNodeComponent*> ChildrenJoints;
		for (int i = 0; i < ChildrenAll.Num(); ++i) {
			if (ChildrenAll[i]->GetClass()->StaticClass == UdmNodeComponent::StaticClass)
				ChildrenJoints.Add((UdmNodeComponent*)ChildrenAll[i]);
		}
		return ChildrenJoints;
	}

	FTransform GetNodeWorldSpace() { return this->GetComponentTransform(); }
	FTransform GetNodeObjectSpace() { return this->GetComponentTransform().GetRelativeTransform(this->GetRootNode()->GetComponentTransform()); }
	FTransform GetNodeLocalSpace() { return this->GetRelativeTransform(); }

	FBoxSphereBounds GetNodeColliderBounds() { return FBoxSphereBounds(); }

	float GetJointLength() { return 0.0f; }

	bool GetIsEndNode() { return false; }
	//bool HasParentNode() {  }

	int GetCurrentFrame() { return -1; }
	int GetLastFrame() { return -1; }
	float GetCurrentAnimationLength() { return 0.0f; }
};
