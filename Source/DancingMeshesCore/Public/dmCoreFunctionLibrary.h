// Copyright by Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "dmCoreFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class DANCINGMESHESCORE_API UdmCoreFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
