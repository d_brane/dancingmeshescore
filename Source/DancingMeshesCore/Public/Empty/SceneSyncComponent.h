// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SceneSyncComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API USceneSyncComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USceneSyncComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void OnSceneDirty() {};	// can be listened to, for updates (for example: bone mesh drawing overlay)
	bool IsSceneDirty() { return false; }
	
};
