// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "dmBoneComponent.generated.h"

enum EBoneType
{
	BT_None,
	BT_Cube,
	BT_Diamond,
	BT_Cylinder
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UdmBoneComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UdmBoneComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
