// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AnimationRecorderComponent.generated.h"


//[note][ademets] think about merging with AnimationEditor, not sure why it needs to be its own component?

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UAnimationRecorderComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAnimationRecorderComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
