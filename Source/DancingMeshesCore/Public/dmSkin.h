// Copyright Alexander Demets

#pragma once
// ue4
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
// dmcore-ue4
#include "dmJoint.h"
// dmcore-c
#include "dm_scene.h"
// ubs
#include "dmSkin.generated.h"

/**
 * 
 */
UCLASS()
class DANCINGMESHESCORE_API UdmSkin : public UObject
{
	GENERATED_BODY()

public:
	dm_skin* skin_{ nullptr };

	UFUNCTION(BlueprintCallable) void SetVisualizationMode(EJointVisualizationMode Vis) {};
	UFUNCTION(BlueprintCallable) void Reset() {};
	//UFUNCTION(BlueprintCallable) void SetupFromDataStruct(skin* skin) {};
	
};
