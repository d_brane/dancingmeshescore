// Copyright by Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "dm_imgui.h"
//#include "ImGuiDelegates.h"

#include "dmImGuiTest.generated.h"

#define IMGUI_WITH_DELEGATES 0


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DANCINGMESHESCORE_API UdmImGuiTest : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UdmImGuiTest();

	virtual void BeginDestroy() override;

	int32 ShowDemoWindowMask = 0;
	int32 ShowAnotherWindowMask = 0;

	int32 DemoWindowCounter = 0;
	uint32 LastDemoWindowFrameNumber = 0;
	int ContextIndex = 1;

	ImVec4 ClearColor;
	

protected:
	// Called when the game starts

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "DM|IMGUI") void InitImGui();
	UFUNCTION(BlueprintCallable, Category = "DM|IMGUI") void ShutdownImGui();

#if DM_WITH_IMGUI && IMGUI_WITH_DELEGATES
	//void ImGuiTick();
	//static void ImGuiMultiContextTick();

	//FImGuiDelegateHandle ImGuiTickHandle;
	//static FImGuiDelegateHandle ImGuiMultiContextTickHandle;
#endif // DM_WITH_IMGUI
		
};
