@echo off

set ASSIMP_PATH=%ASSIMP_DIR%
set CMAKE_PATH="cmake"
set ANDROID_NDK_PATH=%NDKRoot%
set ANDROID_CMAKE_PATH=%NDKROOT%/build/cmake

pushd %ASSIMP_PATH%
git checkout master
git pull
:: git apply fix_pi_compile.patch
popd

pushd %ASSIMP_PATH%
rmdir /s /q build_android
mkdir build_android
cd build_android

%CMAKE_PATH% .. ^
  -GNinja ^
  -DCMAKE_BUILD_TYPE=Release ^
  -DCMAKE_TOOLCHAIN_FILE=%ANDROID_CMAKE_PATH%/android.toolchain.cmake ^
  -DANDROID_TOOLCHAIN_NAME=aarch64-linux-android-clang ^
  -DCMAKE_SYSTEM_NAME=Android ^
  -DANDROID_NDK=%ANDROID_NDK_PATH% ^
  -DANDROID_NATIVE_API_LEVEL=latest ^
  -DANDROID_TOOLCHAIN=clang ^
  -DASSIMP_ANDROID_JNIIOSYSTEM=ON ^
  -DANDROID_FORCE_ARM_BUILD=TRUE ^
  -DCMAKE_ANDROID_ARCH_ABI=arm64-v8a ^
  -DANDROID_PLATFORM=android-29 ^
  -DASSIMP_BUILD_ZLIB=ON ^
  -DBUILD_SHARED_LIBS=OFF ^
  -DANDROID_STL=c++_shared ^
  -DASSIMP_BUILD_TESTS=OFF ^
  -DCMAKE_BUILD_TYPE=Release

%CMAKE_PATH% --build .
popd

xcopy %ASSIMP_PATH%\build_android\lib\libassimp.a                .\lib\android\ /Y
xcopy %ASSIMP_PATH%\build_android\port\AndroidJNI\libandroid_jniiosystem.a   .\lib\android\ /Y
xcopy %ASSIMP_PATH%\build_android\contrib\zlib\libzlibstatic.a            .\lib\android\ /Y
xcopy %ASSIMP_PATH%\build_android\lib\libdraco.a .\lib\android\ /Y

PAUSE

:: flag to disable exceptions:
:: -DCMAKE_CXX_FLAGS=-fno-exceptions