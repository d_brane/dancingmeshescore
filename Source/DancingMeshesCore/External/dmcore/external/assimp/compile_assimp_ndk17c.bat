@echo off

set ASSIMP_PATH=%ASSIMP_DIR%
set CMAKE_PATH="cmake"
set ANDROID_NDK_PATH=%NDKRoot%
set ANDROID_CMAKE_PATH=%NDKROOT%/build/cmake

pushd %ASSIMP_PATH%

git checkout master
git pull
git checkout local_fixes
git rebase master

rmdir /s /q build_vs
mkdir build_vs
cd build_vs

%CMAKE_PATH% .. ^
  -DCMAKE_SYSTEM_NAME=Windows ^
  -DASSIMP_BUILD_ZLIB=ON ^
  -DBUILD_SHARED_LIBS=OFF ^
  -DASSIMP_BUILD_TESTS=OFF

%CMAKE_PATH% --build . --config RelwithDebInfo

popd

pushd %ASSIMP_PATH%

rmdir /s /q build_android
mkdir build_android
cd build_android

%CMAKE_PATH% .. ^
  -GNinja ^
  -DCMAKE_BUILD_TYPE=Release ^
  -DCMAKE_TOOLCHAIN_FILE=%ANDROID_CMAKE_PATH%\android.toolchain.cmake ^
  -DANDROID_TOOLCHAIN_NAME=aarch64-linux-android-clang ^
  -DCMAKE_SYSTEM_NAME=Android ^
  -DANDROID_NDK=%ANDROID_NDK_PATH% ^
  -DANDROID_NATIVE_API_LEVEL=latest ^
  -DANDROID_TOOLCHAIN=clang ^
  -DASSIMP_ANDROID_JNIIOSYSTEM=ON ^
  -DANDROID_FORCE_ARM_BUILD=TRUE ^
  -DCMAKE_ANDROID_ARCH_ABI=arm64-v8a ^
  -DANDROID_PLATFORM=android-28 ^
  -DASSIMP_BUILD_ZLIB=ON ^
  -DBUILD_SHARED_LIBS=OFF ^
  -DANDROID_STL=gnustl_shared ^
  -DASSIMP_BUILD_TESTS=OFF ^
  -DCMAKE_BUILD_TYPE=Release

%CMAKE_PATH% --build .

popd

xcopy %ASSIMP_PATH%\include .\include /e /Y
xcopy %ASSIMP_PATH%\build_vs\include .\include /e /Y

copy %ASSIMP_PATH%\build_vs\lib\RelWithDebInfo\assimp-vc142-mt.lib .\lib\win64\assimp-vc142-mt.lib
copy %ASSIMP_PATH%\build_vs\lib\RelWithDebInfo\zlibstatic.lib      .\lib\win64\zlibstatic.lib
copy %ASSIMP_PATH%\build_vs\lib\RelWithDebInfo\IrrXML.lib          .\lib\win64\IrrXML.lib

copy %ASSIMP_PATH%\build_android\lib\libassimp.a                .\lib\android\libassimp.a
copy %ASSIMP_PATH%\build_android\lib\libandroid_jniiosystem.a   .\lib\android\libandroid_jniiosystem.a
copy %ASSIMP_PATH%\build_android\lib\libzlibstatic.a            .\lib\android\libzlibstatic.a
copy %ASSIMP_PATH%\build_android\lib\libIrrXML.a                .\lib\android\libIrrXML.a

PAUSE

:: flag to disable exceptions:
:: -DCMAKE_CXX_FLAGS=-fno-exceptions