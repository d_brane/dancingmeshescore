@echo off

set ASSIMP_PATH=%ASSIMP_DIR%
set CMAKE_PATH="cmake"
set ANDROID_NDK_PATH=%NDKRoot%
set ANDROID_CMAKE_PATH=%NDKROOT%/build/cmake
set UNREAL_TOOLCHAIN_PATH=C:/UnrealToolchains/v18_clang-11.0.1-centos7

pushd %ASSIMP_PATH%
git checkout master
git pull
::git apply fix_pi_compile.patch
::git checkout local_fixes
::git rebase master

:: Windows Binaries build
rmdir /s /q build_vs
mkdir build_vs
cd build_vs

%CMAKE_PATH% .. ^
  -DCMAKE_SYSTEM_NAME=Windows ^
  -DASSIMP_BUILD_ZLIB=ON ^
  -DASSIMP_BUILD_DRACO=ON ^
  -DBUILD_SHARED_LIBS=OFF ^
  -DASSIMP_BUILD_TESTS=OFF
%CMAKE_PATH% --build . --config RelwithDebInfo
popd

pushd %ASSIMP_PATH%

:: Android Binaries build
rmdir /s /q build_android
mkdir build_android
cd build_android

%CMAKE_PATH% .. ^
  -GNinja ^
  -DCMAKE_BUILD_TYPE=Release ^
  -DCMAKE_TOOLCHAIN_FILE=%ANDROID_CMAKE_PATH%/android.toolchain.cmake ^
  -DANDROID_TOOLCHAIN_NAME=aarch64-linux-android-clang ^
  -DCMAKE_SYSTEM_NAME=Android ^
  -DANDROID_NDK=%ANDROID_NDK_PATH% ^
  -DANDROID_NATIVE_API_LEVEL=latest ^
  -DANDROID_TOOLCHAIN=clang ^
  -DASSIMP_ANDROID_JNIIOSYSTEM=ON ^
  -DANDROID_FORCE_ARM_BUILD=TRUE ^
  -DCMAKE_ANDROID_ARCH_ABI=arm64-v8a ^
  -DANDROID_PLATFORM=android-29 ^
  -DASSIMP_BUILD_ZLIB=ON ^
  -DASSIMP_BUILD_DRACO=ON ^
  -DBUILD_SHARED_LIBS=OFF ^
  -DANDROID_STL=c++_shared ^
  -DASSIMP_BUILD_TESTS=OFF ^
  -DCMAKE_BUILD_TYPE=Release

%CMAKE_PATH% --build .
popd

:: LinuxAarch64 Binaries build
@REM rmdir /s /q build_aarch64
@REM mkdir build_aarch64
@REM cd build_aarch64

@REM %CMAKE_PATH% .. ^
@REM   -GNinja ^
@REM   -DCMAKE_BUILD_TYPE=Release ^
@REM   -DCMAKE_TOOLCHAIN_FILE=%UNREAL_TOOLCHAIN_PATH%/android.toolchain.cmake ^
@REM   -DANDROID_TOOLCHAIN_NAME=aarch64-linux-android-clang ^
@REM   -DCMAKE_SYSTEM_NAME=Android ^
@REM   -DANDROID_NDK=%ANDROID_NDK_PATH% ^
@REM   -DANDROID_NATIVE_API_LEVEL=latest ^
@REM   -DANDROID_TOOLCHAIN=clang ^
@REM   -DASSIMP_ANDROID_JNIIOSYSTEM=ON ^
@REM   -DANDROID_FORCE_ARM_BUILD=TRUE ^
@REM   -DCMAKE_ANDROID_ARCH_ABI=arm64-v8a ^
@REM   -DANDROID_PLATFORM=android-29 ^
@REM   -DASSIMP_BUILD_ZLIB=ON ^
@REM   -DBUILD_SHARED_LIBS=OFF ^
@REM   -DANDROID_STL=c++_shared ^
@REM   -DASSIMP_BUILD_TESTS=OFF ^
@REM   -DCMAKE_BUILD_TYPE=Release

@REM %CMAKE_PATH% --build .
@REM popd
@REM pushd %ASSIMP_PATH%


xcopy %ASSIMP_PATH%\include .\include /e /Y
::xcopy %ASSIMP_PATH%\build_vs\include .\include /e /Y

xcopy %ASSIMP_PATH%\build_vs\lib\RelWithDebInfo\assimp.lib .\lib\win64\ /Y
xcopy %ASSIMP_PATH%\build_vs\lib\RelWithDebInfo\assimp-vc142-mt.lib .\lib\win64\ /Y
xcopy %ASSIMP_PATH%\build_vs\contrib\zlib\RelWithDebInfo\zlibstatic.lib .\lib\win64\ /Y
xcopy %ASSIMP_PATH%\build_vs\lib\RelWithDebInfo\draco.lib .\lib\win64\ /Y

xcopy %ASSIMP_PATH%\build_android\lib\libassimp.a .\lib\android\ /Y
xcopy %ASSIMP_PATH%\build_android\port\AndroidJNI\libandroid_jniiosystem.a .\lib\android\ /Y
xcopy %ASSIMP_PATH%\build_android\contrib\zlib\libzlibstatic.a .\lib\android\ /Y
xcopy %ASSIMP_PATH%\build_android\lib\libdraco.a .\lib\android\ /Y

PAUSE

:: flag to disable exceptions:
:: -DCMAKE_CXX_FLAGS=-fno-exceptions