#include "dm_ozz.h"

#include "dm_core.h"
#include "dm_common.h"
#include "dm_logging.h"

#include "ozz/base/maths/internal/simd_math_config.h"

#include "ozz/animation/offline/raw_skeleton.h"
#include "ozz/animation/offline/skeleton_builder.h"
#include "ozz/animation/offline/raw_animation.h"
#include "ozz/animation/offline/animation_builder.h"

#include "ozz/animation/runtime/skeleton.h"
#include "ozz/animation/runtime/animation.h"
#include "ozz/animation/runtime/local_to_model_job.h"
#include "ozz/animation/runtime/sampling_job.h"

#include "ozz/geometry/runtime/skinning_job.h"

#include "ozz/base/maths/simd_math.h"
#include "ozz/base/maths/soa_transform.h"
#include "ozz/base/maths/vec_float.h"
#include "ozz/base/maths/soa_float4x4.h"

//#include "base/maths/simd_math.cc"

#include <cstring>



struct dm_ozz_data {
	dm_ozz_state state;
	dm_scene* scene;

	//[todo][fixme] for now std::vector and not dm_array, as ue4 TArray does not support copy constructor (https://forums.unrealengine.com/development-discussion/c-gameplay-programming/1608395-simple-std-unique_ptr-issue)
	std::vector<ozz::unique_ptr<ozz_skeleton>> skeletons_up;
	std::vector<ozz::unique_ptr<ozz_animation>> animations_up;
	dm_array<ozz::unique_ptr<ozz_animation>> animations_up_ue4;

	dm_array<dm_skin*> skeleton_scene{};
	dm_array<dm_animation*> animation_scene{};
	dm_array<ozz_skeleton*> skeletons{};
	dm_array<ozz_animation*> animations{};
	dm_map<dm_skin*, ozz_skeleton*> skeleton_map{};
	dm_map<dm_animation*, ozz_animation*> animation_map{};

	dm_array<ozz_sampling_cache> sampling_cache;
	// Buffer of local transforms as sampled from animation_.
	dm_array<dm_array<ozz::math::SoaTransform>> locals_{ {} };
	// Buffer of model space matrices.
	dm_array<dm_array<ozz::math::Float4x4>> models_{ {} };
};


//#pragma message("Ozz libraries were built with " _OZZ_SIMD_IMPLEMENTATION \
//                " SIMD math implementation")

//#include "framework/utils.h"
//#include "ozz/animation/offline/raw_track.h"


using ozz::animation::offline::RawSkeleton;
using ozz::animation::offline::RawAnimation;
using ozz::animation::Skeleton;



//RawSkeleton::Joint* find_ozz_joint(const dm_string& joint_name, const RawSkeleton& skeleton) {
//	for (const RawSkeleton::Joint& root_joint : skeleton.roots) {
//		if (root_joint.name == dm_string_to_ozz(joint_name)) {
//			return &root_joint;
//		}
//	}
//}

// Returns a mutable ozz::Range from a vector.
template <typename _Ty>
inline ozz::span<_Ty> make_ranger(dm_array<_Ty>& _array) {
	const size_t size = _array.Num();
	return ozz::span<_Ty>(size != 0 ? &_array[0] : NULL, size);
}

// Returns a non mutable ozz::Range from a vector.
template <typename _Ty>
inline ozz::span<const _Ty> make_ranger(const dm_array<_Ty>& _array) {
	const size_t size = _array.Num();
	return ozz::span<const _Ty>(size != 0 ? &_array[0] : NULL, size);
}

dm_mat4 
internal::dm_mat4_from_ozz(const ozz::math::Float4x4& matrixIn)
{
	dm_mat4 matrixOut;

	for (int i = 0; i < 4; ++i) {
		matrixOut.M[i][0] = ozz::math::GetX(matrixIn.cols[i]);
		matrixOut.M[i][1] = ozz::math::GetY(matrixIn.cols[i]);
		matrixOut.M[i][2] = ozz::math::GetZ(matrixIn.cols[i]);
		matrixOut.M[i][3] = ozz::math::GetW(matrixIn.cols[i]);
	}
	//// build matrix to transform coordinate space from Yup to Zup
	//FMatrix translate(
	//	FPlane(1, 0, 0, 0),
	//	FPlane(0, 0, 1, 0),
	//	FPlane(0, 1, 0, 0),
	//	FPlane(0, 0, 0, 1));
	//matrixOut = translate * matrixOut * translate.Inverse();
	//matrixOut = matrixOut.GetTransposed();
	//matrixOut.ScaleTranslation({ 100.0f, 100.0f,100.0f });

	return matrixOut;
}

ozz::string dm_string_to_ozz(const dm_string& s) {
	return ozz::string(TCHAR_TO_UTF8(*s));
}

ozz::math::Float4x4 
internal::dm_mat4_to_ozz(const dm_mat4& matrixIn)
{
	dm_mat4 matrix_in = matrixIn;
	//matrix_in.ScaleTranslation({ 0.01f, 0.01f, 0.01f });
	ozz::math::Float4x4 matrixOut;
	for (int i = 0; i < 4; ++i) {
#if DM_PLATFORM_ANDROID || defined(OZZ_SIMD_REF)
		matrixOut.cols[i].x = matrix_in.M[i][0];
		matrixOut.cols[i].y = matrix_in.M[i][1];
		matrixOut.cols[i].z = matrix_in.M[i][2];
		matrixOut.cols[i].w = matrix_in.M[i][3];
#else
		matrixOut.cols[i].m128_f32[0] = matrix_in.M[i][0];
		matrixOut.cols[i].m128_f32[1] = matrix_in.M[i][1];
		matrixOut.cols[i].m128_f32[2] = matrix_in.M[i][2];
		matrixOut.cols[i].m128_f32[3] = matrix_in.M[i][3];
#endif
	}
	return matrixOut;
}


ozz::math::Float3 dm_vec3_to_ozz(const dm_vec3& v) {
	return ozz::math::Float3(v.X, v.Y, v.Z);
	//return ozz::math::Float3(v.X, v.Z, v.Y);
}

ozz::math::Quaternion dm_quat_to_ozz(const dm_quat& q) {
	return ozz::math::Quaternion(q.X, q.Y, q.Z, q.W);
	//return ozz::math::Quaternion(q.X, -q.Z, -q.Y, q.W);
}

ozz::math::Transform dm_transform_to_ozz(const dm_transform& t) {
	ozz::math::Transform ozzt;
	ozzt.translation = dm_vec3_to_ozz(t.GetTranslation());
	ozzt.scale = dm_vec3_to_ozz(t.GetScale3D());
	ozzt.rotation = dm_quat_to_ozz(t.GetRotation());
	return ozzt;
}

dm_array<dm_skin*> get_skins_from_node(const dm_scene* scene, const dm_node* node) {
	dm_check(scene);
	dm_check(node);

	dm_array<dm_skin*> skin_refs{ };

	const auto find_node_in_skin = [](const dm_skin* skin_, const dm_node* node) -> int {
		int joint_index = 0;
		for (auto n : skin_->joints) {
			if (dm_node_compare(node, n)) {
				return joint_index;
			}
			++joint_index;
		}
		return -1;
	};

	for (auto skin_ : scene->skins) {
		if (find_node_in_skin(skin_, node) > -1) {
			skin_refs.Add(skin_);
		}
	}
	return skin_refs;
}

ozz::animation::Animation* process_animation(dm_scene* scene, dm_animation* anim) {
	// dmanim -> circle trhough node-translation tracks
	// find node in skins -> -1 if not, otherwise return index in skin
	// if found valid skin -> get number of joints, to specificy number of raw animation tracks (needs to map to skin)
	// for each node, get index in skin (and check with index in ozz_skeleton), and add track at this index
	// (-> optimization: order node_tracks in animation based on supplied skin, now they should be in order)

	// use target node to get referencing skin and thereby _ozz_skeleton
	// since an animation doesn't necessarily have each track, we need to look into each for at least 1 track
	dm_check(scene);
	dm_check(anim);
	dm_log_msg("entering process_animation | processing: " + anim->name);

	const auto find_node_in_skin = [](const dm_skin* skin_, const dm_node* node_) -> int {
		int joint_index = 0;
		for (auto n : skin_->joints) {
			if (dm_node_compare(node_, n)) {
				return joint_index;
			}
			++joint_index;
		}
		return -1;
	};

	const auto find_referenced_skins = [find_node_in_skin](dm_array<dm_skin*>& skins, const dm_array<dm_node*>& nodes) -> dm_array<dm_skin*> {
		dm_array<dm_skin*> referenced_skins{};
		for (auto node : nodes) {
			for (auto skin : skins) {
				int index = find_node_in_skin(skin, node);
				if (index >= 0) {
					dm_array_add_unique(referenced_skins, skin);
				}
			}
		}
		return referenced_skins;
	};

	const auto get_array_of_referenced_nodes_in_animation = [scene](const dm_animation* animation) -> dm_array<dm_node*> {
		dm_array<dm_node*> referenced_nodes{};
		for (auto track : animation->node_translation_tracks) {
			dm_array_add_unique(referenced_nodes, track.target);
		}
		for (auto track : animation->node_rotation_tracks) {
			dm_array_add_unique(referenced_nodes, track.target);
		}
		for (auto track : animation->node_scale_tracks) {
			dm_array_add_unique(referenced_nodes, track.target);
		}
		return referenced_nodes;
	};

	//dm_node* reference_node = nullptr;
	//if (dmanim->node_translation_tracks.Num() > 0) {
	//	reference_node = dmanim->node_translation_tracks[0].target;
	//}
	//else if (dmanim->node_rotation_tracks.Num() > 0) {
	//	reference_node = dmanim->node_rotation_tracks[0].target;
	//}
	//else if (dmanim->node_scale_tracks.Num() > 0) {
	//	reference_node = dmanim->node_scale_tracks[0].target;
	//}
	//else {
	//	// not a node base animation
	//	dm_log_msg("return null: not a node base animation");
	//	return nullptr;
	//}
	//dm_log_msg("reference node is: " + reference_node->name);

	// now that we have a node, find a skin/skeleton, that references the node (else it's not a skinned animation)
	dm_skin* my_skin = nullptr;
	ozz::animation::Skeleton* ozz_skeleton = nullptr;
	//auto skin_refs = get_skins_from_node(dmscene, reference_node);
	//auto skin_refs = find_referenced_skins(dmscene->skins, dmanim->);
	dm_array<dm_node*> referenced_nodes = get_array_of_referenced_nodes_in_animation(anim);
	dm_array<dm_skin*> referenced_skins = find_referenced_skins(scene->skins, referenced_nodes);
	if (referenced_skins.Num() > 0) {
		//[fixme][todo] only takes first skin for now, ignores reste
		my_skin = referenced_skins[0];
		ozz_skeleton = (ozz::animation::Skeleton*)referenced_skins[0]->_ozz_skeleton;
		anim->_ozz_skeleton = ozz_skeleton;
	}
	else {
		// not skin based animation 
		//[todo] handle this type of animation (maybe create extra skin with rest of node hierarchy - call it non_skin)
		dm_log_msg("return null: not a skin based animation");
		return nullptr;
	}


	RawAnimation raw_animation;
	raw_animation.name = dm_string_to_ozz(anim->name);
	raw_animation.duration = anim->length;
	//raw_animation.tracks.resize(dm_skin->joints.Num());
	raw_animation.tracks.resize(ozz_skeleton->num_joints());


	auto find_joint_index = [](ozz::animation::Skeleton* skeleton, dm_string joint_name) {
		const auto& names = skeleton->joint_names();
		for (size_t i = 0; i < names.size(); ++i) {
			if (std::strcmp(names[i], dm_string_to_cstr(joint_name).c_str()) == 0) {
				return static_cast<int>(i);
			}
		}
		return -1;
	};


	for (auto& dmtrack : anim->node_translation_tracks) {
		//int node_index = find_node_in_skin(dm_skin, dmtrack.target);
		int node_index = find_joint_index(ozz_skeleton, dmtrack.target->name);
		if (node_index < 0) {
			// not part of skeletal animation
			continue;
		}
		RawAnimation::JointTrack& track = raw_animation.tracks[node_index];
		//auto& sampler = dmtrack.values;
		for (int key_index = 0; key_index < dmtrack.values.in.Num(); ++key_index) {
			RawAnimation::TranslationKey key;
			key.time = dmtrack.values.in[key_index];
			key.value = dm_vec3_to_ozz(dmtrack.values.out[key_index]);
			track.translations.push_back(key);
		}
	}
	for (auto& dmtrack : anim->node_rotation_tracks) {
		//int node_index = find_node_in_skin(dm_skin, dmtrack.target);
		int node_index = find_joint_index(ozz_skeleton, dmtrack.target->name);
		if (node_index < 0) {
			// not part of skeletal animation
			continue;
		}
		RawAnimation::JointTrack& track = raw_animation.tracks[node_index];
		for (int key_index = 0; key_index < dmtrack.values.in.Num(); ++key_index) {
			RawAnimation::RotationKey key;
			key.time = dmtrack.values.in[key_index];
			key.value = dm_quat_to_ozz(dmtrack.values.out[key_index]);
			track.rotations.push_back(key);
		}
	}
	for (auto& dmtrack : anim->node_scale_tracks) {
		//int node_index = find_node_in_skin(dm_skin, dmtrack.target);
		int node_index = find_joint_index(ozz_skeleton, dmtrack.target->name);
		if (node_index < 0) {
			// not part of skeletal animation
			continue;
		}
		RawAnimation::JointTrack& track = raw_animation.tracks[node_index];
		for (int key_index = 0; key_index < dmtrack.values.in.Num(); ++key_index) {
			RawAnimation::ScaleKey key;
			key.time = dmtrack.values.in[key_index];
			key.value = dm_vec3_to_ozz(dmtrack.values.out[key_index]);
			track.scales.push_back(key);
		}
	}

	ozz::animation::offline::AnimationBuilder animation_builder;
	if (!raw_animation.Validate()) {
		// ohoh
		dm_log_msg("dm_ozz::process_animation: raw_animation not valid.");
		return nullptr;
	}

	return animation_builder(raw_animation).release();
}

// make this a lambda func
void process_joint(RawSkeleton& raw_skeleton, RawSkeleton::Joint* parent, dm_skin* skin, dm_node* joint) {
	//bool is_skin_node = false;
	//for (auto& j : skin_->joints) {
	//	if (dmjoint == j) {
	//		is_skin_node = true;
	//		break;
	//	}
	//}
	//if (!is_skin_node) {
	//	return;
	//}
	dm_check(skin);
	dm_check(joint);
	dm_log_msg("entering process_joint | processing: " + joint->name);

	RawSkeleton::Joint* ozzjoint;
	if (parent == nullptr) {
		//raw_skeleton.roots.push_back(ozzjoint);
		uint32 num_of_roots = raw_skeleton.roots.size();
		raw_skeleton.roots.resize(num_of_roots + 1);
		ozzjoint = &raw_skeleton.roots[num_of_roots];
		//[warning] this line could be important, or be super destructive
		//ozzjoint->transform = dm_transform_to_ozz(dm_node_relative_to_local_transform(dmjoint));
		ozzjoint->transform = dm_transform_to_ozz(joint->transform);
	}
	else {
		//parent->children.push_back(ozzjoint);
		uint32 num_of_children = parent->children.size();
		parent->children.resize(num_of_children + 1);
		ozzjoint = &parent->children[num_of_children];
		ozzjoint->transform = dm_transform_to_ozz(joint->transform);
	}
	ozzjoint->name = dm_string_to_ozz(joint->name);
	//ozzjoint->transform = dm_transform_to_ozz(dmjoint->transform);
	//ozzjoint->transform = dm_transform_to_ozz(dmjoint->joint->offset_transform);

	for (auto children_joint : joint->children) {
		process_joint(raw_skeleton, ozzjoint, skin, children_joint);
	}
}

ozz::animation::Skeleton* process_skeleton(dm_skin* skin, dm_scene *scene) 
{
	dm_check(skin);
	dm_check(scene);
	dm_log_msg("entering process_skeleton | processing: " + skin->name);
	// create list of root joints, for easier parsing
	dm_array<dm_node*> root_joints = dm_node_array_get_roots(skin->joints);

	// parse those root joints recursevily //[note][ade] should there be more than 1 root per skin allowed, or batch together in import?
	ozz::animation::offline::RawSkeleton raw_skeleton;
	for (auto root : root_joints) {
		//int num = raw_skeleton.num_joints();
		process_joint(raw_skeleton, nullptr, skin, root);
	}
	// build skeleton
	ozz::animation::offline::SkeletonBuilder builder;
	int num = raw_skeleton.num_joints();
	if (!raw_skeleton.Validate()) {
		dm_log_msg("dm_ozz::process_skeleton: raw_skeleton not valid.");
		return nullptr;
	}

	return builder(raw_skeleton).release();
}

void
dm_scene_to_ozz(dm_scene* scene, dm_ozz_data* ozz_data)
{
	dm_check(scene);
	dm_log_msg("entering dm_scene_to_ozz...");
	//[todo] need to remove, so dm_scene can be const again. having children pointer available makes skeleten creation a lot easier
	dm_scene_recalculate_node_children(scene);


	//[todo] properly separate ozz from scene
	// for now allocate and save in scene directly
	scene->ozz_data = dm_ozz_init({});


	for (dm_skin* skn : scene->skins) {
		ozz::animation::Skeleton* skeleton = process_skeleton(skn, scene);	// [todo] release memory after use
		dm_check(skeleton);
		skn->_ozz_skeleton = (void*)skeleton;
	}
	for (dm_animation* anim : scene->animations) {
		ozz::animation::Animation* ozz_animation = process_animation(scene, anim); //[todo] free memory/release after use in!
		dm_check(ozz_animation);
		anim->_ozz_animation = (void*)ozz_animation;
	}
}

dm_ozz_data*
dm_ozz_init(dm_ozz_init_params params)
{
	//if (ozz_data) {
		//return dm_status(dm_status::dm_status_code::already_initialized, "ozz_data already intialized, operation not allowed.");
	
	//}
	return new dm_ozz_data();
}

dm_status
dm_ozz_free(dm_ozz_data* ozz_data)
{
	if (ozz_data != nullptr) {
		delete ozz_data;
		ozz_data = nullptr;
	}
	return dm_status();
}

void
dm_ozz_from_scene(dm_ozz_data* ozz_data, dm_scene* scene)
{
	dm_check(scene);
	dm_check(ozz_data);
	dm_log_msg("entering dm_scene_to_ozz...");
	//[todo] need to remove, so dm_scene can be const again. having children pointer available makes skeleten creation a lot easier
	dm_scene_recalculate_node_children(scene);

	for (dm_skin* skin : scene->skins) {
		ozz::animation::Skeleton* ozz_skeleton = process_skeleton(skin, scene);
		dm_check(ozz_skeleton);
		ozz_data->skeleton_map.Add(skin, ozz_skeleton);
	}
	for (dm_animation* anim : scene->animations) {
		ozz::animation::Animation* ozz_animation = process_animation(scene, anim);
		dm_check(ozz_animation);
		ozz_data->animation_map.Add(anim, ozz_animation);
	}

	for (dm_skin* skn : scene->skins) {
		ozz::animation::Skeleton* skeleton = process_skeleton(skn, scene);	// [todo] release memory after use
		dm_check(skeleton);
		skn->_ozz_skeleton = (void*)skeleton;
	}
	for (dm_animation* anim : scene->animations) {
		ozz::animation::Animation* ozz_animation = process_animation(scene, anim); //[todo] free memory/release after use in!
		dm_check(ozz_animation);
		anim->_ozz_animation = (void*)ozz_animation;
	}
}

bool 
on_update(float time_ratio, ozz::animation::Skeleton& ozz_skeleton, ozz::animation::Animation& ozz_animation, 
				ozz::animation::SamplingJob::Context& ozz_cache, 
				dm_array<ozz::math::SoaTransform>& locals, dm_array<ozz::math::Float4x4>& models)
{
	// Samples animation at t = animation_time_.
	ozz::animation::SamplingJob sampling_job;
	sampling_job.animation = &ozz_animation;
	sampling_job.context = &ozz_cache;
	sampling_job.ratio = time_ratio;
	sampling_job.output = make_ranger(locals);
	if (!sampling_job.Run()) {
		return false;
	}

	// Converts from local space to model space matrices.
	ozz::animation::LocalToModelJob ltm_job;
	ltm_job.skeleton = &ozz_skeleton;
	ltm_job.input = make_ranger(locals);
	ltm_job.output = make_ranger(models);

	if (!ltm_job.Run()) {
		return false;
	}

	return true;
}

bool 
update_skinned_mesh(const ozz::animation::Skeleton* skeleton_, const dm_scene* scene_, 
						const dm_mesh& mesh_in, const dm_array<ozz::math::Float4x4> models_, 
						dm_array<dm_array<dm_vec3>>& vertices_out, dm_mesh& mesh_out)
{
	dm_check(skeleton_);
	dm_check(scene_);
	//for (size_t i = 0; i < mesh_.joint_remaps.size(); ++i) {
	//	skinning_matrices_[i] = models_[mesh_.joint_remaps[i]] * mesh_.inverse_bind_poses[i];
	//}
	if (vertices_out.Num() < mesh_in.primitives.Num()) {
		dm_log_msg("error update_skinned_mesh: vertices_out.Num() < mesh_in.primitivies.Num() ");
		return false;
	}

	dm_array<ozz::math::Float4x4> _skinning_matrices = {};
	_skinning_matrices.SetNum(models_.Num());
	for (size_t i = 0; i < models_.Num(); ++i) {

		//// Builds soa matrices from soa transforms.
		//const ozz::math::SoaTransform& transform = skeleton_->joint_bind_poses()[i];
		//const ozz::math::SoaFloat4x4 local_soa_matrices = ozz::math::SoaFloat4x4::FromAffine(
		//	transform.translation, transform.rotation, transform.scale);

		//// Converts to aos matrices.
		//ozz::math::Float4x4 local_aos_matrices[1];
		////ozz::math::Transpose16x16(&local_soa_matrices.cols[0].x,
		////	local_aos_matrices->cols);
		//ozz::math::Transpose4x4(&local_soa_matrices.cols[0].x, local_aos_matrices->cols);

		dm_string jname = skeleton_->joint_names()[i];
		auto jnode = dm_scene_find_node(scene_, jname);	//[todo] make this hashed access
		//find_node_in_skin(scene_->skins[0], )

		//skeleton_->joint_bind_poses()[i];
		//auto jt = scene_->skins[0]->joints[i]->joint;
		//if (!jt) {
		if (!jnode->joint) {
			auto nt = dm_node_relative_to_local_transform(jnode);
			dm_mat4 nm = nt.ToMatrixWithScale();
			_skinning_matrices[i] = models_[i] * internal::dm_mat4_to_ozz(nm);
		}
		else {
			//_skinning_matrices[i] = models_[i];
			//_skinning_matrices[i] = models_[i] * skeleton_->joint_bind_poses()[i];
			//_skinning_matrices[i] = models_[i] * internal::dm_mat4_to_ozz(jt->inverse_bind_matrix);	// * mesh_.inverse_bind_poses[i];
			//_skinning_matrices[i] = models_[i] * local_aos_matrices[0];
			_skinning_matrices[i] = models_[i] * internal::dm_mat4_to_ozz(jnode->joint->inverse_bind_matrix);
			//auto nt = dm_node_relative_to_local_transform(jnode);
			//mat4 nm = nt.ToMatrixWithScale();
			//_skinning_matrices[i] = models_[i] * internal::dm_mat4_to_ozz(nm);
		}
	}

	for (int primitive_index = 0; primitive_index < mesh_in.primitives.Num(); ++primitive_index) {

		auto& section = mesh_in.primitives[primitive_index];
		ozz::geometry::SkinningJob skinning_job;
		const int num_of_joints = section.joints.Num();

		if (vertices_out[primitive_index].Num() < section.vertices.Num()) {
			dm_log_msg("error update_skinned_mesh: vertices_out[primitive_index].Num() < section.vertices.Num()");
			return false;
		}

		skinning_job.vertex_count = section.vertices.Num();
		skinning_job.influences_count = num_of_joints;
		skinning_job.joint_matrices = make_ranger(_skinning_matrices);

		// dm-dm_scene uses int32 indices, ozz wants uint16
		//dm_array<uint16> section_indices{};
		//section_indices.SetNum(section.indices.Num());
		//for (int indices_index = 0; indices_index < section.indices.Num(); ++indices_index) {
		//	section_indices[indices_index] = section.indices[indices_index];
		//}
		//skinning_job.joint_indices = make_ranger(section_indices);	//skinning_job.joint_indices = make_ranger(section.indices);
		//skinning_job.joint_indices_stride = sizeof(uint16) * num_of_joints;
		auto find_joint_index = [](const ozz::animation::Skeleton* skeleton, dm_string joint_name) {
			dm_check(skeleton);
			const auto& names = skeleton->joint_names();
			for (size_t i = 0; i < names.size(); ++i) {
				if (std::strcmp(names[i], dm_string_to_cstr(joint_name).c_str()) == 0) {
					return static_cast<int>(i);
				}
			}
			return -1;
		};
		dm_array<int> joint_ids = {};
		//joint_ids.SetNum(section.joints.Num());
		for (auto& n : section.joints) {
			//[todo][ahd] dont use skin indices, but ozz_skeleton ones, in case they are not the same!
			//joint_ids.Add(find_node_in_skin(scene_->skins[0], n));
			int index = find_joint_index(skeleton_, n->name);
			joint_ids.Add(index);
		}

		//dm_array<uint16> joint_indices{};
		//joint_indices.SetNum(section.vertices.Num() * num_of_joints);
		//for (int jid = 0; jid < (joint_indices.Num() - (num_of_joints-1)); jid = jid + num_of_joints) {
		//	//joint_indices[jid] = joint_ids[jid%num_of_joints];
		//	for (int xx = 0; xx < num_of_joints; ++xx) {
		//		joint_indices[jid + xx] = joint_ids[xx];
		//	}
		//}
		//skinning_job.joint_indices = make_ranger(joint_indices);
		//skinning_job.joint_indices_stride = sizeof(uint16) * num_of_joints;
		dm_array<uint16_t> joint_indices{};
		joint_indices.SetNum(section.vertices.Num() * num_of_joints);
		for (int jid = 0; jid < (joint_indices.Num() - (num_of_joints - 1)); jid = jid + num_of_joints) {
			//joint_indices[jid] = joint_ids[jid%num_of_joints];
			for (int xx = 0; xx < num_of_joints; ++xx) {
				joint_indices[jid + xx] = joint_ids[xx];
			}
		}
		skinning_job.joint_indices = make_ranger(joint_indices);
		skinning_job.joint_indices_stride = sizeof(uint16_t) * num_of_joints;

		// Setup joint's weights.
		if (num_of_joints > 1) {
			skinning_job.joint_weights = make_ranger(section.weights);
			skinning_job.joint_weights_stride = sizeof(float) * (num_of_joints);
		}

		// Setup input positions, coming from the loaded mesh.
		dm_array<float> section_vertices_in = {};
		section_vertices_in.SetNum(section.vertices.Num() * 3);
		for (int pos_index = 0; pos_index < section.vertices.Num(); ++pos_index) {
			const dm_vec3& vertex = section.vertices[pos_index];
			section_vertices_in[pos_index * 3 + 0] = vertex.X;
			section_vertices_in[pos_index * 3 + 1] = vertex.Y;
			section_vertices_in[pos_index * 3 + 2] = vertex.Z;
		}
		skinning_job.in_positions = make_ranger(section_vertices_in); //skinning_job.in_positions = make_ranger(section.vertices);
		skinning_job.in_positions_stride = sizeof(float) * 3;

		// Setup output positions, coming from the rendering output mesh buffers.
		// We need to offset the buffer every loop.
		dm_array<float> section_vertices_out = {};
		section_vertices_out.SetNum(section_vertices_in.Num());
		skinning_job.out_positions = make_ranger(section_vertices_out);
		skinning_job.out_positions_stride = sizeof(float) * 3;


		// Setup input normals, coming from the loaded mesh.
		dm_array<float> section_normals_in = {};
		section_normals_in.SetNum(section.normals.Num() * 3);	// x, y, z
		for (int pos_index = 0; pos_index < section.normals.Num(); ++pos_index) {
			const dm_vec3& normal = section.normals[pos_index];
			section_normals_in[pos_index * 3 + 0] = normal.X;
			section_normals_in[pos_index * 3 + 1] = normal.Y;
			section_normals_in[pos_index * 3 + 2] = normal.Z;
		}
		skinning_job.in_normals = make_ranger(section_normals_in);
		skinning_job.in_normals_stride = sizeof(float) * 3;

		// Setup output normals, coming from the rendering output mesh buffers.
		// We need to offset the buffer every loop.
		dm_array<float> section_normals_out = {};
		section_normals_out.SetNum(section_normals_in.Num());
		skinning_job.out_normals = make_ranger(section_normals_out);
		skinning_job.out_normals_stride = sizeof(float) * 3;

		//skinning_job.in_tangents;
		//skinning_job.in_tangents_stride;
		//skinning_job.out_tangents;
		//skinning_job.out_tangents_stride;

		if (!skinning_job.Run()) {
			dm_log_msg("!skinning_job.Run()");
			return false;
		}

		for (int vertex_index = 0; vertex_index < section.vertices.Num(); ++vertex_index) {
			vertices_out[primitive_index][vertex_index].X = section_vertices_out[vertex_index * 3 + 0];
			vertices_out[primitive_index][vertex_index].Y = section_vertices_out[vertex_index * 3 + 1];
			vertices_out[primitive_index][vertex_index].Z = section_vertices_out[vertex_index * 3 + 2];
		}
		auto vert = vertices_out;
	}

	return true;
}

void release_ozz_memory(dm_scene* dmscene) {
	dm_check(dmscene);
	for (auto anim : dmscene->animations) {
		ozz::Delete((ozz::animation::Skeleton*) anim->_ozz_skeleton);
		ozz::Delete((ozz::animation::Skeleton*) anim->_ozz_animation);
		//OZZ_DELETE(ozz::memory::default_allocator(), (ozz::animation::Skeleton*) anim->_ozz_skeleton);
		//OZZ_DELETE(ozz::memory::default_allocator(), (ozz::animation::Skeleton*) anim->_ozz_animation);
		//ozz::memory::default_allocator()->Deallocate((ozz::animation::Skeleton*) anim->_ozz_skeleton);
		//ozz::memory::default_allocator()->Deallocate((ozz::animation::Animation*)anim->_ozz_animation);
	}
}
