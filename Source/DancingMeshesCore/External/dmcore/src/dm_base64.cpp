
#include "dm_base64.h"

namespace internal {
	// clang-format off
	//using namespace std;
	//constexpr std::array<char, 64> EncodeMap =
	constexpr char dm_base64_EncodeMap[64] =
	{
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
		'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
		'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
		'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
	};

	//constexpr std::array<char, 256> DecodeMap =
	constexpr char dm_base64_DecodeMap[256] =
	{
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
		52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
		-1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
		15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
		-1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
		41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
	};
	// clang-format on

} // namespace internal

//inline dm_string Encode(std::vector<uint8_t> const& bytes)
dm_string dm_base64_encode(dm_array<uint8_t> const& bytes)
{
	const size_t length = dm_array_length(bytes);	//const size_t length = bytes.size();
	if (length == 0)
	{
		return {};
	}

	//std::string out{};
	dm_string out{};
	dm_string_reserve(out, ((length * 4 / 3) + 3) & (~3u));//out.reserve(((length * 4 / 3) + 3) & (~3u)); // round up to nearest 4

	uint32_t value = 0;
	int32_t bitCount = -6;
	for (const uint8_t c : bytes)
	{
		value = (value << 8u) + c;
		bitCount += 8;
		while (bitCount >= 0)
		{
			const uint32_t shiftOperand = bitCount;
			//out.push_back(internal::EncodeMap.at((value >> shiftOperand) & 0x3fu));
			//dm_string_append_char(out, internal::EncodeMap.at((value >> shiftOperand) & 0x3fu));
			dm_string_append_char(out, internal::dm_base64_EncodeMap[((value >> shiftOperand) & 0x3fu)]);
			bitCount -= 6;
		}
	}

	if (bitCount > -6)
	{
		const uint32_t shiftOperand = bitCount + 8;
		//out.push_back(internal::EncodeMap.at(((value << 8u) >> shiftOperand) & 0x3fu));
		//dm_string_append_char(out, internal::EncodeMap.at(((value << 8u) >> shiftOperand) & 0x3fu));
		dm_string_append_char(out, internal::dm_base64_EncodeMap[(((value << 8u) >> shiftOperand) & 0x3fu)]);
	}

	while (dm_string_length(out) % 4 != 0)//while (out.size() % 4 != 0)
	{
		//out.push_back('=');
		dm_string_append_char(out, '=');
	}

	return dm_string(dm_string_to_cstr(out).c_str());
}

#if defined(FEATURE_LEVEL_CPP_17) && !defined(DM_RUNTIME_UNREAL)
bool dm_base64_decode(std::string_view in, std::vector<uint8_t>& out)
#else
bool dm_base64_decode(dm_string const& in, dm_array<uint8_t>& out)
#endif
{
	dm_array_clear(out);
	const uint64 length = dm_string_length(in);

	if (length == 0)
	{
		return true;
	}

	if (length % 4 != 0)
	{
		return false;
	}

	dm_array_reserve(out, (length / 4) * 3);	//out.reserve((length / 4) * 3);

	bool invalid = false;
	uint32_t value = 0;
	int32_t bitCount = -8;
	for (std::size_t i = 0; i < length; i++)
	{
		const uint8_t c = static_cast<uint8_t>(in[i]);
		//const char map = internal::DecodeMap.at(c);
		const char map = internal::dm_base64_DecodeMap[c];
		if (map == -1)
		{
			if (c != '=') // Non base64 character
			{
				invalid = true;
			}
			else
			{
				// Padding characters not where they should be
				const std::size_t remaining = length - i - 1;
				if (remaining > 1 || (remaining == 1 ? in[i + 1] != '=' : false))
				{
					invalid = true;
				}
			}

			break;
		}

		value = (value << 6u) + map;
		bitCount += 6;
		if (bitCount >= 0)
		{
			const uint32_t shiftOperand = bitCount;
			dm_array_add(out, static_cast<uint8_t>(value >> shiftOperand));	//out.push_back(static_cast<uint8_t>(value >> shiftOperand));
			bitCount -= 8;
		}
	}

	if (invalid)
	{
		dm_array_clear(out);	//out.clear();
	}

	return !invalid;
} 
