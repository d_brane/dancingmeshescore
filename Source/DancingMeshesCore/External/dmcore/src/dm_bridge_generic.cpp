#include "dm_bridge.h"


bool generic_from_dm(dm_scene* from_scene, void* to_scene) {
	return true;
}

bool generic_to_dm(void* from_scene, dm_scene* to_scene) {
	return true;
}

const struct dm_bridge_interface bridge_generic = {
	dm_string("Generic Bridge"),
	dm_bridge_interface::dm_bridge_direction::direction_to_dm,
	//dm_bridge_mode::
	generic_from_dm,
	generic_to_dm,
	generic_from_dm,
	generic_to_dm
};

bool copy_from_dm(dm_scene* from, void* to) {
	if ((to == nullptr) || (from == nullptr)) {
		return false;
	}
	for (auto n : from->nodes) {
		;
	}
	return true;
}

bool copy_to_dm(void* from, dm_scene* to) {
	return copy_from_dm((dm_scene*)from, to);
}

const struct dm_bridge_interface bridge_copy = {
	dm_string("Copy Bridge"),
	dm_bridge_interface::dm_bridge_direction::direction_bidirectional,
	copy_from_dm,
	copy_to_dm,
	copy_from_dm,
	copy_to_dm
};

// in C or starting C++20 one can use aggregate initializers (https://en.cppreference.com/w/cpp/language/aggregate_initialization)
//const struct bridge_interface bridge_generic = {
//	.name = dm_string("Generic Bridge"),
//	.direction = bridge_interface::bridge_direction::to_dm;
//	.from_dm = generic_from_dm,
//	.to_dm = generic_to_dm
//};

