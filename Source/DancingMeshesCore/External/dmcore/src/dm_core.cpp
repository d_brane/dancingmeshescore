
#include "dm_core.h"
#include "dm_common.h"
#include "dm_math.h"
#include "dm_scene.h"

template<class T>
inline dm_string dm_unique_name_generator(const dm_string& name, dm_array<T*> check_with) {
	dm_string generated_name = name;
	int index = 0;
	for (auto element : check_with) {
		if (element->name == generated_name) {
			++index;
			generated_name = name + dm_string("_") + dm_string::FromInt(index);//+ std::to_chars(index);
		}
	}
	return generated_name;
}
template<class T>
inline dm_string dm_unique_name_generator_appended(const dm_string& name, const dm_string& append, dm_array<T*> check_with) {
	return dm_unique_name_generator(name + "_" + append, check_with);
}

dm_status dm_scene_update(dm_scene* scene, dm_scene_update_params* params) { return dm_status(); }

dm_status dm_scene_from_json(dm_scene* scene, dm_string& str, bool merge_in) { return dm_status(); }


int dm_scene_get_meshes(dm_scene* scene, dm_mesh_handle* meshes) {
	if (int num_of_meshes = dm_scene_num_meshes(scene)) {
		*meshes = scene->meshes[0];
		return num_of_meshes;
	}
	else {
		*meshes = nullptr;
		return 0;
	}
}

dm_node*
dm_node_create(const dm_string& name, dm_transform transform_, dm_node* parent)
{
	//dm_node* node_ = new dm_node(name, transform_, parent);
	dm_node* node_ = new dm_node({name, transform_, parent });

	if (parent != nullptr) {
		dm_array_add(parent->children, node_);
//#ifdef DM_RUNTIME_UNREAL
//		parent->children.Add(node_);
//#else
//		parent->children.push_back(node_);
//#endif //DM_RUNTIME_UNREAL
	}

	return node_;
}

/// only adds parent as _parent_name, use fix_node_hierarchY_from_name to fix up hierachy
dm_node* 
dm_node_create(const dm_string& name, dm_transform transform_, dm_string &parentId)
{
	dm_node* n = dm_node_create(name, transform_, nullptr);
	n->_parent_name = parentId;

	return n;
}


dm_node* 
dm_node_copy(dm_node* node_)
{
	dm_node* new_node = dm_node_create(node_->name, node_->transform, node_->parent);
	new_node->children = node_->children;
	new_node->camera = node_->camera;
	new_node->environment = node_->environment;
	new_node->instance = node_->instance;
	new_node->type = node_->type;

	return new_node;
}



void 
dm_node_delete(dm_node* &node_, bool delete_children)
{
	if (delete_children) {
		for (dm_node *child_node : node_->children) {
			dm_node_delete(child_node, delete_children);
		}
	}
	else {
		// reparent childs (with node_'s parent, can be nullptr in case node_ was root)
		for (dm_node *child_node : node_->children) {
			child_node->parent = node_->parent;
		}
	}
	if (node_->parent != nullptr) {
		dm_array_remove(node_->parent->children, node_);
//#ifdef DM_RUNTIME_UNREAL
//		node_->parent->children.Remove(node_);
//#else
//		node_->parent->children.pop(node_);
//#endif //DM_RUNTIME_UNREAL
	}
	delete node_;
	node_ = nullptr;
}


void 
dm_node_clone(dm_node* in_node, dm_node* out_node, bool clone_children)
{
	out_node = dm_node_copy(in_node);

	if (!clone_children) {
		out_node->children = {};
		return;
	}

	int i = 0;
	for (dm_node *child_node : out_node->children) {
		dm_node *cloned_child = nullptr;
		dm_node_clone(child_node, cloned_child, true);
		cloned_child->parent = out_node;
		out_node->children[i] = cloned_child;
	}
}

bool dm_node_compare(const dm_node* node_a, const dm_node* node_b)
{
	dm_check(node_a); 
	dm_check(node_b); 
	return (node_a->name == node_b->name);
}



void 
dm_node_move_relative(dm_node *joint, dm_transform delta_transform)
{
	//todo(alex): implement operator+ for transforms on non Ue4 build
	joint->transform += delta_transform;
}



void 
dm_node_flip(dm_node* in_node, dm_flip_axis flip_axis_, bool flip_child_nodes)
{
	//dm_node flipped_node = clone_node
	//in_node->trans.Rotation
	
	switch (flip_axis_) {
	case dm_flip_axis::X:

		break;
	case dm_flip_axis::Y:
		break;
	case dm_flip_axis::Z:
		break;
	
	case dm_flip_axis::XY:
		break;
	case dm_flip_axis::XZ:
		break;
	case dm_flip_axis::YZ:
		break;
	}
}



void
dm_node_mirror(dm_node* in_node, dm_flip_axis flip_axis_, bool flip_child_nodes)
{
	//dm_node flipped_node = clone_node
	//in_node->trans.Rotation

	switch (flip_axis_) {

	case dm_flip_axis::XY:

		break;
	case dm_flip_axis::XZ:
		break;
	case dm_flip_axis::YZ:
		break;
	}
}



void
dm_node_reparent(dm_node * node_, dm_node * new_parent)
{
	node_->parent = new_parent;
}


void 
dm_node_normalize_scale(dm_node* node_, bool normalize_child_nodes)
{
	//
#ifdef DM_RUNTIME_UNREAL
	dm_vec3 node_scale = node_->transform.GetScale3D();
#else
	dm_vec3 node_scale = node_->trans.scale;
#endif
	
}

dm_array<dm_node*> 
dm_node_array_get_roots(const dm_array<dm_node*> nodes)
{
	dm_array<dm_node*> root_nodes = nodes;
	for (auto n : nodes) {
		auto parent_node = n->parent;
		for (auto nn : nodes) {
			if (nn == parent_node)
				root_nodes.Remove(n);
		}
	}
	return root_nodes;
}

dm_array<dm_node*> dm_node_array_sort_depth_first(const dm_array<dm_node*>& nodes)
{
	dm_array<dm_node*> sorted_list{};
	dm_array<dm_node*> roots = dm_node_array_get_roots(nodes);

	// recursive lambda - requires c++14
	const auto process_node = [&](const auto& self, dm_array<dm_node*>& sorted_list, const dm_array<dm_node*>& to_sort) -> void {
		for (auto n : to_sort) {
			sorted_list.Add(n);
			self(self, sorted_list, n->children);
		}
	};
	process_node(process_node, sorted_list, roots);

	return sorted_list;
}

dm_string dm_node_array_to_string_verbose(const dm_array<dm_node*> nodes)
{
	dm_array<dm_node*> printed_list{};
	printed_list = dm_node_array_sort_depth_first(nodes);
	dm_array<dm_node*> roots = dm_node_array_get_roots(printed_list);

	uint32 intent = 0;
	// see https://stackoverflow.com/a/45824777
	const auto print = [&](const auto& self, dm_string& printout, dm_array<dm_node*>& to_print) -> void {
		for (auto n : to_print) {
			FString intentsymbol(" ");
			printout.AppendChars(&intentsymbol[0], intent);
			printout.Append(n->name);
			printout.Append("\n");
			++intent;
			self(self, printout, n->children);
			--intent;
		}
	};
	dm_string printout{ "" };
	print(print, printout, roots);

	return printout;
}

//void 
//sort_node_array(dm_array<node*> nodes, dm_node_sort_mode sorting_mode)
//{
//	//[todo][ademets] implement me
//}


void 
dm_scene_add_node(dm_scene * scene_, dm_node * node_)
{
	bool add_children = false;
	node_->name = dm_unique_name_generator(node_->name, scene_->nodes);
	scene_->nodes.Add(node_);

	// instead, have dm_node_array_get_roots() as computed function in dm_scene struct
	//if (in_node->parent == nullptr) {
	//scene_->root_nodes.Add(in_node);
	//}
	if (!add_children)
		return;
	
	for (dm_node *child_node : node_->children) {
		dm_scene_add_node(scene_, child_node);
	}
}



namespace internal {
	// would be lambda in dm_scene_remove_node, if lambdas wouldn't suck so much (try recursive lambda and look at syntax and call stack)
	void remove_node_from_list_recursive(dm_scene* scene_, dm_node* &node_) {
		for (auto child_node : node_->children) {
			remove_node_from_list_recursive(scene_, child_node);
		}
		scene_->nodes.Remove(node_);
	}
}

void
dm_scene_remove_node(dm_scene* scene_, dm_node* &node_)
{
	internal::remove_node_from_list_recursive(scene_, node_);
	dm_node_delete(node_, true);

	// maybe return list of pointer ids, that were removed, for easier integration with view
	// like:
	// struct modified nodes { dm_array<node*> modified_node, enum action { added, modified, deleted } };
	// return modified_nodes_; instead of void
	//
	// have separate structs for all other dm_scene-parts (modified_meshes, splines, ...)
	// have callback function, that can be observed
}



void
dm_scene_clone_node(dm_scene * scene_, dm_node * in_node, dm_node * out_node, bool clone_children)
{
	dm_node_clone(in_node, out_node, clone_children);

	dm_scene_add_node(scene_, in_node);

}

bool 
dm_scene_fix_node_hierarchy(dm_scene* scn, bool flushParentIds, bool recomputeChildrenPointer)
{
	for (dm_node* n : scn->nodes)
	{
		if (n->_parent_name.Len() > 0) {
			dm_node* parentNode = dm_scene_find_node(scn, n->_parent_name);
			if (flushParentIds) n->_parent_name = "";
			if (parentNode == nullptr) {
				return false;
			}
			else {
				n->parent = parentNode;
				if (recomputeChildrenPointer)
					parentNode->children.AddUnique(n);
			}
		}
	}
	return true;
}


dm_node* 
dm_scene_add_joint(dm_scene* scene, dm_scene_add_joint_params params)
{
	dm_node* joint_node = dm_node_create(dm_unique_name_generator(params.name, scene->nodes), params.transform, params.parent);
	dm_joint* joint_ = new dm_joint();
	joint_->name = dm_unique_name_generator_appended(params.name, "joint", scene->joints);
	joint_node->joint = joint_;
	scene->joints.Add(joint_);
	// bone geometry ...

	//dm_node* mesh_node = create_node(name + "_mesh", transform_, joint_node);
	//mesh_node->instance = new instance();
	//mesh_node->instance->name = name + "_inst";
	//mesh_node->instance->type = type_;
	//mesh_node->transform = instance_transform;
	//scene_->instances.Add(mesh_node->instance);

	dm_scene_add_node(scene, joint_node);

	return joint_node;
}



void 
dm_scene_primitive_apply_weights_from_nodes(dm_scene* scene, dm_node*& joint_node, bool clear_weights_in_joint)
{
	dm_joint* joint_ = joint_node->joint;

	int primitive_index = 0;
	for (auto prim : joint_->_primitive_sections) {

		uint32 number_of_joints = prim->joints.Num();
		uint32 number_of_vertices = prim->vertices.Num();

		if (prim->joint_ids2d.Num() < prim->vertices.Num()) {
			prim->joint_ids2d.SetNum(prim->vertices.Num());
		}


		// add only, if not already in list, and spread out weights array
		if (!prim->joints.Contains(joint_node)) {

			prim->joints.Add(joint_node);
			number_of_joints += 1;

			TArray<float> updated_weights;
			updated_weights.SetNumZeroed(number_of_vertices * number_of_joints);
			// if now more than 1 joint, need to spread out values [x1|x2|x1|x2|x1|x2|..] -> [x1|x2|.|x1|x2|.|x1|x2|.|x1..]
			if (number_of_joints > 1) {
				for (uint32 runner = 0, runner_updated = 0, sub_run = 0; runner < (number_of_vertices * (number_of_joints -1)); ++runner, ++sub_run, ++runner_updated) {
					if (sub_run == (number_of_joints - 1)) {
						//updated_weights[runner_updated] = 0.0f;
						++runner_updated;
						sub_run = 0;
					}
					updated_weights[runner_updated] = prim->weights[runner];
				}
			}
			prim->weights = updated_weights;


			TArray<dm_node*> updated_joint_ids;
			updated_joint_ids.SetNumZeroed(number_of_vertices * number_of_joints);
			//updated_joint_ids.SetNumUninitialized(number_of_vertices * number_of_joints);
			// if now more than 1 joint, spread out values - like with weights
			if (number_of_joints > 1) {
				for (uint32 runner = 0, runner_updated = 0, sub_run = 0; runner < (number_of_vertices * (number_of_joints - 1)); ++runner, ++sub_run, ++runner_updated) {
					if (sub_run == (number_of_joints - 1)) {
						++runner_updated;
						sub_run = 0;
					}
					updated_joint_ids[runner_updated] = prim->joint_ids[runner];
				}
			}
			prim->joint_ids = updated_joint_ids;

		}

		// fill with values
		int joint_index = prim->joints.Find(joint_node);

		for (int ji = 0; ji < joint_->_vertex_ids_in_section[primitive_index].Num(); ++ji) {
			uint32 vertex_id = joint_->_vertex_ids_in_section[primitive_index][ji];
			//p->weights[vertex_id * (joint_index + 1)] = joint_->_weights_in_section[index][ji];
			//uint32 weight_index = ((vertex_id + 1) * number_of_joints) - (number_of_joints - joint_index);
			//uint32 weight_index = (vertex_id + 1) * (joint_index + 1) - 1;
			//uint32 weight_index = (vertex_id + 1) * ()
			
			uint32 weight_index = ((vertex_id + 1) * number_of_joints - 1) - (number_of_joints - (joint_index + 1));
			prim->weights[weight_index] = joint_->_weights_in_section[primitive_index][ji];
			//prim->joint_ids[vertex_id * (number_of_joints - 1)] = joint_node;
			prim->joint_ids[weight_index] = joint_node;
			prim->joint_ids2d[vertex_id].Add(joint_node);
		}

		++primitive_index;
	}

	if (clear_weights_in_joint) {
		joint_->_primitive_sections.Empty();
		joint_->_vertex_ids_in_section.Empty();
		joint_->_weights_in_section.Empty();
	}
	
}

void 
dm_scene_node_calculate_bounds(dm_scene* scene_, dm_node*& joint_node, bool recalculate_existing_bounds)
{
	joint_node->joint->collision_bounds.bounds_type = dm_bounds::dm_bounds_type::bt_box;
	joint_node->joint->collision_bounds.param = dm_vec4(0, 0, 0, 0);

	// get joint node direction
	// see which children alligns with parent direction
	// set length to distance from former child
	// calculate xy based on mesh intersection or certain minimum
	dm_quat j_rot = joint_node->transform.GetRotation();
	dm_vec3 j_dir = j_rot.Vector();
	dm_vec3 j_pos = joint_node->transform.GetLocation();
	float smallest_angle = 10.0f;
	dm_node* smallest_angle_child = nullptr;
	for (auto child_j : joint_node->children) {
		dm_vec3 child_pos = child_j->transform.GetLocation();
		dm_vec3 child_parent_offset = child_pos - j_pos;
		dm_vec3 normalized_child_parent_offset = child_parent_offset;
		normalized_child_parent_offset.Normalize();
		float diff_angle = dm_vec3::DotProduct(normalized_child_parent_offset, j_dir);
		if (diff_angle < smallest_angle) {
			smallest_angle = diff_angle;
			smallest_angle_child = child_j;
			if (smallest_angle <= 0.01f) {
				break;
			}
		}
	}

	float collider_length = 1.0f;
	if (smallest_angle_child != nullptr) {
		dm_vec3 offset = smallest_angle_child->transform.GetLocation() - j_pos;
		collider_length = offset.Size();
		collider_length /= 100.0f;
	}
	float collider_thickness = 0.05f;

	joint_node->joint->collision_bounds.param = dm_vec4(collider_thickness, collider_thickness, collider_length, 0.0);

}

dm_scene* 
dm_scene_change_reference_frame(scene_reference_frame from, scene_reference_frame to, dm_scene* scene_in)
{
	return scene_in;
}

dm_transform 
dm_node_relative_to_local_transform(const dm_node* node_)
{	
	dm_transform trans = dm_transform::Identity;
	if (node_->parent != nullptr) {
		trans = dm_node_relative_to_local_transform(node_->parent);
	}
	return trans * node_->transform;
}

dm_transform 
dm_node_relative_to_world_transform(const dm_scene* scene_, const dm_node* node_)
{
	dm_transform tr = dm_transform_compute_relative_between(node_, scene_->nodes[0]);
	return tr;
}

dm_transform 
dm_transform_compute_relative_between(const dm_node* node_, const dm_node* parent_)
{
	dm_transform trans = dm_transform::Identity;
	if ((node_->parent != nullptr) && (node_ != parent_)) {
		trans = dm_transform_compute_relative_between(node_->parent, parent_);
	}
	return node_->transform * trans;
}

//node* 
//find_node_in_array(const node*& node_, const dm_array<node*>& node_array)
//{
//	for (auto& n : node_array) {
//		if (n == node_) {
//			return n;
//		}
//	}
//	return nullptr;
//}

uint32_t dm_scene_sizeof(dm_scene* scene)
{
	uint32_t scene_size = 0;

	for (auto mesh : scene->meshes)
		scene_size += sizeof(mesh);
	for (auto node : scene->nodes)
		scene_size += sizeof(node);
	for (auto material : scene->materials)
		scene_size += sizeof(material);
	for (auto animation : scene->animations)
		scene_size += sizeof(animation);

	return scene_size;
}

void dm_scene_recalculate_inverse_bind_matrices(dm_scene* dmscene, dm_skin* skin, dm_node* mesh_node)
{
	dm_node* root_node = nullptr;
	if (mesh_node) {
		root_node = mesh_node;
	}
	else {
		root_node = dm_node_array_get_roots(skin->joints)[0];
	}

	for (auto joint_node : skin->joints) {

		dm_transform tr = dm_transform_compute_relative_between(joint_node, root_node);
		//dm_transform tr = dm_transform_compute_relative_between(joint_node, dm_node_array_get_roots(dmscene->nodes)[0]);
		if (joint_node->joint) {
			joint_node->joint->inverse_bind_matrix = tr.ToInverseMatrixWithScale();
		}
	}
}

void dm_scene_merge_skins(dm_scene* dmscene)
{
	if (dmscene->skins.Num() < 2)
		return;

	//for (auto skin : dmscene->skins) {
	//	auto roots = get_root_joints(skin);
	//}

	auto skn = dmscene->skins[0];
	for (int i = 1; i < dmscene->skins.Num(); ++i) {
		//skin->joints.Append(dmscene->skins[i]->joints);	// can lead to duplicates
		for (auto& j : dmscene->skins[i]->joints) {
			skn->joints.AddUnique(j);
		}
		skn->inverse_bind_matrices.Append(dmscene->skins[i]->inverse_bind_matrices);
	}
	dmscene->skins.SetNum(1);
}

void 
dm_scene_recalculate_node_children(dm_scene* scene_)
{
	for (auto n : scene_->nodes) {
		n->children.Reset();
	}
	for (auto n : scene_->nodes) {
		if (n->parent) {
			n->parent->children.Add(n);
		}
	}
}

void 
dm_scene_recalculate_joint_bounds(dm_scene* dmscene)
{
	for (auto n : dmscene->nodes) {
		dm_joint* j = n->joint;
		if (j == nullptr) {
			continue;
		}
		//j->collision_bounds.type = dm_bounds::bounds_type::bt_box;
		//j->collision_bounds.pivot = dm_bounds::pivot_type::pt_origin;
		j->collision_bounds.color = j->_color;
		//j->collision_bounds.param = vec4(0.05, 0.05, 0.5, 0);
		dm_scene_node_calculate_bounds(dmscene, n, false);
	}
}

void dm_scene_fixup_skins(dm_scene* dmscene, dm_scene_fixup_skins_params params)
{
	//merge_skins = false;
	//params.merge_skins = false;
	if (params.merge_skins) {
		dm_scene_merge_skins(dmscene);
	}

	bool remove_duplicates = true;
	if (remove_duplicates) {
		for (auto skn : dmscene->skins) {
			dm_array<dm_node*> joint_list = {};
			for (auto& j : skn->joints) {
				joint_list.AddUnique(j);
			}
			skn->joints = joint_list;
		}
	}

	bool retraverse_skin = true;
	if (retraverse_skin) {

		// recursive lambda - requires c++14
		const auto process_joint = [&](const auto& self, dm_array<dm_node*>& joint_list, dm_node*& node_to_add) -> void {
			joint_list.Add(node_to_add);
			for (auto& children : node_to_add->children) {
				self(self, joint_list, children);
			}
		};

		for (auto skn : dmscene->skins) {
			dm_array<dm_node*> root_joints = dm_node_array_get_roots(skn->joints);
			dm_array<dm_node*> updated_joint_list = {};

			for (auto& root : root_joints) {
				process_joint(process_joint, updated_joint_list, root);
			}
			skn->joints = updated_joint_list;
		}

	}


	//params.sort_skin_nodes = false;
	if (params.sort_skin_nodes) {
		for (auto& skn : dmscene->skins) {
			skn->joints = dm_node_array_sort_depth_first(skn->joints);
		}
	}

	if (params.add_missing_nodes) {

	}

	//bool _remove_control_bones = false;
	if (params.remove_control_bones) {
		// recursive lambda - requires c++14
		const auto check_for_removal = [&](const auto& self, dm_node*& node_to_remove) -> bool {
			bool remove_node = true;
			for (auto n : node_to_remove->children) {
				if (n->joint != nullptr)
					remove_node = false;
				else
					remove_node = self(self, n);
			}
			return remove_node;
		};

		for (auto& skn : dmscene->skins) {

			auto joints = skn->joints;
			dm_array<dm_node*> remove_list = {};
			for (auto j : joints) {
				if (j->joint == nullptr) {
					remove_list.Add(j);
				}
			}
			//auto sorted_remove_list = dm_node_array_sort_depth_first(remove_list);
			//auto roots = dm_node_array_get_roots(sorted_remove_list);
			dm_array<dm_node*> checked_remove_list = {};

			for (auto& j : remove_list) {
				bool remove = check_for_removal(check_for_removal, j);
				if (remove) {
					checked_remove_list.Add(j);
				}
			}
			for (auto& j : checked_remove_list) {
				skn->joints.Remove(j);
			}
		}
	}

	bool recalculate_parent_joints = true;
	if (recalculate_parent_joints) {

		const auto find_parent_from_array = [&](const auto& self, dm_node*& node_, const dm_array<dm_node*>& node_array) -> dm_node * {
			dm_node* parent_joint = nullptr;

			if (node_array.Contains(node_->parent)) {
				parent_joint = node_->parent;
			}
			else if (node_->parent != nullptr) {
				parent_joint = self(self, node_->parent, node_array);
			}
			return parent_joint;
		};

		for (auto skn : dmscene->skins) {
			for (auto& j : skn->joints) {
				//if (skin->joints.Contains(j->parent)) {
				//	j->joint->parent_joint = j->parent;
				//}
				//else {
				//	if (skin->joints.Contains(j->parent->parent)) {
				//		j->joint->parent_joint = j->parent->parent;
				//	}
				//}
				if (j->joint)
					j->joint->parent_joint = find_parent_from_array(find_parent_from_array, j, skn->joints);
			}
		}
	}

	bool recalculate_joint_offsets = true;
	if (recalculate_joint_offsets) {
		for (auto skn : dmscene->skins) {
			for (auto& j : skn->joints) {

				if ((j->joint != nullptr) && (j->joint->parent_joint != nullptr)) {
					dm_transform offset = dm_transform_compute_relative_between(j, j->joint->parent_joint);
					j->joint->offset_transform = offset;
					j->joint->offset = offset.ToMatrixWithScale();
				}
				else if (j->joint != nullptr) {
					j->joint->offset_transform = j->joint->ibt;
					j->joint->offset = j->joint->inverse_bind_matrix;
				}
			}
		}
	}

	//bool remove_non_joints = true;
	//if (remove_non_joints) {
	//	for (auto skin : dmscene->skins) {
	//		dm_array<dm_node*> updated_joint_list = {};
	//		for (auto& j : skin->joints) {
	//			if (j->joint != nullptr) {
	//				updated_joint_list.Add(j);
	//			}
	//		}
	//		skin->joints = updated_joint_list;
	//	}
	//}



	if (params.combine_root_nodes) {

	}


	bool fixup_ibm = true;
	if (fixup_ibm) {
		for (auto skn : dmscene->skins) {
			dm_scene_recalculate_inverse_bind_matrices(dmscene, skn, nullptr);
		}
	}

}
