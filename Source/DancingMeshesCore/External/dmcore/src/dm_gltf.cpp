// Copyright branedev, Inc. All Rights Reserved.

#include "dm_gltf.h"
#include "dm_logging.h"
#include "dm_scene_types.h"
#include "dm_scene.h"
#include "dm_core.h"
#include "dm_scene_io.h"

#define CGLTF_IMPLEMENTATION
#include "../external/cgltf/cgltf.h"

//assimp
//joint_weights 7030
//joint_weights_stride 76
//vertex_count_minus_1 369
//vertex_count_at_least_1 1
//
//cgltf
//joint_weights 1480
//joint_weights_stride 76
//vertex_count_minus_1 369
//vertex_count_at_least_1 1
//
//com.qti.pasrservice
//Android Services Library
//com.facebook.spatial_persistence_service
//oculus.platform

struct dm_gltf
{
	cgltf_data* data;
};

// Math type conversions
dm_mat4 dm_mat4_from_cgltf(const cgltf_float* mat_in) {
	dm_mat4 mat_out;
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			mat_out.M[i][j] = mat_in[(j * 4) + i];
		}
	}
	mat_out.ScaleTranslation({ 100.0f, 100.0f, 100.0f });
	return mat_out;	
}
dm_vec3 dm_vec3_from_cgltf(const cgltf_float* vec_in) {
	return dm_vec3(vec_in[0], vec_in[1], vec_in[2]);
}
dm_vec2 dm_vec2_from_cgltf(const cgltf_float* vec_in) {
	return dm_vec2(vec_in[0], vec_in[1]);
}
dm_quat dm_quat_from_cgltf(const cgltf_float* rot_in) {
	//return dm_quat({ rot_in[0], rot_in[1], rot_in[2], rot_in[3] });
	return dm_quat(rot_in[0], rot_in[1], rot_in[2], rot_in[3]);
}
dm_transform dm_transform_from_cgltf(const cgltf_float* mat_in) {
	dm_mat4 m = dm_mat4_from_cgltf(mat_in);
	m.ScaleTranslation({ 100.0f, 100.0f, 100.0f });
	return dm_transform(m);
}
dm_color dm_color_from_cgltf(const cgltf_float* color_in) {
	return dm_color( color_in[0], color_in[1], color_in[2], 1.0f );
}

dm_string dm_cgltf_id_to_string(unsigned int id) {
	return FString::FromInt(id);
}

// node: name, transform, parent_name. needs cleanup and afterpass, for full conversion
dm_node* dm_node_from_cgltf(const cgltf_node* node)
{
	// transforms
	dm_transform transform;
	if (node->has_matrix) {
		transform = dm_transform_from_cgltf(node->matrix);
	}
	else {
		if (node->has_translation) {
			transform.SetTranslation(dm_vec3_from_cgltf(node->translation) * 100.0f);
		}
		if (node->has_rotation) {
			transform.SetRotation(dm_quat_from_cgltf(node->rotation));
		}
		if (node->has_scale) {
			transform.SetScale3D(dm_vec3_from_cgltf(node->scale));
		}
	}
	dm_node* n = dm_node_create(node->name, transform);
	
	// parent2
	if (node->parent != nullptr) {
		n->_parent_name = node->parent->name;
	}

	return n;
}

dm_mesh* dm_mesh_from_cgltf(dm_scene* scene, const cgltf_mesh* mesh)
{
	dm_mesh* m = new dm_mesh();
	m->name = mesh->name;
	m->name.Append("_mesh");

	for (int i = 0; i < mesh->primitives_count; ++i) 
	{
		dm_primitive p;
		p.primitive_type = dm_primitive::dm_primitive_type::triangles;

		cgltf_primitive* cp = &mesh->primitives[i];

		//cgltf_accessor_unpack_uints(cp->indices, &p.indices.GetData()[0], cp->indices->count);
		for (int index_ = 0; index_ < cp->indices->count; ++index_) {
			p.indices.Add(cgltf_accessor_read_index(cp->indices, index_));
		}
		

		for (int j = 0; j < cp->attributes_count; ++j) 
		{
			cgltf_attribute* attr = &cp->attributes[j];
			switch (attr->type)
			{
			case cgltf_attribute_type::cgltf_attribute_type_position:
			{
				int num_of_elements = attr->data->count;
				auto& vert = p.vertices;
				p.vertices.SetNum(num_of_elements);
				if (num_of_elements > 0)
					cgltf_accessor_unpack_floats(attr->data, &p.vertices.GetData()->X, num_of_elements * cgltf_num_components(attr->data->type));
				for (dm_vec3& v : p.vertices) {
					v *= 100.0f;
				}
				break;
			};
			case cgltf_attribute_type::cgltf_attribute_type_normal:
			{
				int num_of_elements = attr->data->count;
				p.normals.SetNum(num_of_elements);
				if (num_of_elements > 0)
					cgltf_accessor_unpack_floats(attr->data, &p.normals.GetData()->X, num_of_elements * cgltf_num_components(attr->data->type));
				break;
			};
			case cgltf_attribute_type::cgltf_attribute_type_color:
			{
				int num_of_elements = attr->data->count;
				p.vertex_colors.SetNum(num_of_elements);
				// gltf standard allows either 3 or 4 elements colors, https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#meshes
				// dm_color always 4 elements, need to extra copy data around in case data is different
				//cgltf_accessor_unpack_floats(attr->data, &p.vertex_colors.GetData()->R, num_of_elements * 4);
				break;
			};
			case cgltf_attribute_type::cgltf_attribute_type_texcoord:
			{
				int num_of_elements = attr->data->count;
				p.uvs.SetNum(num_of_elements);
				if (num_of_elements > 0)
					cgltf_accessor_unpack_floats(attr->data, &p.uvs.GetData()->X, num_of_elements * cgltf_num_components(attr->data->type));
				break;
			};
			case cgltf_attribute_type::cgltf_attribute_type_tangent:
			{
				int num_of_elements = attr->data->count;
				p.tangents.SetNum(num_of_elements);
				if (num_of_elements > 0)
					cgltf_accessor_unpack_floats(attr->data, &p.tangents.GetData()->X, num_of_elements * cgltf_num_components(attr->data->type));
				break;
			};
			case cgltf_attribute_type::cgltf_attribute_type_weights:
			{
				int num_of_elements = attr->data->count;
				int num_of_components = cgltf_num_components(attr->data->type);	// packed in 4
				p.weights.SetNum(num_of_elements * num_of_components);
				//if (num_of_elements > 0)
				//	cgltf_accessor_unpack_floats(attr->data, &p.weights.GetData()[0], num_of_elements);
				for (int index_ = 0; index_ < num_of_elements; ++index_) {
					float weights[4];
					cgltf_accessor_read_float(attr->data, index_, weights, num_of_components);
					memcpy(&p.weights.GetData()[index_ * num_of_components], weights, sizeof(float) * num_of_components);
				}
				break;
			};
			case cgltf_attribute_type::cgltf_attribute_type_joints:
			{
				// JOINTS_0: VEC4
				// skin joint ids. packed in 4, even when only 1 or 2 joints are influencing the mesh, example: (0,1,0,0) means skin->joints[0] & skin->joints[1]
				int num_of_elements = attr->data->count;
				int num_of_components = cgltf_num_components(attr->data->type);	// packed in 4
				p.joint_ids2d.SetNum(num_of_elements);
				for (int index_ = 0; index_ < num_of_elements; ++index_) {
					unsigned int ids[4];
					bool success = cgltf_accessor_read_uint(attr->data, index_, ids, 4) == 1 ? true : false;
					if (success) {
						//[todo] only supports scenes with 1 skin at this point
						if (scene->skins[0]) {
							//[todo] joint_ids2d and joint_ids are same, but different datastructures, clean this up.
							//[todo] save as skin->joint indices instead of pointers to nodes, as speed up is imense vs searching through list each time
							p.joint_ids2d[index_].Add(scene->skins[0]->joints[ids[0]]);
							p.joint_ids2d[index_].Add(scene->skins[0]->joints[ids[1]]);
							p.joint_ids2d[index_].Add(scene->skins[0]->joints[ids[2]]);
							p.joint_ids2d[index_].Add(scene->skins[0]->joints[ids[3]]);

							p.joint_ids.Add(scene->skins[0]->joints[ids[0]]);
							p.joint_ids.Add(scene->skins[0]->joints[ids[1]]);
							p.joint_ids.Add(scene->skins[0]->joints[ids[2]]);
							p.joint_ids.Add(scene->skins[0]->joints[ids[3]]);

							//[todo] not clear why those references are needed, as they are also available in skins. delete from scene->joint struct.
							p.joints.AddUnique(scene->skins[0]->joints[ids[0]]);
							p.joints.AddUnique(scene->skins[0]->joints[ids[1]]);
							p.joints.AddUnique(scene->skins[0]->joints[ids[2]]);
							p.joints.AddUnique(scene->skins[0]->joints[ids[3]]);
						}
					}
				}
				// gltf always fills weights to 4 joints, so add dummy joints (are going to be multiplied out by 0.0 weights)
				while (p.joints.Num() < 4) {
					dm_node* n = p.joints.Last();
					p.joints.Add(n);
				}

				break;
			};
			}
		}
		m->primitives.Add(p);
	} 
	return m;
}
dm_material* dm_material_from_cgltf(const cgltf_material* material)
{
	dm_material* mat = new dm_material();
	mat->name = material->name;

	return mat;
}

dm_skin* dm_skin_from_cgltf(dm_scene* scene, const cgltf_skin* skin)
{
	dm_skin* s = new dm_skin();
	s->name = skin->name;

	// inverse bind matrices
	int num_of_ibm = skin->inverse_bind_matrices->count;
	s->inverse_bind_matrices.SetNum(num_of_ibm);
	cgltf_accessor_unpack_floats(skin->inverse_bind_matrices, &s->inverse_bind_matrices.GetData()->M[0][0], num_of_ibm * 16);

	for (dm_mat4& m : s->inverse_bind_matrices) {
		m = m.GetTransposed();
		m.ScaleTranslation({ 100.0f, 100.0f, 100.0f });
	}

	for (int i = 0; i < skin->joints_count; ++i) {
		cgltf_node* j = skin->joints[i];
		dm_node* n = dm_scene_find_node(scene, j->name);
		if (n->joint == nullptr) {
			n->joint = new dm_joint();
			n->joint->inverse_bind_matrix = s->inverse_bind_matrices[i];
			n->joint->name = n->name;
			n->joint->ibtm.SetFromMatrix(n->joint->inverse_bind_matrix);
			//n->joint->ibt.SetFromMatrix(n->joint->inverse_bind_matrix);
			dm_vec3 t = n->joint->inverse_bind_matrix.GetOrigin();
			dm_quat q = n->joint->inverse_bind_matrix.Rotator().Quaternion();
			dm_vec3 sc = n->joint->inverse_bind_matrix.ExtractScaling();
			n->joint->ibt.SetComponents(q, t, sc);

			scene->joints.Add(n->joint);
		}
		s->joints.Add(n);
	}
	return s;
}

dm_animation* dm_animation_from_cgltf(dm_scene* scene, const cgltf_animation* animation)
{
	dm_animation* anim = new dm_animation();
	anim->name = animation->name;
	anim->frames_per_second = 30;
	anim->length = 0.0f;

	for (int i = 0; i < animation->channels_count; ++i) {
		cgltf_animation_channel* channel = &animation->channels[i];
		cgltf_animation_path_type path_type = channel->target_path;
		cgltf_accessor* input = channel->sampler->input;
		cgltf_accessor* output = channel->sampler->output;
		cgltf_node* n = channel->target_node;

		int num_of_inputs = input->count;
		int num_of_outputs = output->count;
		dm_node* node = dm_scene_find_node(scene, n->name);	// instead of looking through whole hierarchy create skin based lookup

		switch (path_type) {
		case cgltf_animation_path_type_translation:
		{
			dm_animation::dm_node_translation_track track(dm_animation::dm_node_translation_track::dm_track_type::translation);
			track.target = node;
			track.values.in.SetNum(num_of_inputs);
			track.values.out.SetNum(num_of_outputs);
			cgltf_accessor_unpack_floats(input, &track.values.in.GetData()[0], num_of_inputs);
			cgltf_accessor_unpack_floats(output, &track.values.out.GetData()->X, num_of_outputs * 3);
			// scale for unreal
			for (dm_vec3& v : track.values.out) {
				v *= 100.0f;
			}
			anim->node_translation_tracks.Add(track);

			// write animation length
			if (track.values.in.Last() > anim->length) {
				anim->length = track.values.in.Last();
			}
			break;
		};
		case cgltf_animation_path_type_rotation:
		{
			dm_animation::dm_node_rotation_track track(dm_animation::dm_node_rotation_track::dm_track_type::rotation);
			track.target = node;
			track.values.in.SetNum(num_of_inputs);
			track.values.out.SetNum(num_of_outputs);cgltf_accessor_unpack_floats(input, &track.values.in.GetData()[0], num_of_inputs);
			cgltf_accessor_unpack_floats(output, &track.values.out.GetData()->X, num_of_outputs * 4);
			anim->node_rotation_tracks.Add(track);

			// write animation length
			if (track.values.in.Last() > anim->length) {
				anim->length = track.values.in.Last();
			}
			break;
		}
		case cgltf_animation_path_type_scale:
		{
			dm_animation::dm_node_scale_track track(dm_animation::dm_node_scale_track::dm_track_type::rotation);
			track.target = node;
			track.values.in.SetNum(num_of_inputs);
			track.values.out.SetNum(num_of_outputs);
			cgltf_accessor_unpack_floats(input, &track.values.in.GetData()[0], num_of_inputs);
			cgltf_accessor_unpack_floats(output, &track.values.out.GetData()->X, num_of_outputs * 3);
			anim->node_scale_tracks.Add(track);
			// write animation length
			if (track.values.in.Last() > anim->length) {
				anim->length = track.values.in.Last();
			}
			break;
		}
		case cgltf_animation_path_type_weights:
		{
			break;
		}
		case cgltf_animation_path_type_invalid:
		{
			break;
		}
		}
	}
	return anim;
}


dm_status dm_scene_from_cgltf(dm_scene* scene, cgltf_data* data, cgltf_options* options)
{
	// nodes prepass
	// makes sure every cgltf_node has a name, that can be used as id in node main pass
	void* (*memory_alloc)(void*, cgltf_size) = &cgltf_default_alloc;
	for (int node_index = 0; node_index < data->nodes_count; ++node_index) {
		cgltf_node* node = &data->nodes[node_index];
		if (!node->name) {
			//char buffer[16];
			//itoa(node_index, buffer, 10);
			//std::to_string(node_index);
			node->name = (char*)malloc(16 * sizeof(char)); //memory_alloc(//cgltf_calloc(options, 8, 255);//options->memory.alloc(options->memory.user_data, 256);
			strcpy(node->name, std::to_string(node_index).c_str());
		}
	}
	// node main pass
	for (int node_index = 0; node_index < data->nodes_count; ++node_index) {
		cgltf_node* cn = &data->nodes[node_index];
		dm_node* n = dm_node_from_cgltf(cn);
		dm_scene_add_node(scene, n);
	}
	dm_scene_fix_node_hierarchy(scene, true, true);
	//scene->nodes = dm_node_array_sort_depth_first(scene->nodes);

	// skins
	for (int skin_index = 0; skin_index < data->skins_count; ++skin_index) {
		dm_skin* skin;// = new dm_skin();
		skin = dm_skin_from_cgltf(scene, &data->skins[skin_index]);
		if (skin != nullptr)
			scene->skins.Add(skin);
	}
	// materials
	for (int mat_index = 0; mat_index < data->materials_count; ++mat_index) {
		dm_material* mat;// = new dm_material();
		mat = dm_material_from_cgltf(&data->materials[mat_index]);
		scene->materials.Add(mat);
	}
	// meshes
	for (int mesh_index = 0; mesh_index < data->meshes_count; ++mesh_index) {
		dm_mesh* m;// = new dm_mesh();
		m = dm_mesh_from_cgltf(scene, &data->meshes[mesh_index]);
		scene->meshes.Add(m);
	}
	// animations
	for (int anim_index = 0; anim_index < data->animations_count; ++anim_index) {
		dm_animation* anim;
		anim = dm_animation_from_cgltf(scene, &data->animations[anim_index]);
		if (anim != nullptr)
			scene->animations.Add(anim);
	}
	
	// fill missing node references
	for (int node_index = 0; node_index < data->nodes_count; ++node_index) {
		cgltf_node* cnode = &data->nodes[node_index];

		// skins
		if (cnode->skin != nullptr) {
			dm_string skin_name = cnode->skin->name;
			//skin_name.Append("_skin");
			dm_skin* skin = dm_scene_find_skin(scene, skin_name);
			dm_node* n = dm_scene_find_node(scene, cnode->name);
			n->skin = skin;
		}

		// instances
		if (cnode->mesh != nullptr) {
			dm_string mesh_name = cnode->mesh->name;
			mesh_name.Append("_mesh");
			dm_mesh* mesh = dm_scene_find_mesh(scene, mesh_name);
			dm_node* n = dm_scene_find_node(scene, cnode->name);
			n->instance = new dm_instance();
			n->instance->name = cnode->mesh->name;
			n->instance->name.Append("_instance");
			n->instance->mesh = mesh;
			n->instance->type = dm_instance::dm_instance_type::it_custom_mesh;
			scene->instances.Add(n->instance);

			for (int prim_index = 0; prim_index < cnode->mesh->primitives_count; ++prim_index) {
				cgltf_primitive* prim = &cnode->mesh->primitives[prim_index];
				if (prim->material != nullptr) {
					dm_string mat_name = prim->material->name;
					dm_material* mat = dm_scene_find_material(scene, mat_name);
					n->instance->mesh->primitives[prim_index].material = mat;
				}
			}

			bool recompute_ibm = false;
			if (recompute_ibm) {
				if (n->skin && n->instance->mesh) {
					dm_scene_recalculate_inverse_bind_matrices(scene, n->skin, n);
				}
			}
		}
	}

	return dm_status();
}

#include "dm_python.h"

dm_status 
dm_gltf_load(dm_scene** scene, const dm_string filepath)
{
	dm_log_msg("dm_gltf_load entered...");

	//dm_python_init();

	cgltf_options options = { };
	cgltf_data* data = nullptr;

	cgltf_result result = cgltf_parse_file(&options, TCHAR_TO_ANSI(*filepath), &data);
	if (result != cgltf_result_success)
		return dm_status(dm_status::dm_status_code::io_error);

	result = cgltf_load_buffers(&options, data, TCHAR_TO_ANSI(*filepath));
	if (result != cgltf_result_success)
		return dm_status(dm_status::dm_status_code::io_error);

	dm_scene_create(scene);
	dm_scene_from_cgltf(*scene, data, &options);
	dm_scene_save(*scene, "D:\\savedscene_cgltf.json");

	// set metadata
	filepath.Split("/", &(*scene)->_filepath, &(*scene)->_filename, ESearchCase::IgnoreCase, ESearchDir::FromEnd);

	cgltf_free(data);
	dm_log_msg("dm_gltf_load finished.");

	return dm_status_okay();
}

dm_status dm_gltf_save(const dm_scene* scene, const dm_string path)
{
	return dm_status(dm_status::not_implemented);
}
