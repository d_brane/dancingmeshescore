
#include "dm_python.h"
#include "dm_string.h"
#include "dm_logging.h"
#include <string>

#if DM_WITH_PYTHON
// UE4 redefines check() macro, so need to push-pop during include 
#pragma push_macro("check")
#undef check
#define PY_MAJOR_VERSION 3
#define PY_MINOR_VERSION 8
#include "../external/pybind11/include/pybind11/pybind11.h"
#include "../external/pybind11/include/pybind11/embed.h"
#pragma pop_macro("check")

namespace py = pybind11;
using namespace py::literals;
#endif // DM_WITH_PYTHON


int add(int i, int j) {
    return i + j;
}


#if DM_WITH_PYTHON
//PYBIND11_MODULE(example, m) {
//    m.doc() = "pybind11 example plugin"; // optional module docstring
//
//    m.dec_ref("add", &add, "A function which adds two numbers");
//}

PYBIND11_EMBEDDED_MODULE(embedded, m) {
    m.def("add_embedded", [](int i, int j) {
            return i + j;
        });
}
#endif

void dm_python_init()
{
#if DM_WITH_PYTHON
    dm_log_msg("[dma] python exec...");
    Py_SetPythonHome(const_cast<wchar_t*>(L"C:/Users/ademe/AppData/Local/Programs/Python/Python38"));

    py::scoped_interpreter guard{}; // start the interpreter and keep it alive

    auto locals = py::dict("name"_a = "World", "number"_a = 42);
    py::exec(R"(
        message = "Hello, {name}! The answer is {number}".format(**locals())
    )", py::globals(), locals);

    auto message = locals["message"].cast<std::string>();
    dm_log_msg(message.c_str());

    auto embedded = py::module::import("embedded");
    auto result = embedded.attr("add_embedded")(1, 2).cast<int>();
    //py::exec(R"()");

    dm_log_msg(dm_string::FromInt(result));

    //try {
    //    dm_log_msg("entering py::print");
    //    py::print("hello again");
    //}
    //catch (pybind11::error_already_set &e) {
    //    dm_log_msg("exception thrown py::print");
    //    dm_log_msg(e.what());
    //}    
    dm_log_msg("[dma] python exec end...");
#endif
}