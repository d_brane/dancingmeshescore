// Copyright branedev, Inc. All Rights Reserved.
#include "dm_assimp.h"
#include "dm_core.h"
#include "dm_std_helper.h"
#include "dm_logging.h"
#include "dm_common.h"
#include "dm_scene.h"
#include "dm_scene_io.h"

#include <assimp/cimport.h>        // Plain-C interface
#include <assimp/scene.h>          // Output data structure
#include <assimp/postprocess.h>    // Post processing flags
#include <assimp/Importer.hpp>

// note:
// rhs y up -> +Y up/+X right/+Z out of the screeen
// uvs: lower-left corner is (0,0) - can be changed with aiProcess_FlipUVs to upper-left
// matrices: row major. order in array: x1, y1, z1, t1, x2, y2, ...
// - all names (nodes,..) are unique
// - each mesh has max 1 material (ref by index)
// - each mesh has can have multiple bones
// - each skin gets imported by looking for same root (different roots -> different skins)
// - each node can reference multiple meshes/animations keys/...
// - a node references the mesh by index, different nodes can point to same mesh (in a form of instancing)

dm_status 
dm_assimp_save(const dm_scene* scene, const dm_string path)
{
	return dm_status(dm_status::not_implemented);
}

dm_status
internal::dm_assimp_create_from_file(const aiScene** assimp_scene, const dm_string file)
{ 
	dm_log_msg("begin load_assimp_scene, path: " + file);
	//const aiScene* scene = aiImportFile(dm_string_to_std(file).c_str(),
	*assimp_scene = aiImportFile(dm_string_to_std(file).c_str(),
		aiProcess_FlipUVs |
		aiProcess_MakeLeftHanded |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_PopulateArmatureData |
		aiProcess_GenNormals //|
		//aiProcessPreset_TargetRealtime_Quality |
		//aiProcess_FlipWindingOrder |
		//aiProcess_FixInfacingNormals// |
		//aiProcess_JoinIdenticalVertices |
		//aiProcess_ImproveCacheLocality|
		//aiProcess_LimitBoneWeights|
		//aiProcess_PreTransformVertices|
		//aiProcess_TransformUVCoords|
		//aiProcess_FindInstances|
		//aiProcess_OptimizeMeshes|
		//aiProcess_OptimizeGraph|
		//aiProcess_GenUVCoords |
		//aiProcess_FindDegenerates|
		//aiProcess_FindInvalidData|
		//aiProcess_SplitLargeMeshes|
		//aiProcess_RemoveRedundantMaterials//|
		//aiProcess_SortByPType
	);

	if (!*assimp_scene)
	{
		*assimp_scene = nullptr;
		dm_status status;
		status.error_string = dm_string(aiGetErrorString());
		status.state = dm_status::dm_status_code::file_import_failed;
		return status;
	}
	return dm_status_okay();
}

void 
internal::dm_assimp_release(const aiScene* scene) 
{
	aiReleaseImport(scene);
}

dm_status
dm_assimp_load(dm_scene** scene, const dm_string path)
{
	const aiScene* ass_scene = nullptr;
	internal::dm_assimp_create_from_file(&ass_scene, path);

	dm_string err;
	dm_checkf(ass_scene, dm_text("%s"), err.GetCharArray().GetData());

	dm_scene_create(scene);
	path.Split("/", &(*scene)->_filepath, &(*scene)->_filename, ESearchCase::IgnoreCase, ESearchDir::FromEnd);

	internal::dm_scene_from_assimp(ass_scene, *scene);
	internal::dm_assimp_release(ass_scene);

	return dm_status_okay();
}

dm_node* 
internal::dm_node_create_from_assimp(const aiNode* Node)
{
	dm_string name = internal::dm_string_from_assimp(Node->mName);
	dm_string parentName("");
	if (Node->mParent != nullptr) {
		parentName = internal::dm_string_from_assimp(Node->mParent->mName);
	}
	dm_mat4 nodeMat = dm_mat4_from_assimp(Node->mTransformation);
	dm_transform nodeTransform = dm_transform(nodeMat);

	return dm_node_create(name, nodeTransform, parentName);
}

dm_mesh* 
internal::dm_mesh_create_from_assimp(const aiMesh* Mesh)
{
	return nullptr;
}

dm_mat4
internal::dm_mat4_from_assimp(const aiMatrix4x4& matrixIn)
{
	dm_mat4 matrixOut;

	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			matrixOut.M[i][j] = matrixIn[j][i];
		}
	}
	// build matrix to transform coordinate space from Yup to Zup
	//FMatrix translate(FPlane(1, 0, 0, 0),
	//					FPlane(0, 0, 1, 0),
	//					FPlane(0, 1, 0, 0),
	//					FPlane(0, 0, 0, 1));
	//matrixOut = translate * matrixOut * translate.Inverse();

	//matrixOut = matrixOut.GetTransposed();
	matrixOut.ScaleTranslation({ 100.0f, 100.0f,100.0f });
	return matrixOut;
}

dm_vec2
internal::dm_vec2_from_assimp(const aiVector2D& vecIn)
{
	return dm_vec2(vecIn.x, vecIn.y);
}

dm_vec3
internal::dm_vec3_from_assimp(const aiVector3D& vecIn)
{
	//FMatrix translate(FPlane(1, 0, 0, 0),
	//				  FPlane(0, 0, 1, 0), 
	//				  FPlane(0, 1, 0, 0),
	//				  FPlane(0, 0, 0, 1) );
	//v = translate.TransformVector(v);
	//return v;
	//return dm_vec3(vecIn[0], vecIn[2], vecIn[1]);
	return dm_vec3(vecIn[0], vecIn[1], vecIn[2]);
}

dm_quat
internal::dm_quat_from_assimp(const aiQuaternion& quatIn)
{
	//return dm_quat(quatIn.x, -quatIn.z, -quatIn.y, quatIn.w);
	return dm_quat(quatIn.x, quatIn.y, quatIn.z, quatIn.w);
}

dm_transform 
internal::dm_transform_from_assimp(const aiMatrix4x4& matrixIn)
{
	dm_mat4 m = dm_mat4_from_assimp(matrixIn);
	return dm_transform(m);
}

dm_color
internal::dm_color_from_assimp(const aiColor4D& colorIn)
{
#if DM_PLATFORM_ANDROID || DM_PLATFORM_LINUX
	return dm_color( colorIn[0], colorIn[1], colorIn[2], colorIn[3] );
#else
	return dm_color({ colorIn[0], colorIn[1], colorIn[2], colorIn[3] });
#endif
}

dm_string 
internal::dm_string_from_assimp(const aiString& stringIn)
{
	return stringIn.C_Str();
}

//const aiNode* 
//internal::find_assimp_node_by_name(const aiString& Name, const aiNode* RootNode)
//{
//	return RootNode->FindNode(Name);
//	//if (Name == RootNode->mName) 
//	//	return RootNode;

//	//for (uint32 i = 0; i < RootNode->mNumChildren; ++i) {
//	//	RootNode->mChildren[i];d
//	//}
//}

void process_material(const aiMesh* Mesh, const aiNode* ParentNode, const aiScene* aiscene, dm_scene* dmscene, dm_primitive* dmprimitive) {
	aiMaterial* aimat = aiscene->mMaterials[Mesh->mMaterialIndex];
	dm_string material_name = aimat->GetName().C_Str();
	dm_material* material = dm_scene_find_material(dmscene, material_name);

	if (material == nullptr) {
		material = new dm_material();
		material->name = material_name;
		dmscene->materials.Add(material);

		// fill material params
		uint32 number_of_diffuse_textures = aimat->GetTextureCount(aiTextureType::aiTextureType_DIFFUSE);
		for (uint32 i = 0; i < number_of_diffuse_textures; ++i) {
			aiString aitexture_path;
			aimat->GetTexture(aiTextureType::aiTextureType_DIFFUSE, i, &aitexture_path);

			// is texture in question embedded?
			if (auto aitex = aiscene->GetEmbeddedTexture(aitexture_path.C_Str())) {
				//aitex->pcData
			}
			else {
				// load texture via filepath
				if (material->diffuse_map == nullptr) {
					material->diffuse_map = new dm_texture();
					material->diffuse_map->uri = dmscene->_filepath + "/" + internal::dm_string_from_assimp(aitexture_path);
				}
			}
		}

	}
	dmprimitive->material = material;
}

void process_animations(const aiScene* aiscene, dm_scene* dmscene/*, bool scale_translation_track_100*/) {
	if (!aiscene->HasAnimations()) {
		return;
	}
			
	for (uint32 anim_index = 0; anim_index < aiscene->mNumAnimations; ++anim_index) {
		aiAnimation* aianim = aiscene->mAnimations[anim_index];
		double ticks = aianim->mDuration;
		double tps = aianim->mTicksPerSecond > 0 ? aianim->mTicksPerSecond : 30;	// fallback to 30, in case it's unspecified
		//double fps = 1 / tps;
		double anim_length = ticks / tps;
		dm_string anim_name = aianim->mName.C_Str();

		dm_animation* dmanim = new dm_animation();
		dmanim->name = anim_name;
		dmanim->frames_per_second = tps;
		dmanim->frames = ticks;
		dmanim->length = anim_length;
		dmscene->animations.Add(dmanim);

		bool scale_translation_track_100 = true;
		float translation_scaler = 1.0f;
		if (scale_translation_track_100)
			translation_scaler = 100.0f;

		//[todo] handle ainode->mPreState

		for (uint32 channel_index = 0; channel_index < aianim->mNumChannels; ++channel_index) {
			aiNodeAnim* ainode = aianim->mChannels[channel_index];
			dm_node* dmnode = dm_scene_find_node(dmscene, ainode->mNodeName.C_Str());

			if (uint32 num_position_keys = ainode->mNumPositionKeys) {
				dm_animation::dm_node_translation_track track(dm_animation::dm_node_translation_track::dm_track_type::translation);
				track.target = dmnode;
				for (uint32 key_index = 0; key_index < num_position_keys; ++key_index) {
					double time = ainode->mPositionKeys[key_index].mTime / tps;
					dm_vec3 pos = internal::dm_vec3_from_assimp(ainode->mPositionKeys[key_index].mValue) * translation_scaler;
					track.values.in.Add(time);
					track.values.out.Add(pos);
				}
				dmanim->node_translation_tracks.Add(track);
			}
			if (uint32 num_scale_keys = ainode->mNumScalingKeys) {
				dm_animation::dm_node_scale_track track(dm_animation::dm_node_scale_track::dm_track_type::scale);
				track.target = dmnode;
				for (uint32 key_index = 0; key_index < num_scale_keys; ++key_index) {
					double time = ainode->mScalingKeys[key_index].mTime / tps;
					dm_vec3 scale = internal::dm_vec3_from_assimp(ainode->mScalingKeys[key_index].mValue);
					track.values.in.Add(time);
					track.values.out.Add(scale);
				}
				dmanim->node_scale_tracks.Add(track);
			}
			if (uint32 num_rot_keys = ainode->mNumRotationKeys) {
				dm_animation::dm_node_rotation_track track(dm_animation::dm_node_rotation_track::dm_track_type::rotation);
				track.target = dmnode;
				for (uint32 key_index = 0; key_index < num_rot_keys; ++key_index) {
					double time = ainode->mRotationKeys[key_index].mTime / tps;
					dm_quat rot = internal::dm_quat_from_assimp(ainode->mRotationKeys[key_index].mValue);
					track.values.in.Add(time);
					track.values.out.Add(rot);
				}
				dmanim->node_rotation_tracks.Add(track);
			}

			//[todo] handle ainode->mPostState
		}
	}
}

void process_bones(const aiMesh* Mesh, const aiNode* ParentNode, const aiScene* aiscene, dm_scene* dmscene, dm_node* meshnode) {
#if 1
	dm_skin* skin = nullptr;
	if (int num_of_bones = Mesh->mNumBones) {
		aiBone* B = Mesh->mBones[0];
		dm_string skin_name = "skin";
		if (B->mArmature != nullptr) {
			skin_name = B->mArmature->mName.C_Str();
		}
		//dm_string skin_name = B->mArmature->mName.C_Str();
		skin_name.Append("_skin");
		skin = dm_scene_find_skin(dmscene, skin_name);

		if (skin == nullptr) {
			skin = new dm_skin();
			skin->name = skin_name;
			dmscene->skins.Add(skin);

		}
	}
	meshnode->skin = skin;

	//int start_bone_id = skin->inverse_bind_matrices.Num();
	//skin->inverse_bind_matrices.SetNum(start_bone_id + Mesh->mNumBones);
	for (uint32 bone_index = 0; bone_index < Mesh->mNumBones; ++bone_index) {
		aiBone* Bone = Mesh->mBones[bone_index];
		aiNode* BoneNode = Bone->mNode;
		if (BoneNode == nullptr) {
			BoneNode = aiscene->mRootNode->FindNode(Bone->mName);
		}

		//dm_log_msg("armature Bone: " + dm_string_from_std(Bone->mName.C_Str()));
		//if (Bone->mArmature == nullptr) {
		//	dm_log_msg("no armature");
		//}
		//else {
		//	if (Bone->mArmature->mName.C_Str() == nullptr)
		//		dm_log_msg("no mArmature name");
		//	else
		//		dm_log_msg("armature bone->armature: " + dm_string_from_std(Bone->mArmature->mName.C_Str()));
		//}
		//
		//dm_log_msg("armature bone->node: " + dm_string_from_std(Bone->mNode->mName.C_Str()));

		dm_string bone_name = Bone->mName.C_Str();
		dm_string bone_parent_name("");
		if ((BoneNode != nullptr) && (BoneNode->mParent != nullptr)) {
			bone_parent_name = BoneNode->mParent->mName.C_Str();
		}
		dm_mat4 bone_model_matrix_2 = dm_mat4::Identity;
		if (Bone)
			bone_model_matrix_2 = internal::dm_mat4_from_assimp(Bone->mOffsetMatrix); // Bone->mOffsetMatrix can't be trusted, seems to give relativ transform, not local
		dm_mat4 bone_model_matrix = internal::dm_mat4_from_assimp(BoneNode->mTransformation);
		dm_transform bone_transform = dm_transform(bone_model_matrix);

		//node* bone_node = create_node(bone_name, bone_transform, bone_parent_name);
		dm_node* bone_node = dm_scene_find_node(dmscene, bone_name);
		if (bone_node == nullptr) {
			bone_node = dm_node_create(bone_name, bone_transform, bone_parent_name);
			dm_scene_add_node(dmscene, bone_node);
		}

		dm_joint* joint = dm_scene_find_joint(dmscene, bone_name);//new joint();
		if (joint == nullptr) {
			joint = new dm_joint();
			dmscene->joints.Add(joint);
		}
		joint->name = bone_name;
		//joint->inverse_bind_matrix = bone_model_matrix;
		//joint->ibt = dm_node_relative_to_local_transform(bone_node);
		joint->ibt = dm_transform(bone_model_matrix_2);
		joint->ibtm = joint->ibt;//transform(joint->inverse_bind_matrix);
		joint->inverse_bind_matrix = bone_model_matrix_2;//joint->ibt.ToMatrixWithScale();

		//joint->inverse_bind_matrix = joint->inverse_bind_matrix.Inverse();
		bone_node->joint = joint;	// [note] could be &joint
					

		// actually transform those datapoints from per joint to mesh sections
		dm_primitive* mesh_primitive = &meshnode->instance->mesh->primitives.Last();
		int section_id = -1;
		if (joint->_primitive_sections.Find(mesh_primitive, section_id) == false) {
			joint->_primitive_sections.Add(mesh_primitive);
			section_id = joint->_primitive_sections.Num() - 1;
		}
		if (joint->_vertex_ids_in_section.Num() < (section_id + 1)) {
			joint->_vertex_ids_in_section.SetNum(section_id + 1);
		}
		if (joint->_weights_in_section.Num() < (section_id + 1)) {
			joint->_weights_in_section.SetNum(section_id + 1);
		}
					
		//auto vertex_ids = &joint->_vertex_ids_in_section[section_id];
		//auto weights = &joint->_weights_in_section[section_id];
		dm_array<uint32>& vertex_ids = joint->_vertex_ids_in_section[section_id];
		dm_array<float>& weights = joint->_weights_in_section[section_id];
		vertex_ids.SetNumZeroed(Bone->mNumWeights);
		weights.SetNumZeroed(Bone->mNumWeights);
		//vertex_ids->SetNum(Bone->mNumWeights);
		//weights->SetNum(Bone->mNumWeights);

		for (int vertex_index = 0; vertex_index < vertex_ids.Num(); ++vertex_index) {
			vertex_ids[vertex_index] = Bone->mWeights[vertex_index].mVertexId;
			weights[vertex_index] = Bone->mWeights[vertex_index].mWeight;
			//vertex_ids[i] = Bone->mWeights[i].mVertexId;
			//(*vertex_ids)[i] = Bone->mWeights[i].mVertexId;
			//(*weights)[i] = Bone->mWeights[i].mWeight;
		}
		//skin->inverse_bind_matrices.EmplaceAt(bone_index, joint->inverse_bind_matrix);
		//skin->inverse_bind_matrices[start_bone_id + bone_index] = joint->inverse_bind_matrix;
		int my_bone_index = skin->joints.AddUnique(bone_node);
		if (skin->inverse_bind_matrices.Num() <= my_bone_index)
			skin->inverse_bind_matrices.SetNum(my_bone_index + 1);
		skin->inverse_bind_matrices[my_bone_index] = joint->inverse_bind_matrix;
		//skin->inverse_bind_matrices.
		//skin->inverse_bind_matrices.EmplaceAt(my_bone_index, joint->inverse_bind_matrix);
		//skin->inverse_bind_matrices.Add(joint->inverse_bind_matrix);
		//skin->joints.AddUnique(bone_node);
	}
	skin->inverse_bind_matrices.Shrink();
#endif
#if 0
	dm_string skin_name = meshnode->name;
	skin_name.Append("_skin");
	dm_skin* skin = dm_scene_find_skin(dmscene, skin_name);

	if (skin != nullptr)
		return;

	skin = new dm_skin();
	skin->name = skin_name;

	meshnode->skin = skin;
	skin->inverse_bind_matrices.SetNum(Mesh->mNumBones);
	dmscene->skins.Add(skin);

	for (uint32 bone_index = 0; bone_index < Mesh->mNumBones; ++bone_index) {
		aiBone* Bone = Mesh->mBones[bone_index];
		aiNode* BoneNode = aiscene->mRootNode->FindNode(Bone->mName);

		//dm_log_msg("armature Bone: " + dm_string_from_std(Bone->mName.C_Str()));
		//if (Bone->mArmature == nullptr) {
		//	dm_log_msg("no armature");
		//}
		//else {
		//	if (Bone->mArmature->mName.C_Str() == nullptr)
		//		dm_log_msg("no mArmature name");
		//	else
		//		dm_log_msg("armature bone->armature: " + dm_string_from_std(Bone->mArmature->mName.C_Str()));
		//}
		//
		//dm_log_msg("armature bone->node: " + dm_string_from_std(Bone->mNode->mName.C_Str()));

		dm_string bone_name = Bone->mName.C_Str();
		dm_string bone_parent_name("");
		if ((BoneNode != nullptr) && (BoneNode->mParent != nullptr)) {
			bone_parent_name = BoneNode->mParent->mName.C_Str();
		}
		dm_mat4 bone_model_matrix_2 = dm_mat4::Identity;
		if (Bone)
			bone_model_matrix_2 = internal::dm_mat4_from_assimp(Bone->mOffsetMatrix); // Bone->mOffsetMatrix can't be trusted, seems to give relativ transform, not local
		dm_mat4 bone_model_matrix = internal::dm_mat4_from_assimp(BoneNode->mTransformation);
		dm_transform bone_transform = dm_transform(bone_model_matrix);

		//node* bone_node = create_node(bone_name, bone_transform, bone_parent_name);
		dm_node* bone_node = dm_scene_find_node(dmscene, bone_name);
		if (bone_node == nullptr) {
			bone_node = dm_node_create(bone_name, bone_transform, bone_parent_name);
			dm_scene_add_node(dmscene, bone_node);
		}

		dm_joint* joint = dm_scene_find_joint(dmscene, bone_name);//new joint();
		if (joint == nullptr) {
			joint = new dm_joint();
			dmscene->joints.Add(joint);
		}
		joint->name = bone_name;
		//joint->inverse_bind_matrix = bone_model_matrix;
		//joint->ibt = dm_node_relative_to_local_transform(bone_node);
		joint->ibt = dm_transform(bone_model_matrix_2);
		joint->ibtm = joint->ibt;//transform(joint->inverse_bind_matrix);
		joint->inverse_bind_matrix = bone_model_matrix_2;//joint->ibt.ToMatrixWithScale();

		//joint->inverse_bind_matrix = joint->inverse_bind_matrix.Inverse();
		bone_node->joint = joint;	// [note] could be &joint


		// actually transform those datapoints from per joint to mesh sections
		dm_primitive* mesh_primitive = &meshnode->instance->mesh->primitives.Last();
		int section_id = -1;
		if (joint->_primitive_sections.Find(mesh_primitive, section_id) == false) {
			joint->_primitive_sections.Add(mesh_primitive);
			section_id = joint->_primitive_sections.Num() - 1;
		}
		if (joint->_vertex_ids_in_section.Num() < (section_id + 1)) {
			joint->_vertex_ids_in_section.SetNum(section_id + 1);
		}
		if (joint->_weights_in_section.Num() < (section_id + 1)) {
			joint->_weights_in_section.SetNum(section_id + 1);
		}

		//auto vertex_ids = &joint->_vertex_ids_in_section[section_id];
		//auto weights = &joint->_weights_in_section[section_id];
		dm_array<uint32>& vertex_ids = joint->_vertex_ids_in_section[section_id];
		dm_array<float>& weights = joint->_weights_in_section[section_id];
		vertex_ids.SetNumZeroed(Bone->mNumWeights);
		weights.SetNumZeroed(Bone->mNumWeights);
		//vertex_ids->SetNum(Bone->mNumWeights);
		//weights->SetNum(Bone->mNumWeights);

		for (int vertex_index = 0; vertex_index < vertex_ids.Num(); ++vertex_index) {
			vertex_ids[vertex_index] = Bone->mWeights[vertex_index].mVertexId;
			weights[vertex_index] = Bone->mWeights[vertex_index].mWeight;
			//vertex_ids[i] = Bone->mWeights[i].mVertexId;
			//(*vertex_ids)[i] = Bone->mWeights[i].mVertexId;
			//(*weights)[i] = Bone->mWeights[i].mWeight;
		}

		skin->inverse_bind_matrices[bone_index] = joint->inverse_bind_matrix;
		skin->joints.Add(bone_node);
		//skin->joints.AddUnique(bone_node);
	}
#endif
}

void process_mesh(const aiMesh* Mesh, const aiNode* ParentNode, const aiScene* aiscene, dm_scene* dmscene, dm_node* dmparent) {
	// setup mesh
	dm_string mesh_name = internal::dm_string_from_assimp(Mesh->mName);
	mesh_name.Append("_mesh");
	dm_mesh* mesh = dm_scene_find_mesh(dmscene, mesh_name);
	if (mesh == nullptr) {
		mesh = new dm_mesh();
		mesh->name = mesh_name;

		// fill mesh primitives
		mesh->primitives.AddZeroed();
		dm_primitive& mesh_section = mesh->primitives.Last();
		for (uint32 i = 0; i < Mesh->mNumVertices; i++) {

			mesh_section.vertices.Add(internal::dm_vec3_from_assimp(Mesh->mVertices[i]) * 100.0f);
			mesh_section.normals.Add(internal::dm_vec3_from_assimp(Mesh->mNormals[i]));

			if (Mesh->HasTextureCoords(0)) {
				mesh_section.uvs.Add(dm_vec2(Mesh->mTextureCoords[0][i].x, Mesh->mTextureCoords[0][i].y));
			}
			else {
				mesh_section.uvs.Add(dm_vec2(0.0f, 0.0f));
			}

			if (Mesh->HasVertexColors(0) == true) {
				mesh_section.vertex_colors.Add(internal::dm_color_from_assimp(Mesh->mColors[0][i]));
			}
			else {
				//mesh_section.vertex_colors.AddZeroed(mesh_section.vertices.Num());
				mesh_section.vertex_colors.SetNumZeroed(mesh_section.vertices.Num());
			}
		}

		// define mesh topology
		bool recalculateIndices = true;
		if (recalculateIndices == true) {
			// process indices (not neccessary, if no changes to topology took place)
			for (uint32 i = 0; i < Mesh->mNumFaces; ++i) {
				aiFace face = Mesh->mFaces[i];
				mesh_section.indices.Append({ (int32)face.mIndices[0], (int32)face.mIndices[1], (int32)face.mIndices[2] });
			}
		}
		dmscene->meshes.Add(mesh);

		process_material(Mesh, ParentNode, aiscene, dmscene, &mesh_section);
	}
	
	// setup mesh instance
	dm_string instance_name = internal::dm_string_from_assimp(Mesh->mName);
	instance_name.Append("_instance");
	dm_instance* mesh_instance = dm_scene_find_instance(dmscene, instance_name);
	if (mesh_instance == nullptr) {
		mesh_instance = new dm_instance();
		mesh_instance->name = instance_name;
		mesh_instance->mesh = mesh;
		mesh_instance->type = dm_instance::dm_instance_type::it_custom_mesh;
		dmscene->instances.Add(mesh_instance);
	}

	// setup instance node
	dm_string node_name = internal::dm_string_from_assimp(Mesh->mName);
	node_name.Append("_instancenode");
	dm_node* mesh_node = dm_scene_find_node(dmscene, node_name);
	if (mesh_node == nullptr) {
		mesh_node = dm_node_create(node_name, dm_transform::Identity, dmparent);
		mesh_node->instance = mesh_instance;
		dm_scene_add_node(dmscene, mesh_node);
	}

	// setup skin
	if (Mesh->HasBones() == true) {
		mesh_instance->type = dm_instance::dm_instance_type::it_skinned_mesh;
		process_bones(Mesh, ParentNode, aiscene, dmscene, mesh_node);
	}
}

void process_node(const aiNode* Node, const aiScene* aiscene, dm_scene* dmscene, bool nodePrepass) {
	dm_checkf(Node, dm_text("Node == nullptr"));
	dm_check(aiscene);
	dm_check(dmscene);
	dm_node* n = dm_scene_find_node(dmscene, internal::dm_string_from_assimp(Node->mName));
	if (n == nullptr) {
		n = internal::dm_node_create_from_assimp(Node);
		dm_scene_add_node(dmscene, n);
	}
	if (!nodePrepass) {
		for (uint32 i = 0; i < Node->mNumMeshes; i++) {
			aiMesh* mesh = aiscene->mMeshes[Node->mMeshes[i]];
			process_mesh(mesh, Node, aiscene, dmscene, n);
		}
	}
	// process children
	for (uint32 i = 0; i < Node->mNumChildren; i++) {
		process_node(Node->mChildren[i], aiscene, dmscene, nodePrepass);
	}
}

void fill_nodes_from_assimp(dm_scene* dmscene, const aiScene* aiscene) {
	dm_checkf(dmscene, dm_text("dm scene invalid."));
	dm_checkf(aiscene, dm_text("ai scene invalid."));
	process_node(aiscene->mRootNode, aiscene, dmscene, true);
	dm_log_msg("1");
	dm_scene_fix_node_hierarchy(dmscene, true, true);
	dm_log_msg("2");
	process_node(aiscene->mRootNode, aiscene, dmscene, false);
	dm_log_msg("3");
	dm_scene_fix_node_hierarchy(dmscene, true, true);
	dm_log_msg("4");
	dmscene->nodes = dm_node_array_sort_depth_first(dmscene->nodes);

	dm_log_msg("fixup");
	//dm_scene_fixup_skins(dmscene, { true, true, true, true, false }); //dm_scene_merge_skins(dmscene);
	dm_scene_recalculate_joint_bounds(dmscene);

	dm_log_msg("print");
	bool print_skin_hierarchies = false;
	if (print_skin_hierarchies) {
		for (auto skn : dmscene->skins) {
			skn->joints = dm_node_array_sort_depth_first(skn->joints);
			dm_array<dm_node*> empty_bones{};
			for (auto n : skn->joints) {
				if (n->joint == nullptr) {
					empty_bones.Add(n);
				}
			}
			//log.write(*dm_node_array_to_string_verbose(empty_bones));
		}
	}
	dm_log_msg("process animation");
	process_animations(aiscene, dmscene);		
	//recalculate_inverse_bind_matrices(dmscene, true);	// after fix_node_hierachy is done, can recalculate all rel-to-model transforms
}

void
internal::dm_scene_from_assimp(const aiScene* assimp_, dm_scene* scene_)
{
	dm_check(assimp_);

	fill_nodes_from_assimp(scene_, assimp_);
	dm_log_msg("fill nodes done");
	for (auto n : scene_->nodes) {
		if (n->joint == nullptr)
			continue;
		dm_log_msg("apply weights");
		dm_scene_primitive_apply_weights_from_nodes(scene_, n);	// is n actual valid pointer in this case? or need ref onto pointer?
		dm_log_msg("apply done");
	}
	dm_log_msg("scene_from_assmip done");
	dm_scene_save(scene_, "D:\\savedscene_assimp.json");
}

void 
internal::dm_scene_to_assimp(const dm_scene* scene_, aiScene* assimp_)
{
	;
}
