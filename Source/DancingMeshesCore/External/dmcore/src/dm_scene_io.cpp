// Copyright branedev, Inc. All Rights Reserved.
#include "dm_scene_io.h"
#include "dm_json.h"
#include <fstream>

#include "dm_logging.h"
#include "dm_assimp.h"
#include "dm_gltf.h"


void to_json(json& js, const dm_camera& cam)
{
	static const auto def = dm_camera();
	internal::write_field("name", js, cam.name, def.name);
	internal::write_field("ortho", js, cam.ortho, def.ortho);
	internal::write_field("imsize", js, cam.imsize, def.imsize);
	internal::write_field("focal", js, cam.focal, def.focal);
	internal::write_field("focus", js, cam.focus, def.focus);
	internal::write_field("aperture", js, cam.aperture, def.aperture);

	internal::write_field("near", js, cam.near_plane, def.near_plane);
	internal::write_field("far", js, cam.far_plane, def.far_plane);
}
void from_json(const json& js, dm_camera& cam)
{
	internal::read_field("name", js, cam.name);
	internal::read_field("imsize", js, cam.imsize);
	internal::read_field("focal", js, cam.focal);
	internal::read_field("focus", js, cam.focus);
	internal::read_field("aperture", js, cam.aperture);

	internal::read_field("near", js, cam.near_plane);
	internal::read_field("far", js, cam.far_plane);
}


void to_json(json& js, const dm_texture& tex)
{
	static const auto def = dm_texture();
	internal::write_field("name", js, tex.name, def.name);
	internal::write_field("path", js, tex.uri, def.uri);
	internal::write_field("clamp", js, tex.clamp, def.clamp);
	internal::write_field("scale", js, tex.scale, def.scale);
	internal::write_field("gamma", js, tex.gamma, def.gamma);
	internal::write_field("has_opacity", js, tex.has_opacity, def.has_opacity);
}
void from_json(const json& js, dm_texture& tex)
{
	static const auto def = dm_texture();
	internal::read_field("name", js, tex.name);
	internal::read_field("path", js, tex.uri);
	internal::read_field("clamp", js, tex.clamp);
	internal::read_field("scale", js, tex.scale);
	internal::read_field("gamma", js, tex.gamma);
	internal::read_field("has_opacity", js, tex.has_opacity);
}


void to_json(json& js, const dm_material& mat)
{
	static const auto def = dm_material();

	internal::write_field("name", js, mat.name, def.name);
	internal::write_field("double_sided", js, mat.double_sided, def.double_sided);

	internal::write_field("color_emissive", js, mat.color_emissive, def.color_emissive);
	internal::write_field("color_diffuse", js, mat.color_diffuse, def.color_diffuse);
	internal::write_field("color_specular", js, mat.color_specular, def.color_specular);
	internal::write_field("color_transmission", js, mat.color_transmission, def.color_transmission);

	internal::write_field("roughness", js, mat.roughness, def.roughness);
	internal::write_field("opacity", js, mat.opacity, def.opacity);
	internal::write_field("specular", js, mat.specular, def.specular);
	internal::write_field("emissive", js, mat.emissive, def.emissive);

	internal::write_pointer_ref("diffuse_map", js, mat.diffuse_map, def.diffuse_map);
	// ...

	internal::write_field("fresnel", js, mat.fresnel, def.fresnel);
	internal::write_field("refract", js, mat.refract, def.refract);
}
void from_json(const json& js, dm_material& mat)
{
	internal::read_field("name", js, mat.name);
	internal::read_field("double_sided", js, mat.double_sided);
	internal::read_field("color_emissive", js, mat.color_emissive);

	internal::read_field("color_diffuse", js, mat.color_diffuse);
	internal::read_field("color_specular", js, mat.color_specular);
	internal::read_field("color_transmission", js, mat.color_transmission);

	internal::read_field("roughness", js, mat.roughness);
	internal::read_field("opacity", js, mat.opacity);
	internal::read_field("specular", js, mat.specular);
	internal::read_field("emissive", js, mat.emissive);

	internal::read_pointer_ref("diffuse_map", js, mat.diffuse_map);

	internal::read_field("fresnel", js, mat.fresnel);
	internal::read_field("refract", js, mat.refract);
}


void to_json(json& js, const dm_primitive& primitive_)
{
	static const auto def = dm_primitive();
	internal::write_field("primitive_type", js, primitive_.primitive_type, def.primitive_type);
	internal::write_field("points", js, primitive_.vertices, def.vertices);
	internal::write_field("normals", js, primitive_.normals, def.normals);
	internal::write_field("indices", js, primitive_.indices, def.indices);

	internal::write_field("texcoord_0", js, primitive_.uvs, def.uvs);
	internal::write_field("colors_0", js, primitive_.vertex_colors, def.vertex_colors);

	internal::write_field("weights", js, primitive_.weights, def.weights);	// [note][ademets] might be write_array(), not sure why not?
	internal::write_pointer_ref_array("joints", js, primitive_.joints);		// [note][ademets] same as above - might be array, not field

	internal::write_pointer_ref("material", js, primitive_.material, def.material);
}
void from_json(const json& js, dm_primitive& primitive_)
{
	internal::read_field("primitive_type", js, primitive_.primitive_type);
	internal::read_field("points", js, primitive_.vertices);
	internal::read_field("normals", js, primitive_.normals);
	internal::read_field("indices", js, primitive_.indices);

	internal::read_field("texcoord_0", js, primitive_.uvs);
	internal::read_field("colors_0", js, primitive_.vertex_colors);

	internal::read_field("weights", js, primitive_.weights);
	internal::read_pointer_ref_array("joints", js, primitive_.joints);

	internal::read_pointer_ref("material", js, primitive_.material);
}


void to_json(json& js, const dm_mesh& mesh_)
{
	static const auto def = dm_mesh();
	internal::write_field("name", js, mesh_.name, def.name);
	internal::write_array("primitives", js, mesh_.primitives);
	//internal::write_array("weights", js, mesh_.weights);
}
void from_json(const json& js, dm_mesh& mesh_)
{
	internal::read_field("name", js, mesh_.name);
	internal::read_field("primitives", js, mesh_.primitives);
	//internal::read_field("weights", js, mesh_.weights);
}


void to_json(json& js, const dm_instance& inst)
{
	static const auto def = dm_instance();
	internal::write_field("type", js, inst.type, def.type);
	internal::write_field("name", js, inst.name, def.name);
	internal::write_field("transform", js, inst.transform, def.transform);

	// ..
	// .. data
	// 
	internal::write_pointer_ref("mesh", js, inst.mesh, def.mesh);

	internal::write_pointer_ref("material", js, inst.material, def.material);

	internal::write_array("sockets_snap_to", js, inst.sockets_snap_to);
	internal::write_array("sockets_snap_from", js, inst.sockets_snap_from);
}
void from_json(const json& js, dm_instance& inst)
{
	internal::read_field("type", js, inst.type);
	internal::read_field("name", js, inst.name);
	internal::read_field("transform", js, inst.transform);

	// ..
	// .. data
	// 
	internal::read_pointer_ref("mesh", js, inst.mesh);

	internal::read_pointer_ref("material", js, inst.material);

	internal::read_field("sockets_snap_to", js, inst.sockets_snap_to);
	internal::read_field("sockets_snap_from", js, inst.sockets_snap_from);
}


void to_json(json& js, const dm_joint& inst)
{
	static const auto def = dm_joint();
	internal::write_field("name", js, inst.name, def.name);
	internal::write_field("inverse_bind_matrix", js, inst.inverse_bind_matrix, def.inverse_bind_matrix);
	internal::write_field("ibt", js, inst.ibt, def.ibt);
	internal::write_field("ibtm", js, inst.ibtm, def.ibtm);
}
void from_json(const json& js, dm_joint& inst)
{
	internal::read_field("name", js, inst.name);
	internal::read_field("inverse_bind_matrix", js, inst.inverse_bind_matrix);
	internal::read_field("ibt", js, inst.ibt);
	internal::read_field("ibtm", js, inst.ibtm);
}

void to_json(json& js, const dm_skin& skin_)
{
	static const auto def = dm_skin();

	internal::write_field("name", js, skin_.name, def.name);
	internal::write_pointer_ref_array("joints", js, skin_.joints);
	//[todo][ademets] need to write serializer for dm_array<transform> & dm_array<mat4>
	//internal::write_field("joint_offsets", js, skin_.joint_offsets, def.joint_offsets);
	//internal::write_field("inverse_bind_matrices", js, skin_.inverseBindMatrices, def.inverseBindMatrices);
}

void from_json(const json& js, dm_skin& skin_)
{
	internal::read_field("name", js, skin_.name);
	internal::read_pointer_ref_array("joints", js, skin_.joints);
	//[todo][ademets] need to write deserializer for dm_array<transform> & dm_array<mat4>
	//internal::read_field("joint_offsets", js, skin_.joint_offsets);
	//internal::read_field("inverse_bind_matrices", js, skin_.inverseBindMatrices);
}


void to_json(json& js, const dm_node& node_)
{
	static const auto def = dm_node();

	internal::write_field("type", js, node_.type, def.type);
	internal::write_field("name", js, node_.name, def.name);
	internal::write_field("transform", js, node_.transform, def.transform);

	internal::write_pointer_ref("parent", js, node_.parent, def.parent);
	internal::write_pointer_ref("camera", js, node_.camera, def.camera);
	internal::write_pointer_ref("instance", js, node_.instance, def.instance);
	internal::write_pointer_ref("joint", js, node_.joint, def.joint);
	internal::write_pointer_ref("skin", js, node_.skin, def.skin);
}
void from_json(const json& js, dm_node& node_)
{
	internal::read_field("type", js, node_.type);
	internal::read_field("name", js, node_.name);
	internal::read_field("transform", js, node_.transform);

	internal::read_pointer_ref("parent", js, node_.parent);
	internal::read_pointer_ref("camera", js, node_.camera);
	internal::read_pointer_ref("instance", js, node_.instance);
	internal::read_pointer_ref("joint", js, node_.joint);
	internal::read_pointer_ref("skin", js, node_.skin);
}


void to_json(json& js, const dm_animation& anim)
{
	static const auto def = dm_animation();

	//internal::write_field("type", js, anim.type, def.type);

	//internal::write_field("name", js, anim.name, def.name);
	//internal::write_field("path", js, anim.path, def.path);
	//
	//internal::write_pointer_ref_array("targets", js, anim.targets);

	//internal::write_field("frames_per_second", js, anim.frames_per_second, def.frames_per_second);
	//internal::write_field("key_frames", js, anim.key_frames, def.key_frames);

	//internal::write_array("translation", js, anim.translation);
	//internal::write_array("rotation", js, anim.rotation);
	//internal::write_array("scale", js, anim.scale);

	//internal::write_array("weights", js, anim.weights);
}
void from_json(const json& js, dm_animation& anim)
{
	//internal::read_field("type", js, anim.type);

	//internal::read_field("name", js, anim.name);
	//internal::read_field("path", js, anim.path);

	//internal::read_pointer_ref_array("targets", js, anim.targets);

	//internal::read_field("frames_per_second", js, anim.frames_per_second);
	//internal::read_field("key_frames", js, anim.key_frames);

	//internal::read_field("translation", js, anim.translation);
	//internal::read_field("rotation", js, anim.rotation);
	//internal::read_field("scale", js, anim.scale);

	//internal::read_field("weights", js, anim.weights);
}


void to_json(json& js, const dm_scene& scn)
{
	static const auto def = dm_scene();

	internal::write_field("name", js, scn.name, def.name);

	//
	// resolve names (for references) - no duplicate or non named items allowed
	// ...

	internal::write_pointer_array("cameras", js, scn.cameras);
	internal::write_pointer_array("meshes", js, scn.meshes);
	internal::write_pointer_array("instances", js, scn.instances);
	internal::write_pointer_array("materials", js, scn.materials);
	internal::write_pointer_array("textures", js, scn.textures);
	internal::write_pointer_array("joints", js, scn.joints);
	internal::write_pointer_array("skins", js, scn.skins);

	internal::write_pointer_array("nodes", js, scn.nodes);
	internal::write_pointer_array("animations", js, scn.animations);
}
void to_json(json& js, const dm_scene* scn)
{
	if (!scn) {
		js = json();
		return;
	}
	to_json(js, *scn);
}

template <typename T>
static dm_map<dm_string, T*> make_named_map(const dm_array<T*>& elements)
{
	auto map = dm_map<dm_string, T*>();
#ifdef DM_RUNTIME_UNREAL
	for (auto element : elements) map.Add(element->name, element);
#else
	for (auto element : elements) map[element->name] = element;
#endif //DM_RUNTIME_UNREAL
	return map;
};


void from_json(const json& js, dm_scene& scn)
{
	internal::read_field("name", js, scn.name);

	internal::read_array("cameras", js, scn.cameras);
	internal::read_array("meshes", js, scn.meshes);
	internal::read_array("instances", js, scn.instances);
	internal::read_array("materials", js, scn.materials);
	internal::read_array("textures", js, scn.textures);
	internal::read_array("joints", js, scn.joints);
	internal::read_array("skins", js, scn.skins);

	internal::read_array("nodes", js, scn.nodes);
	internal::read_array("animations", js, scn.animations);

	//
	// resolve refs via names - delete temp objects, and set pointers to actual objects
	// ...
	//auto cam_map = make_named_map(scene.cameras);
	//auto tex_map = make_named_map(scene.textures);
	//auto mat_map = make_named_map(scene.materials);
	auto instance_map = make_named_map(scn.instances);
	auto joint_map = make_named_map(scn.joints);
	auto node_map = make_named_map(scn.nodes);

	auto fix_ref = [](auto& map, auto& elements, auto& ref) {
		if (!ref) return;
		auto name = ref->name;
#ifdef DM_RUNTIME_UNREAL
		if (map.Contains(ref->name)) {
			ref = *map.Find(ref->name);
		}
		else {
			map.Add(ref->name, ref);
			elements.Add(ref);
		}
#else
		if (map.find(ref->name) != map.end()) {
			ref = map.at(name);
		}
		else {
			map[ref->name] = ref;
			elements.push_back(ref);
		}
#endif //DM_RUNTIME_UNREAL
	};

	for (auto node_ : scn.nodes) {
		fix_ref(node_map, scn.nodes, node_->parent);
		fix_ref(instance_map, scn.instances, node_->instance);
		fix_ref(joint_map, scn.joints, node_->joint);
		//fix_ref(cam_map, scene.cameras, node_->camera);
		//fix_ref(emap, scene->environments, nde->environment);
	}
}

dm_status dm_scene_load(dm_scene** scene, dm_string path)
{
	nlohmann::json js;
	{
		std::ifstream file(dm_string_to_cstr(path));
		if (!file.is_open())
		{
			//throw std::system_error(std::make_error_code(std::errc::no_such_file_or_directory));
			return dm_status(dm_status::dm_status_code::file_not_found, "invalid filepath.");
		}
		file >> js;
	}
	dm_scene_create(scene);
	**scene = js;

	return dm_status();
}

dm_status dm_scene_save(const dm_scene* scene, const dm_string& path)
{
	nlohmann::json js = *scene;
	std::ofstream file(dm_string_to_cstr(path), std::ofstream::out);
	if (!file.is_open())
	{
		//throw std::system_error(std::make_error_code(std::errc::io_error));
		return dm_status(dm_status::dm_status_code::io_error, "failed to open file for saving.");
	}
	file << js.dump(4);
	file.close();

	return dm_status();
}


bool get_file_extension(const dm_string& filepath, dm_string& file_ext) {
	dm_string throw_away;
	return filepath.Split(".", &throw_away, &file_ext, ESearchCase::IgnoreCase, ESearchDir::FromEnd);
}

dm_scene_type ext_to_type(const dm_string& ext) {
	if (ext.Equals("dm")) {
		return dm_scene_type::st_dms;
	}
	else if (ext.Equals("dms")) {
		return dm_scene_type::st_dms;
	}
	else if (ext.Equals("dmz")) {
		return dm_scene_type::st_dmz;
	}
	else if (ext.Equals("dmb")) {
		return dm_scene_type::st_dmb;
	}
	else if (ext.Equals("fbx")) {
		return dm_scene_type::st_fbx;
	}
	else if (ext.Equals("obj")) {
		return dm_scene_type::st_obj;
	}
	else if (ext.Equals("gltf")) {
		return dm_scene_type::st_gltf;
	}
	else if (ext.Equals("glb")) {
		return dm_scene_type::st_glb;
	}
	return dm_scene_type::st_unknown;
}

dm_string type_to_ext(const dm_scene_type type) {
	switch (type) {
	case dm_scene_type::st_dms: return dm_string("dms");
	case dm_scene_type::st_dmz: return dm_string("dmz");
	case dm_scene_type::st_dmb: return dm_string("dmb");
	case dm_scene_type::st_fbx: return dm_string("fbx");
	case dm_scene_type::st_obj: return dm_string("obj");
	case dm_scene_type::st_gltf: return dm_string("gltf");
	case dm_scene_type::st_glb: return dm_string("glb");
	default: return dm_string("");
	}
	return dm_string("");
}

#define USE_ASSIMP_FOR_GLTF 1
dm_status 
dm_scene_import(dm_scene*& scene, dm_scene_import_params params)
{
	dm_string ext;
	get_file_extension(params.filepath, ext);
	dm_scene_type type = ext_to_type(ext);

	dm_log_msg("started loading ..." + params.filepath);

	switch (type) {
	case dm_scene_type::st_dmb: dm_scene_load(&scene, params.filepath); break;
	case dm_scene_type::st_dms: dm_scene_load(&scene, params.filepath); break;
	case dm_scene_type::st_dmz: dm_scene_load(&scene, params.filepath); break;
	case dm_scene_type::st_fbx: dm_assimp_load(&scene, params.filepath); break;
	case dm_scene_type::st_obj: dm_assimp_load(&scene, params.filepath); break;
#if USE_ASSIMP_FOR_GLTF
	case dm_scene_type::st_gltf: dm_assimp_load(&scene, params.filepath); break;
	case dm_scene_type::st_glb: dm_assimp_load(&scene, params.filepath); break;
#else
	case dm_scene_type::st_gltf: dm_gltf_load(&scene, params.filepath); break;
	case dm_scene_type::st_glb: dm_gltf_load(&scene, params.filepath); break;
#endif
	default: dm_assimp_load(&scene, params.filepath); break;
	}

	//filepath.Split("/",  &scene_->_filepath, &scene_->_filename, ESearchCase::IgnoreCase, ESearchDir::FromEnd);
	//scene_->_filepath = filepath;

	return dm_status_okay();
}


dm_status 
dm_scene_export(const dm_scene* scene, dm_scene_export_params params)
{
	dm_string ext;
	bool has_extension = get_file_extension(params.filepath, ext);
	dm_scene_type save_as = params.save_as_type;
	if ((has_extension == true) && (params.save_as_type == dm_scene_type::st_from_file_extension)) {
		save_as = ext_to_type(ext);
	}
	switch (save_as) {
	case dm_scene_type::st_dms: dm_scene_save(scene, params.filepath); break;
	case dm_scene_type::st_dmb: dm_scene_save(scene, params.filepath); break;
	case dm_scene_type::st_dmz: dm_scene_save(scene, params.filepath); break;
	case dm_scene_type::st_fbx: dm_assimp_save(scene, params.filepath); break;
	case dm_scene_type::st_obj: dm_assimp_save(scene, params.filepath); break;
	case dm_scene_type::st_gltf:dm_assimp_save(scene, params.filepath); break;
	case dm_scene_type::st_glb: dm_assimp_save(scene, params.filepath); break;
	}

	return dm_status_okay();
}