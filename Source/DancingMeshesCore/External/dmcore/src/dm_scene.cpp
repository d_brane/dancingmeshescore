// Copyright branedev, Inc. All Rights Reserved.
#include "dm_scene.h"
#include "dm_container.h"
#include "dm_base64.h"

// clang-format off

dm_status dm_scene_create(dm_scene** scene) { *scene = new dm_scene();  return dm_status(); }
dm_status dm_scene_remove(dm_scene* scene) { if (scene) delete scene; scene = nullptr; return dm_status(); }

bool dm_scene_has_nodes(const dm_scene* scene) { return (dm_array_length(scene->nodes) > 0); }
bool dm_scene_has_meshes(const dm_scene* scene) { return (dm_array_length(scene->meshes) > 0); }
bool dm_scene_has_animations(const dm_scene* scene) { return (dm_array_length(scene->animations) > 0); }
bool dm_scene_has_instances(const dm_scene* scene) { return (dm_array_length(scene->instances) > 0); }
bool dm_scene_has_cameras(const dm_scene* scene) { return (dm_array_length(scene->cameras) > 0); }
bool dm_scene_has_materials(const dm_scene* scene) { return (dm_array_length(scene->materials) > 0); }
bool dm_scene_has_textures(const dm_scene* scene) { return (dm_array_length(scene->textures) > 0); }
bool dm_scene_has_skins(const dm_scene* scene) { return (dm_array_length(scene->skins) > 0); }
bool dm_scene_has_scenes(const dm_scene* scene) { return (dm_array_length(scene->scenes) > 0); }
bool dm_scene_has_cmds(const dm_scene* scene) { return (dm_array_length(scene->cmds) > 0); }

int dm_scene_num_nodes(const dm_scene* scene) { return dm_array_length(scene->nodes); }
int dm_scene_num_meshes(const dm_scene* scene) { return dm_array_length(scene->meshes); }
int dm_scene_num_animations(const dm_scene* scene) { return dm_array_length(scene->animations); }
int dm_scene_num_instances(const dm_scene* scene) { return dm_array_length(scene->instances); }
int dm_scene_num_cameras(const dm_scene* scene) { return dm_array_length(scene->cameras); }
int dm_scene_num_materials(const dm_scene* scene) { return dm_array_length(scene->materials); }
int dm_scene_num_textures(const dm_scene* scene) { return dm_array_length(scene->textures); }
int dm_scene_num_skins(const dm_scene* scene) { return dm_array_length(scene->skins); }
int dm_scene_num_scenes(const dm_scene* scene) { return dm_array_length(scene->scenes); }
int dm_scene_num_cmds(const dm_scene* scene) { return dm_array_length(scene->cmds); }

dm_node* dm_scene_find_node(const dm_scene* scene, const dm_string& name) { for (dm_node* n : scene->nodes) { if (name == n->name) { return n; } }return nullptr; }
dm_mesh* dm_scene_find_mesh(const dm_scene* scene, const dm_string& mesh_name) { for (dm_mesh* m : scene->meshes) { if (mesh_name == m->name) { return m; } }return nullptr; }
dm_skin* dm_scene_find_skin(const dm_scene* scene, const dm_string& skin_name) { for (dm_skin* s : scene->skins) { if (skin_name == s->name) { return s; } }return nullptr; }
dm_joint* dm_scene_find_joint(const dm_scene* scene, const dm_string& joint_name) { for (dm_joint* j : scene->joints) { if (joint_name == j->name) { return j; } }return nullptr; }
dm_material* dm_scene_find_material(const dm_scene* scene, const dm_string& material_name) { for (dm_material* m : scene->materials) { if (material_name == m->name) { return m; } }return nullptr; }
dm_instance* dm_scene_find_instance(const dm_scene* scene_, const dm_string& instance_name) { for (dm_instance* i : scene_->instances) { if (instance_name == i->name) { return i; } }return nullptr; }
dm_animation* dm_scene_find_animation(const dm_scene* scene_, const dm_string& animation_name) { for (dm_animation* i : scene_->animations) { if (animation_name == i->name) { return i; } }return nullptr; }

bool dm_texture_is_empty(dm_texture* texture) { return !(dm_array_length(texture->data) && dm_string_length(texture->uri)); }
bool dm_texture_is_embedded_resource(dm_texture* texture) { return ((dm_string_contains(texture->uri, detail::dm_MimetypeImagePNG) == true) || (dm_string_contains(texture->uri, detail::dm_MimetypeImageJPG) == true)); }
dm_status dm_texture_materialize_data(dm_texture* texture, dm_array<uint8_t>& data)
{
	char const* const mimetype = dm_string_contains(texture->uri, detail::dm_MimetypeImagePNG) == 0 ? detail::dm_MimetypeImagePNG : detail::dm_MimetypeImageJPG;
	const uint64_t start_pos = strlen(mimetype) + 1;								// const std::size_t startPos = std::char_traits<char>::length(mimetype) + 1;
	const uint64_t base64_length = dm_string_length(texture->uri) - start_pos;		// const std::size_t base64Length = uri.length() - startPos;
	const bool success = dm_base64_decode(dm_string_substring(texture->uri, start_pos), data);
	//if (!success) {
	//	return dm_status(dm_status::dm_status_code::invalid_input_file, "invalid buffer.uri value", "malformed base64");
	//}
	return dm_status();
}

dm_node::dm_node_type dm_node_get_type(const dm_node* node) {
	if (node->camera != nullptr) return dm_node::dm_node_type::nt_camera;
	//if ((instance != nullptr) && (skin != nullptr)) return node_type::nt_skinned_instance;	// probably not needed
	if (node->instance != nullptr) return dm_node::dm_node_type::nt_instance;
	if (node->joint != nullptr) return dm_node::dm_node_type::nt_joint;
	if (node->skin != nullptr) return dm_node::dm_node_type::nt_skin;
	if (node->environment != nullptr) return dm_node::dm_node_type::nt_environment;
	return dm_node::dm_node_type::nt_node;
};

// clang-format on
