#pragma once

#include "dm_scene.h"
#include "dm_status.h"

dm_status dm_scene_load(dm_scene** scene, const dm_string path);
dm_status dm_scene_save(const dm_scene* scene, const dm_string& path);

// DEPRECATED
enum class dm_scene_type { st_from_file_extension, st_unknown, st_dms, st_dmb, st_dmz, st_fbx, st_obj, st_gltf, st_glb };

struct dm_scene_import_params { dm_string filepath; bool load_textures{ true }; bool load_animations{ true }; };
dm_status dm_scene_import(dm_scene*& scene, dm_scene_import_params params);

struct dm_scene_export_params { dm_string filepath; bool save_textures{ false }; dm_scene_type save_as_type{ dm_scene_type::st_from_file_extension }; };
dm_status dm_scene_export(const dm_scene* scene, dm_scene_export_params params);

