// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_defines.h"

#ifdef DM_RUNTIME_UNREAL
#include "Runtime/Core/Public/Containers/Array.h"
#include "Runtime/Core/Public/Containers/Map.h"

// system
template<typename T>             using dm_array = TArray<T>;
template<typename T, typename C> using dm_map = TMap<T, C>;
//template<typename T>			 using dm_deque = TQueue<T>;

// array wrapper functions
template<typename T> inline int const dm_array_length(const dm_array<T>& arr) { return arr.Num(); }
template<typename T> inline int dm_array_add(dm_array<T>& arr, T const& item) { return arr.Add(item); }
template<typename T> inline int dm_array_add_unique(dm_array<T>& arr, T const& item) { return arr.AddUnique(item); }
template<typename T> inline int dm_array_remove(dm_array<T>& arr, const T& item) { return arr.Remove(item); }
template<typename T> inline T& dm_array_last(dm_array<T>& arr, uint32 index_from_last = 0) { return arr.Last(index_from_last); }
template<typename T> inline T& dm_array_get(dm_array<T>& arr, uint32 index) { return arr[index]; }
template<typename T> inline void  dm_array_clear(dm_array <T>& arr) { return arr.Empty(); }	// arr.clear()
template<typename T> inline void  dm_array_reserve(dm_array<T>& arr, uint32 length) { return arr.SetNum(length); }	// arr.reserve(length)
//template<typename T> inline int32 dm_array_remove_duplicates(dm_array<T>& arr) { need to implement }
#endif // DM_RUNTIME_UNREAL


#ifdef DM_RUNTIME_NATIVE
#include <vector>
#include <map>
#include <deque>

template<typename T>			 using dm_array = std::vector <typename T>;
template<typename T, typename C> using dm_map = std::unordered_map<T, C>;
#endif // DM_RUNTIME_NATIVE
