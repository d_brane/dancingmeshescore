// Copyright branedev, Inc. All Rights Reserved.
///////////////////////////////////////////////////////////////////////////////
// Dancing Meshes Core library
// 
// author: Alexander Demets, ademets
// year: 2019
//
// legend:
// - comments:
//     [authorname] ... add in author to each annotated comment
//     [note]       ... comments that note something
//     [bug]        ... known bug, that should be fixed
//     [fixme]      ... probably works, but hacky
//     [todo]       ... a todo that needs to happen
//     [!][!!][!!!] ... priority level
//     example: // [!!][note][ademets] this should be formated nicer
//
// this is a lazy include header, not recommended to use this, increased build time.
//
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "dm_platform.h"
#include "dm_scene.h"
#include "dm_core.h"
#include "dm_io.h"