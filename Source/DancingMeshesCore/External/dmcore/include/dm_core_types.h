// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_scene_types.h"

constexpr int DM_API_VERSION_LATEST = 41;

//------------------------------------------------------------------------//
// DM PLATFORM FUNCTIONS
//------------------------------------------------------------------------//
struct dm_bridge_interface;

struct dm_context {
	const static int DM_SCENE_VIEWS_MAX{ 5 };
	const static int DM_BRIDGE_INTERFACES_MAX{ 5 };

	dm_scene* model{ nullptr };
	int view_count{ 0 };
	dm_scene* views[DM_SCENE_VIEWS_MAX];
	int bridge_interface_count{ 0 };
	dm_bridge_interface* interfaces[DM_BRIDGE_INTERFACES_MAX];

	dm_array<dm_scene*> scenes;
	dm_scene_state scene_state;
};

//------------------------------------------------------------------------//
// DM INIT
//------------------------------------------------------------------------//
// this can be used to define API version and custom memory allocs.


struct dm_initialize_params {
	uint32 api_version{ DM_API_VERSION_LATEST };
	uint32 product_version{ 0 };
	void* reserved{ nullptr };
	void* alloc_memory_func{ nullptr };
	void* realloc_memory_func{ nullptr };
	void* release_memory_func{ nullptr };
};



struct dm_platform_params {
	bool is_server{ false };
};
struct dm_platform {
	dm_scene* scene{ nullptr };
	dm_array<dm_bridge_interface> bridges{};
	dm_platform_params platform_params;
	bool dispatch_to_memory{ true };
	bool dispatch_to_native{ false };
	bool dispatch_to_unreal{ false };
	bool dispatch_to_unity{ false };
};


typedef struct dm_mesh* dm_mesh_handle;
typedef struct dm_camera* dm_camera_handle;
typedef struct dm_instance* dm_instance_handle;
typedef struct dm_node* dm_node_handle;
typedef struct dm_material* dm_material_handle;
typedef struct dm_texture* dm_texture_handle;
typedef struct dm_skin* dm_skin_handle;
typedef struct dm_joint* dm_joint_handle;
typedef struct dm_animation* dm_animation_handle;
typedef struct dm_scene* dm_scene_handle;
typedef struct dm_command* dm_cmd_handle;