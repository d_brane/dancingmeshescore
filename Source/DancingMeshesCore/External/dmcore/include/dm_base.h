// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#if defined(__cplusplus)
#if defined(_MSC_VER) && _MSC_VER >= 1800
/* Visual Studio 2013 or later */
#include <cstdint>
#else
#include <stdint.h>
#include <stddef.h>
#endif

#if __cplusplus >= 201103L
#define DM_HAS_ENUM_CLASS
#elif defined(_MSC_VER) && defined(_MSVC_LANG) && _MSVC_LANG >= 201103L
#define DM_HAS_ENUM_CLASS
#endif
#else
/* C Compiler */
#include <stdint.h>
#include <stddef.h>
#endif

typedef int32_t dm_bool;
#define DM_TRUE 1
#define DM_FALSE 0

#if defined(DM_BUILDING_SDK) && DM_BUILDING_SDK > 0
#if defined(_WIN32) || defined(__CYGWIN__)
#ifdef __GNUC__
#define DM_API __attribute__ ((dllexport))
#else
#define DM_API __declspec(dllexport)
#endif
#else
#if __GNUC__ >= 4
#define DM_API __attribute__ ((visibility ("default")))
#else
#define DM_API
#endif
#endif

#else

#if defined(_WIN32) || defined(__CYGWIN__)
#if defined(DM_MONOLITHIC) && DM_MONOLITHIC > 0
#define DM_API
#elif defined(DM_BUILD_DLL) && DM_BUILD_DLL > 0
#ifdef __GNUC__
#define DM_API __attribute__ ((dllexport))
#else
#define DM_API __declspec(dllexport)
#endif
#else
#ifdef __GNUC__
#define DM_API __attribute__ ((dllimport))
#else
#define DM_API __declspec(dllimport)
#endif
#endif
#else
#if __GNUC__ >= 4
#define DM_API __attribute__ ((visibility ("default")))
#else
#define DM_API
#endif
#endif
#endif

#if defined(_MSC_VER) && (defined(__i386) || defined(_M_IX86))
#define DM_CALL __stdcall
#define DM_MEMORY_CALL __stdcall
#else
#define DM_CALL
#define DM_MEMORY_CALL
#endif

#ifdef __cplusplus
#define EXTERN_C extern "C"
#else
#define EXTERN_C
#endif

#define DM_DECLARE_CALLBACK(name, ...) EXTERN_C typedef void (DM_CALL * name)(__VA_ARGS__)
#define DM_PASTE(...) __VA_ARGS__
#define DM_STRUCT(struct_name, struct_def)           \
	EXTERN_C typedef struct _tag ## struct_name {     \
		DM_PASTE struct_def                          \
	} struct_name

#ifdef DM_HAS_ENUM_CLASS
#define DM_ENUM(name, ...) typedef enum class name : int32_t { __VA_ARGS__ } name
#else
#define DM_ENUM(name, ...) typedef enum name { __VA_ARGS__, __##name##_PAD_INT32__ = 0x7FFFFFFF } name
#endif

#define DM_DECLARE_FUNC(return_type) EXTERN_C DM_API return_type DM_CALL

#undef DM_HAS_ENUM_CLASS
