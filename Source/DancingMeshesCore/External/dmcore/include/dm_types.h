// Copyright branedev, Inc. All Rights Reserved.
#pragma once


using uint8 = unsigned char;
using u8 = unsigned char;
using s8 = signed char;
using uint16 = unsigned short;
using u16 = unsigned short;
using s16 = signed short;
//using uint32 = unsigned int;
using u32 = unsigned int;
using s32 = signed int;
//using uint64 = unsigned long;
using u64 = unsigned long;
using s64 = signed long;
using f32 = float;
using f64 = double;

using dm_uint8 = unsigned char;
using dm_int8 = signed char;
using dm_u8 = unsigned char;
using dm_s8 = signed char;

using dm_uint16 = unsigned short;
using dm_int16 = signed short;
using dm_u16 = unsigned short;
using dm_s16 = signed short;

using dm_uint32 = unsigned int;
using dm_int32 = int;
using dm_u32 = unsigned int;
using dm_s32 = signed int;

using dm_uint64 = unsigned long;
using dm_int64 = signed long;
using dm_u64 = unsigned long;
using dm_s64 = signed long;

using dm_float = float;
using dm_double = double;
using dm_f32 = float;
using dm_f64 = double;