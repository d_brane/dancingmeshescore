#pragma once

#include "dm_scene.h"

struct dm_obj {};

namespace internal {
	void dm_save_obj_impl(const dm_string& path, const dm_scene& scene_) {};
	dm_obj* dm_load_obj_impl(const dm_string& path) {};

}

void dm_obj_to_scene(const dm_obj* obj_, dm_scene* scene_) {};
void dm_scene_to_obj(const dm_scene* scene_, dm_obj* obj_) {};


