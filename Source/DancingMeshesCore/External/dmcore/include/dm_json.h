// Copyright branedev, Inc. All Rights Reserved.
#include "dm_base64.h"

#define JSON_DIAGNOSTICS 0
#include <nlohmann/json.hpp>

#define DM_JSON_WORKAROUND 1

//#if (defined(__cplusplus) && __cplusplus >= 201703L) || (defined(_HAS_CXX17) && _HAS_CXX17 == 1)
//#define FEATURE_LEVEL_CPP_17
//#include <string_view>
//#endif

using nlohmann::json;

#ifdef DM_RUNTIME_NATIVE



inline void to_json(json& js, const vec2& val)
{
	js = std::array<float, 2>{ { val.X, val.Y } };
}
inline void from_json(const json& js, vec2& val)
{
	auto vala = js.get<std::array<float, 2>>();
	val = { vala[0], vala[1] };
}


inline void to_json(json& js, const vec3& val)
{
	js = std::array<float, 3>{ { val.X, val.Y, val.Z } };
}
inline void from_json(const json& js, vec3& val)
{
	auto vala = js.get<std::array<float, 3>>();
	val = { vala[0], vala[1], vala[2] };
}


inline void to_json(json& js, const vec3i& val)
{
	js = std::array<int, 3>{ { val.X, val.Y, val.Z } };
}
inline void from_json(const json& js, vec3i& val)
{
	auto vala = js.get<std::array<int, 3>>();
	val = { vala[0], vala[1], vala[2] };
}


inline void to_json(json& js, const vec4& val)
{
	js = std::array<float, 4>{ { val.X, val.Y, val.Z, val.W } };
}
inline void from_json(const json& js, vec4& val)
{
	auto vala = js.get<std::array<float, 4>>();
	val = { vala[0], vala[1], vala[2], vala[3] };
}


inline void to_json(json& js, const quat& val)
{
	js = std::array<float, 4>{ { val.X, val.Y, val.Z, val.W } };
}
inline void from_json(const json& js, quat& val)
{
	auto vala = js.get<std::array<float, 4>>();
	val = { vala[0], vala[1], vala[2], vala[3] };
}


inline void to_json(json& js, const transform& val)
{
	js = json{ { "translation", val.translation },
				{ "rotation", val.rotation },
				{ "scale", val.scale } };
}
inline void from_json(const json& js, transform& val)
{
	val.translation = (js.at("translation").get<vec3>());
	val.rotation = (js.at("rotation").get<quat>());
	val.scale = (js.at("scale").get<vec3>());
}


inline void to_json(json& js, const dm_string& val)
{
	js = val;
}
inline void from_json(const json& js, dm_string& val)
{
	val = js.get<dm_string>();
}

}

#endif

#if DM_RUNTIME_UNREAL

namespace nlohmann {
	template <typename T>
	struct adl_serializer<TArray<T>> {
		static void to_json(json& js, const TArray<T>& arr) {
			//for (T el : arr) {...}
			//todo(alex): test if this is correct (TArray->std::vector)
			std::vector<T> arra;
			arra.assign(&arr[0], &arr[0] + arr.Num());
			js = arra;
		}
		static void from_json(const json& js, TArray<T>& arr) {
			//todo(alex): test if this is correct (std::vector->TArray)
			std::vector<T> arra = js.get <std::vector<T>>();
			arr.Append(&arra[0], arra.size());
		}
	};
}


inline void to_json(json& js, const FVector2D& val)
{
	js = std::array<float, 2>{ { val.X, val.Y } };
}
inline void from_json(const json& js, FVector2D& val)
{
	auto vala = js.get<std::array<float, 2>>();
	//val = { vala[0], vala[1] };
	val.X = vala[0];
	val.Y = vala[1];
}


inline void to_json(json& js, const FVector& val)
{
	js = std::array<float, 3>{ { val.X, val.Y, val.Z } };
}
inline void from_json(const json& js, FVector& val)
{
	auto vala = js.get<std::array<float, 3>>();
	val = { vala[0], vala[1], vala[2] };
	//val.X = vala[0];
	//val.Y = vala[1];
	//val.Z = vala[2];
}


inline void to_json(json& js, const FIntVector& val)
{
	js = std::array<int, 3>{ { val.X, val.Y, val.Z } };
}
inline void from_json(const json& js, FIntVector& val)
{
	auto vala = js.get<std::array<int, 3>>();
	val = { vala[0], vala[1], vala[2] };
	//val.X = vala[0];
	//val.Y = vala[1];
	//val.Z = vala[2];
}


inline void to_json(json& js, const FVector4& val)
{
	js = std::array<float, 4>{ { val.X, val.Y, val.Z, val.W } };
}
inline void from_json(const json& js, FVector4& val)
{
	auto vala = js.get<std::array<float, 4>>();
	val.Set(vala[0], vala[1], vala[2], vala[3]);
}

inline void to_json(json& js, const FQuat& val)
{
	js = std::array<float, 4>{ { val.X, val.Y, val.Z, val.W } };
}
inline void from_json(const json& js, FQuat& val)
{
	auto vala = js.get<std::array<float, 4>>();
	val = { vala[0], vala[1], vala[2], vala[3] };
	//val.X = vala[0];
	//val.Y = vala[1];
	//val.Z = vala[2];
	//val.W = vala[3];
}

inline void to_json(json& js, const FLinearColor& val)
{
	js = std::array<float, 4>{ { val.R, val.G, val.B, val.A } };
}
inline void from_json(const json& js, FLinearColor& val)
{
	auto vala = js.get<std::array<float, 4>>();
	val = { vala[0], vala[1], vala[2], vala[3] };
}

inline void to_json(json& js, const FMatrix& val)
{
	js = std::array<float, 16>{ { val.M[0][0], val.M[0][1], val.M[0][2], val.M[0][3],
		val.M[1][0], val.M[1][1], val.M[1][2], val.M[1][3],
		val.M[2][0], val.M[2][1], val.M[2][2], val.M[2][3],
		val.M[3][0], val.M[3][1], val.M[2][2], val.M[2][3] } };
}
inline void from_json(const json& js, FMatrix& val)
{
	auto vala = js.get<std::array<float, 16>>();
	val = FMatrix(FPlane(vala[0], vala[1], vala[2], vala[3]),
		FPlane(vala[4], vala[5], vala[6], vala[7]),
		FPlane(vala[8], vala[9], vala[10], vala[11]),
		FPlane(vala[12], vala[13], vala[14], vala[15]));
}

inline void to_json(json& js, const FTransform& val)
{
	FVector trans = val.GetTranslation();
	FQuat rot = val.GetRotation();
	FVector scale = val.GetScale3D();
#if DM_JSON_WORKAROUND
	js = json{
		   { "translation", std::array<float, 3>{ { trans.X, trans.Y, trans.Z } } },
		   { "rotation", std::array<float, 4>{ { rot.X, rot.Y, rot.Z, rot.W } }   },
		   { "scale", std::array<float, 3>{ { scale.X, scale.Y, scale.Z } }       }
	};
#else
	js = json{
			   { "translation", val.GetTranslation() },
			   { "rotation", val.GetRotation()       },
			   { "scale", val.GetScale3D()           }
	};
#endif
}
inline void from_json(const json& js, FTransform& val)
{
#if DM_JSON_WORKAROUND
	std::array<float, 3> arr_3;
	std::array<float, 4> arr_4;
	js.at("translation").get_to(arr_3);
	val.SetTranslation(FVector(arr_3[0], arr_3[1], arr_3[2]));
	js.at("rotation").get_to(arr_4);
	val.SetRotation(FQuat(arr_4[0], arr_4[1], arr_4[2], arr_4[3]));
	js.at("scale").get_to(arr_3);
	val.SetScale3D(FVector(arr_3[0], arr_3[1], arr_3[2]));
#else
	val.SetTranslation(js["translation"].get<FVector>());
	val.SetRotation(js.at("rotation").get<FQuat>());
	val.SetScale3D(js.at("scale").get<FVector>());
#endif
}


inline void to_json(json& js, const FString& val)
{
	js = std::string(TCHAR_TO_UTF8(*val));
}
inline void from_json(const json& js, FString& val)
{
	val = FString(UTF8_TO_TCHAR(js.get<std::string>().c_str()));
}

#endif // DM_RUNTIME_UNREAL


namespace internal {

	//#if defined(FEATURE_LEVEL_CPP_17)
	//		template <typename TTarget>
	//		inline void read_optional_field(std::string_view key, json const & json, TTarget & target)
	//#else
	//		template <typename TKey, typename TTarget>
	//		inline void read_optional_field(TKey && key, json const & js, TTarget & target)
	//#endif
	//		{
	//			const json::const_iterator iter = js.find(key);
	//			if (iter == js.end())
	//			{
	//				target = iter->get<TTarget>();
	//			}
	//		}
	//

#if defined(FEATURE_LEVEL_CPP_17) && !defined(DM_RUNTIME_UNREAL)
	template <typename TTarget>
	inline void read_field(std::string_view key, json const& js, TTarget& target)
#else
	template <typename TKey, typename TTarget>
	inline void read_field(TKey&& key, json const& js, TTarget& target)
#endif
	{
#if DM_PLATFORM_LINUX
//#if DM_JSON_WORKAROUND
			target = js.at(key).template get<TTarget>();
#else
		const json::const_iterator iter = js.find(key);
		if (iter != js.end()) { 
			;// target = iter->get<TTarget>();
		}
#endif
	}



#if defined(FEATURE_LEVEL_CPP_17) && !defined(DM_RUNTIME_UNREAL)
	template <typename TTarget>
	inline void read_pointer_ref(std::string_view key, json const& js, TTarget*& target)
#else
	template <typename TKey, typename TTarget>
	inline void read_pointer_ref(TKey&& key, json const& js, TTarget*& target)
#endif
	{
		if (target == nullptr)
			target = new TTarget();
#if DM_PLATFORM_ANDROID || DM_PLATFORM_LINUX
		target->name = js.at(key).template get<dm_string>();
#else
		const json::const_iterator iter = js.find(key);
		if (iter != js.end()) {
			if (target == nullptr)
				target = new TTarget();
			target->name = iter->get<dm_string>();
		}
#endif
	}

#if defined(FEATURE_LEVEL_CPP_17) && !defined(DM_RUNTIME_UNREAL)
	template <typename TTarget>
	inline void read_pointer_ref_array(std::string_view key, json const& js, dm_array<TTarget*> & value)
#else
	template <typename TKey, typename TTarget>
	inline void read_pointer_ref_array(TKey && key, json const& js, dm_array<TTarget*> & value)
#endif
	{
		std::vector<dm_string> ref_list = js.at(key);
		for (auto ref_name : ref_list) {
			TTarget* item = new TTarget();
			item->name = ref_name;
#if DM_RUNTIME_UNREAL
			dm_array_add(value, new TTarget());
			//value.Add(item);
#else
			value.push_back(item);
#endif // DM_RUNTIME_UNREAL

		}
	}

#if defined(FEATURE_LEVEL_CPP_17) && !defined(DM_RUNTIME_UNREAL)
	template <typename TTarget>
	inline void read_array(std::string_view key, json const& js, dm_array<TTarget*> & value)
#else
	template <typename TKey, typename TTarget>
	inline void read_array(TKey && key, json const& js, dm_array<TTarget*> & value)
#endif
	{
		for (auto& j : js.value(key, json::array())) {
#ifdef DM_RUNTIME_UNREAL
			dm_array_add(value, new TTarget());
			from_json(j, *dm_array_last(value));
			//value.Add(new TTarget());
			//from_json(j, *value.Last());
#else
			value.push_back(new TTarget());
			from_json(j, *value.back());
#endif // DM_RUNTIME_UNREAL
		}
	}


	template <typename TValue>
	inline void write_field(std::string const& key, json & js, TValue const& value)
	{
		//todo(alex): actually test if values are empty
		//if (!value.empty())
		{
			js[key] = value;
		}
	}

	template <typename TValue>
	inline void write_field(std::string const& key, json & js, TValue const& value, TValue const& defaultValue)
	{
		if (value != defaultValue)
		{
			;// js[key] = value;
		}
	}

	template <typename TValue>
	inline void write_pointer_ref(std::string const& key, json & js, TValue const* value, TValue const* defaultValue)
	{
		if (value != defaultValue)
		{
			js[key] = value->name;
		}
	}

	template <typename TValue>
	inline void write_array(std::string const& key, json & js, dm_array<TValue> const& value)
	{
#ifdef DM_RUNTIME_UNREAL
		//if (value.Num())
		if (dm_array_length(value) > 0)
#else
		if (!value.empty())
#endif // DM_RUNTIME_UNREAL
		{
			js[key] = json::array();
			for (auto& v : value) js[key].push_back(json(v));
		}
	}

	template <typename TValue>
	inline void write_pointer_array(std::string const& key, json & js, dm_array<TValue*> const& value)
	{
#ifdef DM_RUNTIME_UNREAL
		//if (value.Num())
		if (dm_array_length(value) > 0)
#else
		if (!value.empty())
#endif // DM_RUNTIME_UNREAL
		{
			js[key] = json::array();
			for (auto v : value) js[key].push_back(json(*v));
		}
	}

	template <typename TValue>
	inline void write_pointer_ref_array(std::string const& key, json & js, dm_array<TValue*> const& value)
	{
#ifdef DM_RUNTIME_UNREAL
		//if (value.Num())
		if (dm_array_length(value) > 0)
#else
		if (!value.empty())
#endif // DM_RUNTIME_UNREAL
		{
			js[key] = json::array();
			for (auto v : value) js[key].push_back(v->name);
		}
	}

} // namespace internal


