// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_defines.h"

#ifdef DM_RUNTIME_UNREAL
#include "Runtime/Core/Public/Containers/UnrealString.h"
#include "Runtime/Core/Public/Containers/StringConv.h"

#include <string>

using dm_string = FString;

inline int32       dm_string_length(const dm_string& str) { return str.Len(); }
inline bool        dm_string_contains(const dm_string& str, const dm_string& sub_str) { return str.Contains(sub_str); }
inline dm_string   dm_string_substring(const dm_string& str, int32 start_at) { return str.RightChop(start_at); }
inline void        dm_string_reserve(dm_string& str, uint32 length) { str = FString(str, length - str.Len()); }
inline std::string dm_string_to_cstr(const dm_string& str) { return std::string(StringCast<ANSICHAR>(*str).Get()); }
//inline const char* dm_string_to_cstr(const dm_string& str) { return std::string(TCHAR_TO_UTF8(*str)).c_str(); }	//[note] this is insane, but TCHAR_TO_ANSI breaks in debug.
inline std::string dm_string_to_std(const dm_string& str) { return std::string(TCHAR_TO_UTF8(*str)); }
inline dm_string   dm_string_from_std(const std::string& str) { return dm_string(str.c_str()); }
inline void        dm_string_append_char(dm_string& str, char c) { str.AppendChar(c); }
inline bool        dm_string_is_empty(const dm_string& str) { return str.IsEmpty(); }
inline dm_string   dm_string_from_bool(const bool b) { return (b ? dm_string("true") : dm_string("false")); }
inline dm_string   dm_string_from_bool_numeric(const bool b) { return (b ? dm_string("1") : dm_string("0")); }

#define dm_text TEXT
#endif //DM_RUNTIME_UNREAL

#ifdef DM_RUNTIME_STD
#include <iostream>

using dm_string = std::string;
#endif //DM_RUNTIME_STD