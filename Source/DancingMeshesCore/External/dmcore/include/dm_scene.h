// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_scene_types.h"
#include "dm_status.h"

dm_status dm_scene_create(dm_scene** scene);
dm_status dm_scene_remove(dm_scene* scene);

bool dm_scene_has_nodes(const dm_scene* scene);
bool dm_scene_has_meshes(const dm_scene* scene);
bool dm_scene_has_animations(const dm_scene* scene);
bool dm_scene_has_instances(const dm_scene* scene);
bool dm_scene_has_cameras(const dm_scene* scene);
bool dm_scene_has_materials(const dm_scene* scene);
bool dm_scene_has_textures(const dm_scene* scene);
bool dm_scene_has_skins(const dm_scene* scene);
bool dm_scene_has_scenes(const dm_scene* scene);
bool dm_scene_has_cmds(const dm_scene* scene);

int dm_scene_num_nodes(const dm_scene* scene);
int dm_scene_num_meshes(const dm_scene* scene);
int dm_scene_num_animations(const dm_scene* scene);
int dm_scene_num_instances(const dm_scene* scene);
int dm_scene_num_cameras(const dm_scene* scene);
int dm_scene_num_materials(const dm_scene* scene);
int dm_scene_num_textures(const dm_scene* scene);
int dm_scene_num_skins(const dm_scene* scene);
int dm_scene_num_scenes(const dm_scene* scene);
int dm_scene_num_cmds(const dm_scene* scene);

dm_node*		dm_scene_find_node(const dm_scene* scene, const dm_string& node_name);	// :)
dm_mesh*		dm_scene_find_mesh(const dm_scene* scene, const dm_string& mesh_name);	// :)
dm_skin*		dm_scene_find_skin(const dm_scene* scene, const dm_string& skin_name);	// :)
dm_joint*		dm_scene_find_joint(const dm_scene* scene, const dm_string& joint_name);	// :)
dm_material*	dm_scene_find_material(const dm_scene* scene, const dm_string& material_name);	// :)
dm_instance*	dm_scene_find_instance(const dm_scene* scene, const dm_string& instance_name);	// :)
dm_animation*	dm_scene_find_animation(const dm_scene* scene, const dm_string& animation_name); // :)

//dm_array<dm_node_handle> dm_scene_get_nodes(const dm_scene* scene);
//dm_array<dm_mesh_handle> dm_scene_get_meshes(const dm_scene* scene);
//dm_array<dm_skin_handle> dm_scene_get_skins(const dm_scene* scene);
//dm_array<dm_material_handle> dm_scene_get_materials(const dm_scene* scene);
//dm_array<dm_instance_handle> dm_scene_get_instances(const dm_scene* scene);
//dm_array<dm_animation_handle> dm_scene_get_animations(const dm_scene* scene);
//dm_array<dm_texture_handle> dm_scene_get_textures(const dm_scene* scene);
//dm_array<dm_joint_handle> dm_scene_get_joints(const dm_scene* scene);
//dm_array<dm_camera_handle> dm_scene_get_cameras(const dm_scene* scene);

bool	  dm_texture_is_empty(dm_texture* texture);
bool	  dm_texture_is_embedded_resource(dm_texture* texture);
dm_status dm_texture_materialize_data(dm_texture* texture, dm_array<uint8_t>& data);

inline dm_vec3 dm_bounds_size(const dm_bounds* bounds) { return { bounds->param.X, bounds->param.Y, bounds->param.Z }; }

dm_node::dm_node_type dm_node_get_type(const dm_node* node);


//BD_AddNode()
//bd_add_node()
//bd_scene_add_node()
//dm_status make_scene(dm_platform* platform) {
//	dm_scene_handle scene = nullptr;
//	if (!dm_scene_create(scene).is_ok()) {
//		println("fuck.");
//		std::abort();
//	}
//	//dm_node_handle my_node = dm_scene_add_node(scene, { .name = "my_node", .position = {0.0f, 0.0f, 10.0f}, .rotation = {0.5f, 0.0f, 0.0f} });
//	dm_node_handle node = dm_node_register(scene, { .name = "my_node", .position = {0.0f, 0.0f, 10.0f}, .rotation = {0.5f, 0.0f, 0.0f} });
//	dm_instance_handle my_instance = dm_node_add_instance(my_node, { .name = "my_node_instance", .transform{dm_transform_IDENTITY} });
//	dm_mesh_handle my_mesh = dm_scene_register_mesh(scene, {});
//	
//	dm_scene_remove(scene);
//}

//dm_status dm_scene_add_node(dm_scene* scene, dm_node** node);
//dm_status dm_scene_add_texture(dm_scene* scene, dm_texture** texture);
//dm_status dm_scene_add_instance(dm_scene* scene, dm_instance** instance);
//dm_status dm_scene_add_animation(dm_scene* scene, dm_animation** animation);
//dm_status dm_scene_add_mesh(dm_scene* scene, dm_mesh** mesh);


//void dm_animation_change_fps(dm_animation* anim, int fps);
//int dm_animation_time_to_frame(dm_animation* anim, float time) { return (int)(round(time * anim->frames_per_second)); };
//float dm_animation_frame_to_time(dm_animation* anim, int frame) { return (float)(anim->frames_per_second / frame); };
//void dm_animation_add_keyframe(dm_animation* anim, float time) { anim->frames.push_back( dm_animation_time_to_frame(anim, time ) );  };



//// DMZ <==> GLTF conversation (full and delta update functions)	//
// dmz -> gltf
//
//   result dmz_to_gltf_full( dm_scene* dmz, fx::document* gltf )
//   result dmz_to_gltf_delta( dm_scene* dmz, fx::document* gltf, int dmz_version, int gltf_version )
//
//
// gltf -> dmz
//
//   result gltf_to_dmz_full( fx::document* gltf, dm_scene* dmz )
//   result gltf_to_dmz_hierachy ()
//   result gltf_to_dmz_shapes ()
//   result gltf_to_dmz_animation ()
//   result gltf_to_dmz_
//
//// DMZ <==> JSON dm_scene format (full and delta)
//// DMZ <==> DM_RUNTIME_UNREAL conversation (full and delta)
//// DMZ <==> Firebase backend conv (full and delta)


//// DMZ Scene manipulation API
//
// joint
//
//   joint_add( dm_scene &scene, node &joint );
//   joint_copy( node &master, node &clone );
//   joint_duplicate ( node &master, node &clone, int repeat_level = 0 ); 
//   joint_mirror( node &master, std::vector<node *> mirrored_nodes, transform mirror_axis, bool traverse_hierachy = true, bool copy_parent = true );
//   joint_delete( dm_scene &scene, node &joint_to_delete, bool traverse_hierachy = true );
//
//   joint_transform( node &joint, transform t);
//	 joint_rotate()
//	 joint_scale()

//	 joint_reparent()
//   joint_add_constraint( node &master, vec3 constraints );
//	 joint_update()
//	 joint_serialize()
//
//
// animation
//
//   anim_add_keyframe( animation &anim, transform  )
//   anim_insert_keyframe ()
//   anim_delete_keyframe ()
//   spline_from_keyframes(animation &anim, int current_frame )
// 
//
// shapes (geometry)
//
//   mesh_edit_vertex_position ( shape &mesh, int index, transform newTransform )
//
//
// splines, voxel, sdf, texture, material, ...
//
// ik, simulation, scripts, ...
//
// scripts: each node can have list of attached ecs style scripts (start, update, end, ...)


// clang-format on