// Copyright branedev, Inc. All Rights Reserved.
#pragma once

// desc: interface for dm<->xxx conversion, can be implemented for any system that wants to interface with dm_scene format
// edit: 2019/08/21 (ad: created file)

#include "dm_scene.h"


// emulate namespace in C to implement interface for data exchange (see https://stackoverflow.com/a/28535585)
struct dm_bridge_interface 
{
	const dm_string name;
	const enum dm_bridge_direction { direction_to_dm, direction_from_dm, direction_bidirectional } direction;
	//const enum dm_bridge_mode { update, merge, replace, create } mode;
	//uint64 timestamp_last_synced; 
	//uint64 scene_version_compability;

	bool (*from_dm)(dm_scene* from_scene, void* to_scene/*, dm_bridge_mode mode, void* converter_options*/);
	bool (*to_dm)(void* from_scene, dm_scene* to_scene/*, dm_bridge_mode mode, void* converter_options*/);

	//struct bridge_crud{
	//enum crud_level : enum (full, partial)
	// crud_capabilities: enum [](dm_scene, nodes, instances, meshes, primitives,...)
	//} *crud_implementation;

	// use crud_implementation to update as minimal as possible, otherwise just use full from_dm / to_dm
	bool (*update_from_dm)(dm_scene* from_scene, void* to_scene/*, void* converter_options*/);
	bool (*update_to_dm)(void* from_scene, dm_scene* to_scene/*, void* converter_options*/);

	//bool (*setup_bridge)(void* from_scene, void* to_scene, void* converter_options);
	//bool (*sync)(void* sync_options);
	//bool (*update)(float delta, void* update_options);
};
extern const struct dm_bridge_interface bridge_generic;
extern const struct dm_bridge_interface bridge_copy;

