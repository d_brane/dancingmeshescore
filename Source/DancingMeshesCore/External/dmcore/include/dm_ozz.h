// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_scene.h"

#include "dm_core_types.h"

//#ifdef DM_PLATFORM_ANDROID
//#define OZZ_BUILD_SIMD_REF
//#define _M_IX86_FP 0
//#endif

#include "ozz/base/maths/simd_math.h"
#include "ozz/base/maths/soa_transform.h"
#include "ozz/animation/runtime/sampling_job.h"

namespace ozz {
	namespace animation {
		class Skeleton;
		class Animation;
		class SamplingCache;
	}
	//namespace math {
		//struct SoaTransform;
		//struct Float4x4;
	//}
}

typedef ozz::animation::Skeleton ozz_skeleton;
typedef ozz::animation::Animation ozz_animation;
typedef ozz::animation::SamplingJob::Context ozz_sampling_cache;


struct dm_scene;
struct dm_ozz_data;

namespace internal
{
	dm_mat4 dm_mat4_from_ozz(const ozz::math::Float4x4& matrixIn);	// :/
	ozz::math::Float4x4 dm_mat4_to_ozz(const dm_mat4& matrixIn);	// :/
}

struct dm_ozz_init_params { bool gpu_skinning{ false }; int num_of_threads{ -1 }; };
dm_ozz_data* dm_ozz_init(dm_ozz_init_params params);
dm_status    dm_ozz_free(dm_ozz_data* ozz_data);

void dm_ozz_from_scene(dm_ozz_data* ozz_data, dm_scene* scene);

inline dm_status dm_ozz_get_status(dm_ozz_data* ozz_data) { return dm_status(); };

struct dm_ozz_state {
	enum struct dm_ozz_mode { play, stop, pause, pose, uninitialized } mode{ dm_ozz_mode::uninitialized };
	float time_ratio{ 0.0f };
	float playback_speed{ 1.0f };
	int   frames_per_second{ 30 };
};
inline dm_status dm_ozz_set_state(dm_ozz_data* ozz_data, dm_ozz_state state) { return dm_status(); };
inline dm_ozz_state dm_ozz_get_state(dm_ozz_data* ozz_data) { return dm_ozz_state(); };
inline dm_status dm_ozz_set_animation(dm_ozz_data* ozz_data, dm_animation* animation) { return dm_status(); };
inline dm_status dm_ozz_set_animation_from_name(dm_ozz_data* ozz_data, dm_string animation_name) { return dm_status(); };

inline dm_status dm_ozz_set_callback_on_error(dm_ozz_data* ozz_data, void* func_on_error) { return dm_status(); };
inline dm_status dm_ozz_set_callback_on_free(dm_ozz_data* ozz_data, void* func_on_free) { return dm_status(); };
inline dm_status dm_ozz_set_callback_on_init_begin(dm_ozz_data* ozz_data, void* func_on_init_begin) { return dm_status(); };
inline dm_status dm_ozz_set_callback_on_init_finished(dm_ozz_data* ozz_data, void* func_on_init_finished) { return dm_status(); };
inline dm_status dm_ozz_set_callback_on_tick_begin(dm_ozz_data* ozz_data, void* func_on_tick_begin) { return dm_status(); };
inline dm_status dm_ozz_set_callback_on_tick_finished(dm_ozz_data* ozz_data, void* func_on_tick_finished) { return dm_status(); };
	
inline dm_status dm_ozz_set_pose_transforms(dm_ozz_data* ozz_data, dm_array<dm_node_handle> nodes, dm_array<dm_transform> &bone_transforms) { return dm_status(); };
inline dm_status dm_ozz_set_pose_rotations(dm_ozz_data* ozz_data, dm_array<dm_quat> bone_rotations) { return dm_status(); };
inline dm_status dm_ozz_set_pose_positions(dm_ozz_data* ozz_data, dm_array<dm_vec3> bone_relative_positions) { return dm_status(); };
inline dm_status dm_ozz_set_pose_scales(dm_ozz_data* ozz_data, dm_array<dm_vec3> bone_relative_scales) { return dm_status(); };
	
inline dm_status dm_ozz_set_local_transforms(dm_ozz_data* ozz_data, dm_array<dm_transform>& bone_transforms) { return dm_status(); };
inline dm_status dm_ozz_set_model_transforms(dm_ozz_data* ozz_data, dm_array<dm_transform>& bone_transforms) { return dm_status(); };
inline dm_status dm_ozz_set_world_transforms(dm_ozz_data* ozz_data, dm_array<dm_transform>& bone_transforms) { return dm_status(); };

struct dm_ozz_tick_params { bool tick_all{ true }; };
inline dm_status dm_ozz_tick(dm_ozz_data* ozz_data, float time_ratio, dm_ozz_tick_params params) { return dm_status(); };

struct dm_ozz_skinning_params { bool skin_all{ true }; };
inline dm_status dm_ozz_skinning(dm_ozz_data* ozz_data, dm_ozz_skinning_params params) { return dm_status(); };


//[depcrecated] remove from interface
void dm_scene_to_ozz(dm_scene* scene, dm_ozz_data* ozz_data);
//[depcrecated] remove from interface
bool on_update(float time_ratio, ozz::animation::Skeleton& ozz_skeleton, ozz::animation::Animation& ozz_animation,
				ozz::animation::SamplingCache& ozz_cache, dm_array<ozz::math::SoaTransform>& locals, dm_array<ozz::math::Float4x4>& models);
//[depcrecated] remove from interface
bool update_skinned_mesh(const ozz::animation::Skeleton* skeleton_, const dm_scene* scene_, 
							const dm_mesh& mesh_in, const dm_array<ozz::math::Float4x4> models_, 
							dm_array<dm_array<dm_vec3>>& vertices_out, dm_mesh& mesh_out);
