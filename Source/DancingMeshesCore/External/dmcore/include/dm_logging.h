// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_string.h"

#if DM_RUNTIME_UNREAL
#include "Runtime/Core/Public/CoreGlobals.h"
#include "Runtime/Core/Public/Logging/LogMacros.h"
#endif

inline void dm_log_msg(const dm_string& message)
{
#if DM_RUNTIME_UNREAL
	UE_LOG(LogTemp, Display, TEXT("[DM LOG]: %s"), *message);
#else
	printf("[DM LOG]: %s", *message.c_str());
#endif
}

struct dm_logger {
	enum struct dm_log_type { dm_log_type_info, dm_log_type_warning, dm_log_type_error };
	void (*write)(const dm_string& log_message, dm_log_type type);
};