// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_defines.h"

#if defined(IMGUI_API) && defined(DM_BUILD_WITH_IMGUI)
#define DM_WITH_IMGUI 1
#else
#define DM_WITH_IMGUI 0
#endif // IMGUI_API

#if DM_WITH_IMGUI
#include <imgui.h>
#endif // WITH_IMGUI