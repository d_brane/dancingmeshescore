#pragma once

#include "dm_platform.h"
#include "dm_scene.h"

//inline dm_status dm_voxel_init() {};
//inline dm_status dm_voxel_free() {};

struct dm_voxel {};
inline dm_mesh dm_mesh_from_voxel(dm_voxel voxel) {};
inline dm_voxel dm_mesh_to_voxel(dm_mesh mesh) {};

