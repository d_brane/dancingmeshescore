// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_defines.h"

//[todo] move over to dm_platform.h or rename to dm_clang.h

#define UE4_DISABLE_EXCEPTIONS 1

#if defined UE4_DISABLE_EXCEPTIONS
#define JSON_NOEXCEPTION
#define FX_NOEXCEPTION
#define YOCTO_NOEXCEPTION
#endif

#if defined DM_RUNTIME_UNREAL
#include "Runtime/Launch/Resources/Version.h" 
#if ENGINE_MINOR_VERSION < 25
#define DM_PLATFORM_ANDROID_OLD
#endif 
#endif

// fixes for android compile on ue4
#if DM_PLATFORM_ANDROID

#include <string>
#include <sstream>

#include <cstdlib> 
#include <cstdio>
#include <cstring>

//#pragma GCC diagnostic ignored "-Wdangling-else"
//#pragma GCC diagnostic ignored "-Wmissing-braces"
#pragma GCC diagnostic ignored "-Wshadow"

namespace ygl {
	typedef unsigned long long uint64_t;
}

#ifdef DM_PLATFORM_ANDROID_OLD


// undef and redefine _X for ozz::math (due to naming conflict with NDK
// namespace ozz::math { ...}
// redef size_t in proper namespace
// typedef unsigned long long size_t;

namespace std {

	constexpr auto snprintf = ::snprintf;
	constexpr auto strtoull = ::strtoull;
	constexpr auto strtof = ::strtof;
	constexpr auto strtoll = ::strtoll;
	constexpr auto strtold = ::strtold;
	//constexpr auto strstr = ::strstr;
	//constexpr auto atoi = ::atoi;
	//constexpr auto stoi = ::stoi;
	//constexpr auto strtod = ::strtod;

	template <typename T>
	inline std::string to_string(T value)
	{
		std::ostringstream os;
		os << value;
		return os.str();
	}

	//inline long double strtold(const char _nptr, char* _endptr)
	//{
	//	return strtod(_nptr, _endptr);
	//}

	inline int stoi(const string& str, size_t* idx = 0, int base = 10)
	{
		char* ptr;
		return strtol(str.c_str(), &ptr, base);
	}

}

#endif

#define MINIZ_USE_UNALIGNED_LOADS_AND_STORES 0
#define MINIZ_LITTLE_ENDIAN 0
#define MINIZ_X86_OR_X64_CPU 0

#endif
