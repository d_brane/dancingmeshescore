// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_defines.h"


#ifdef DM_RUNTIME_UNREAL
#include "Runtime/Core/Public/Math/Vector2D.h"
#include "Runtime/Core/Public/Math/Vector.h"
#include "Runtime/Core/Public/Math/IntVector.h"
#include "Runtime/Core/Public/Math/IntPoint.h"
#include "Runtime/Core/Public/Math/Transform.h"
#include "Runtime/Core/Public/Math/Matrix.h"
#include "Runtime/Core/Public/Math/Quat.h"
#include "Runtime/Core/Public/Math/Color.h"

#define dm_mat4_identity FMatrix::Identity
#define dm_transform_IDENTITY FTransform::Identity

using dm_vec2 = FVector2D;
using dm_vec3 = FVector;
using dm_vec3i = FIntVector;
using dm_vec2i = FIntPoint;
using dm_vec4 = FVector4;
using dm_mat4 = FMatrix;
using dm_quat = FQuat;
using dm_color = FLinearColor;
using dm_transform = FTransform;

// comparators (for json and container)
inline bool operator!=(const dm_transform& lhs, const dm_transform& rhs) { return !lhs.Equals(rhs); }
inline bool operator==(const dm_transform& lhs, const dm_transform& rhs) { return lhs.Equals(rhs); }
#endif // !DM_RUNTIME_UNREAL

#ifdef DM_RUNTIME_NATIVE
struct dm_vec2
{
	float x = 0;
	float y = 0;
};
struct dm_vec2i
{
	int x = 0;
	int y = 0;
};
struct dm_vec3
{
	float x = 0;
	float y = 0;
	float z = 0;
};
struct dm_vec3i
{
	int x = 0;
	int y = 0;
	int z = 0;
};
struct dm_vec4
{
	float x = 0;
	float y = 0;
	float z = 0;
	float w = 0;
};
struct dm_quat
{
	dm_vec4 i;
};

struct dm_transform
{
	dm_vec3 translation;
	dm_quat rotation;
	dm_vec3 scale;
};


// Small Fixed-size square matrices stored in column major format.
struct dm_mat4
{
	//mat4( ) : x { 1, 0, 0 }, y { 0, 1, 0 }, z { 0, 0, 1 }, w { 0, 0, 0 } { }
	dm_mat4(const dm_vec4& x_, const dm_vec4& y_, const dm_vec4& z_, const dm_vec4& w_) : x{ x_ }, y{ y_ }, z{ z_ }, w{ w_ } { }

	vec4 x = { 1, 0, 0, 0 };
	vec4 y = { 0, 1, 0, 0 };
	vec4 z = { 0, 0, 1, 0 };
	vec4 w = { 0, 0, 0, 1 };
};
const auto dm_idendity_mat4 = dm_mat4{ { 1, 0, 0, 0 },{ 0, 1, 0, 0 },{ 0, 0, 1, 0 },{ 0, 0, 0, 1 } };
#endif // !DM_RUNTIME_NATIVE