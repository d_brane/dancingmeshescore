// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_string.h"
#include "dm_status.h"

// [todo]
// + id -> name in case of no name
// joint name generator in case of no name
// what happens with joint / skeleton / skin generation if mesh is not split into primitives with 4 joints?


//#include "dm_scene.h"

//#include "cgltf/cgltf.h"

//[note][ademets] cool link with example serialization of triangle by fx author: https://github.com/jessey-git/fx-gltf/issues/53#issuecomment-461316283

struct dm_gltf;
struct dm_scene;

dm_status dm_gltf_load(dm_scene** scene, const dm_string path);
dm_status dm_gltf_save(const dm_scene* scene, const dm_string path);

//namespace internal {
	//void dm_scene_to_gltf_file(const dm_scene& scene_, const dm_string& path) {};
	//dm_gltf* dm_gltf_from_gltf_file(const dm_string& path) {};
//}

//void dm_scene_from_gltf(const dm_gltf* gltf_, dm_scene* scene_) {};
//void dm_scene_to_gltf(const dm_scene* scene_, dm_gltf* gltf_) {};


//inline dm_status dm_gltf_from_file(dm_gltf* gltf_scene, const dm_string file) { return dm_status(); };
//inline dm_status dm_gltf_to_file(const dm_gltf* gltf_scene, const dm_string file) { return dm_status(); };

//inline dm_status dm_gltf_from_scene(dm_gltf* gltf_scene, const dm_scene* scene) { return dm_status(); };
//inline dm_status dm_gltf_to_scene(const dm_gltf* gltf_scene, dm_scene* scene) { return dm_status(); };
