// Copyright branedev, Inc. All Rights Reserved.
#pragma once

// list of all available defines
// create custom configs, and uncomment to define
//#define DM_RUNTIME_UNREAL 0
//#define DM_RUNTIME_STD_HEADLESS 0
//#define DM_RUNTIME_WEBASM 0
//#define DM_RUNTIME_UNITY 0
//#define DM_RUNTIME_NATIVE 0
//#define DM_RUNTIME_TWO 0
//#define DM_RUNTIME_DECENTRALAND 0
//
//#define DM_PLATFORM_ANDROID 0
//#define DM_PLATFORM_WINDOWS 0
//#define DM_PLATFORM_MACOSX 0
//#define DM_PLATFORM_LINUX 0
//#define DM_PLATFORM_WEB 0
//#define DM_PLATFORM_HOLOLENS 0
//#define DM_PLATFORM_IOS 0
//#define DM_PLATFORM_ 0
//
//#define DM_COMPILER_MSVC 0
//#define DM_COMPILER_CLANG 0
//#define DM_COMPILER_GCC 0
//
//#define DM_BUILD_WITH_DMAPP 0
//#define DM_BUILD_WITH_ASSIMP 0
//#define DM_BUILD_WITH_FBXSDK 0
//#define DM_BUILD_WITH_CGLTF 0
//#define DM_BUILD_WITH_FXGLTF 0
//#define DM_BUILD_WITH_TINYOBJ 0
//#define DM_BUILD_WITH_OZZ 0
//#define DM_BUILD_WITH_OPEN3D 0
//#define DM_BUILD_WITH_TESTS 0
//#define DM_BUILD_WITH_EXCEPTIONS 0
//


#define DM_CONFIG_UNREAL_FULL

#ifdef DM_CONFIG_UNREAL_FULL

#define DM_RUNTIME_UNREAL 1

#define DM_BUILD_WITH_ASSIMP
#define DM_BUILD_WITH_FBXSDK
#define DM_BUILD_WITH_CGLTF
#define DM_BUILD_WITH_FXGLTF
#define DM_BUILD_WITH_TINYOBJ
#define DM_BUILD_WITH_OZZ
#define DM_BUILD_WITH_OPEN3D
#define DM_BUILD_WITH_TESTS
#define DM_BUILD_WITH_IMGUI

#define DM_PLATFORM_WINDOWS PLATFORM_WINDOWS
#ifdef PLATFORM_ANDROID
#define DM_PLATFORM_ANDROID PLATFORM_ANDROID
#else
#define DM_PLATFORM_ANDROID 0
#endif

#define DM_PLATFORM_MACOSX PLATFORM_OSX
#define DM_PLATFORM_LINUX PLATFORM_UNIX
#define DM_PLATFORM_IOS PLATFORM_IOS

#define DM_COMPILER_MSVC 0
#define DM_COMPILER_CLANG PLATFORM_COMPILER_CLANG
#define DM_BUILD_WITH_SIMD 0

//#if !DM_BUILD_WITH_SIMD
//#define OZZ_BUILD_SIMD_REF
//#endif

//#if PLATFORM_WINDOWS == 1
//#define DM_PLATFORM_WINDOWS
//#define DM_COMPILER_MSVC
//#endif
//#if PLATFORM_ANDROID == 1
//#define DM_PLATFORM_ANDROID
//#define DM_COMPILER_CLANG
//#endif
//#if PLATFORM_MAC == 1
//#define DM_PLATFORM_MACOSX
//#define DM_PLATFORM_CLANG
//#endif
//#if PLATFORM_UNIX == 1
//#define DM_PLATFORM_LINUX
//#endif

#endif // DM_CONFIG_UNREAL_FULL


#ifdef DM_CONFIG_WEBASM
//...
#endif // DM_CONFIG_WEBASM