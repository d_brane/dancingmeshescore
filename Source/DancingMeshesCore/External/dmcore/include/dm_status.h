// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_string.h"


struct dm_status {
	enum dm_status_code {
		okay,
		fatal,
		file_not_found,
		file_import_failed,
		invalid_nullpointer_arg,
		not_implemented,
		already_initialized,
		io_error,
		unkown
	} state{ okay };
	dm_string error_string{};

	dm_status() {};
	dm_status(dm_status_code status_code) : state(status_code) {};
	dm_status(dm_status_code status_code, dm_string error_string) : state(status_code), error_string(error_string) {};
	//dm_status(void* payload) 

	bool is_ok() { return (state == dm_status_code::okay) ? true : false; }

	//explicit operator bool() const { return dm_string_is_empty(error); }
	//bool is_fatal() { return !is_ok(); }
	//#ifdef DM_DEBUG_ERROR_INFO
	//dm_string filename;
	//dm_string func_name;
	//int line;
	//#endif
};


inline dm_status dm_status_okay() {
	return { dm_status::okay };
}

inline dm_status dm_status_fatal() {
	return dm_status(dm_status::fatal);
}

inline dm_status dm_status_fatal(dm_string error_string) {
	return { dm_status::fatal, error_string };
}