// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_math_types.h"

//struct dm_vec2;
//struct dm_vec3;
//struct dm_vec3i;
//struct dm_vec4;
//struct dm_quat;
//struct dm_mat4;
//struct dm_transform;

// comparators (for json and container)
inline bool operator!=(const dm_transform& lhs, const dm_transform& rhs);
inline bool operator==(const dm_transform& lhs, const dm_transform& rhs);