// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_scene.h"

struct dm_mesh_create_cube_params {
	dm_vec3 size{ 1.0f, 1.0f, 1.0f }; dm_quat rotation; dm_vec3 pivot_offset{ 0.0f, 0.0f, 0.0f }; int subdiv{ 0 }; float rounded{ 0.0f; }
};
inline dm_mesh dm_mesh_create_cube(dm_mesh_create_cube_params params) { return dm_mesh(); };

struct dm_mesh_create_sphere_params {
	dm_vec3 size{ 1.0f, 1.0f, 1.0f }; dm_quat rotation; dm_vec3 pivot_offset{ 0.0f, 0.0f, 0.0f }; int subdiv{ 0 }; 
};
inline dm_mesh dm_mesh_create_sphere(dm_mesh_create_sphere_params params) { return dm_mesh(); };

struct dm_mesh_create_capsule_params {
	dm_vec3 size{ 1.0f, 1.0f, 1.0f }; dm_quat rotation; dm_vec3 pivot_offset{ 0.0f, 0.0f, 0.0f }; int subdiv{ 0 };
};
inline dm_mesh dm_mesh_create_capsule(dm_mesh_create_capsule_params params) { return dm_mesh(); };
