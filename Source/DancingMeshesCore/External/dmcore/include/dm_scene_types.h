// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_string.h"
#include "dm_container.h"
#include "dm_math_types.h"

// clang-format off

// use handles instead of pointers:
//using dm_texture_handle = dm_texture*;
//
//struct dm_texture_handle_s {
//	enum struct dm_texture_handle_mode : uint32 { pointer = 'PNTR', packed = 'PCKD', hash64 = 'HSSF', string = 'STRG' } mode;	// make global state, so array's of handles pack nicely, also don't use '' hack to be able to use 8 bit enum
//	struct packed_id {
//		int32 id;
//		int32 hash_id;
//	};
//	union {
//		dm_texture* pointer;
//		packed_id packed;
//		uint64 hash64;
//		char name[8];
//	};
//};

namespace detail {
	constexpr char const* const dm_MimetypeApplicationOctet = "data:application/octet-stream;base64";
	constexpr char const* const dm_MimetypeImagePNG			= "data:image/png;base64";
	constexpr char const* const dm_MimetypeImageJPG			= "data:image/jpeg;base64";
}

struct dm_camera
{
	dm_string			name{};						// name
	bool				ortho{ false };				// orthographic
	dm_vec2				imsize{ 0.036f, 0.024f };	// film width and height (default: 35mm)
	float				focal{ 0.050f };			// focal length (defaut: 50 mm)
	float				focus{ FLT_MAX };			// focal distance (default: infinite), std::numeric_limits<float>::infinity();
	float				aperture{ 0.0f };			// lens aperture
	float				near_plane{ 0.01f };		// near plane distance
	float				far_plane{ 10000.0f };		// far plane distance
};

struct dm_texture
{
	dm_string			name{};						// name
	dm_string			uri{};						// file path / uri
	dm_array<dm_vec4>	data{};						//image4f or dm_array<unsigned char> image	= { };	// image, maybe under computed
	bool				clamp{ false };				// clamp textures coordinates
	float				scale{ 1 };					// scale for occ, normal, bumps
	float				gamma{ 2.2f };				// gamma correction for ldr textures in IO
	bool				has_opacity{ false };		// check whether alpha != 0
};

struct dm_material //: dm_never_empty
{
	dm_string			name{};
	bool				double_sided{ false };

	dm_vec3				color_emissive{ 0, 0, 0 };
	dm_vec3				color_diffuse{ 0, 0, 0 };
	dm_vec3				color_specular{ 0, 0, 0 };
	dm_vec3				color_transmission{ 0, 0, 0 };

	float				roughness{ 0.0001f };
	float				opacity{ 1.0f };
	float				metalness{ 0.001f };
	float				specular{ 0.0f };
	float				emissive{ 0.0f };

	dm_texture*			diffuse_map{ nullptr };			// use std::optional<> ??
	dm_texture*			normal_map{ nullptr };
	dm_texture*			material_map{ nullptr };

	bool				fresnel{ true };
	bool				refract{ false };
};

struct dm_node;
struct dm_primitive //: dm_never_empty
{
	enum struct dm_primitive_type
	{
		points			= 'PNTS',
		lines			= 'LINS',
		line_loop		= 'LILP',
		line_strip		= 'LSTP',
		triangles		= 'TNGL',
		triangle_strip	= 'TNST',
		triangle_fan	= 'TNFN',
		quat			= 'QUAT'
	};
	dm_primitive_type	primitive_type{ dm_primitive_type::triangles };

	dm_array<dm_vec3>	vertices{};
	dm_array<dm_vec3>	normals{};						//[todo][ademets] need to serialize / read
	dm_array<dm_vec3>	tangents{};						//[todo][ademets] need to serialize / read
	dm_array<int>		indices{};						//[todo][ademets] need to serialize / read, need to look into uint32 vs int32

	dm_array<dm_vec2>	uvs{};
	dm_array<dm_color>	vertex_colors{};
	dm_material*		material{ nullptr };
														// maybe make its own struct, or use primitive as base:
	//dm_array<dm_array<vec3>> morph_vertex_targets;
	//dm_array<dm_array<vec3>> morph_normal_targets;
	//dm_array<float> morph_weights;

	dm_array<dm_node*>	joint_ids{};					// per influence & per vertex array of joint ids (len = joints.num() * vertices.num())
	dm_array<dm_array<dm_node*>> joint_ids2d{};
	dm_array<float>		weights{};						//[todo][ademets] need to serialize / read // len = vertices.len() * joints.len() -- v1_j1(weight),v1_j2(weight), v1_jX(weight), v2_j1(weight), ...
	dm_array<dm_node*>	joints{};						//[todo][ademets] need to serialize / read, [note][ademets] not sure if we should use indices, pointers or strings (probably use string or id when serializing)

};

struct dm_mesh
{
	enum struct dm_primitive_split_mode
	{
		one_primitive_per_material,
		one_primitive_per_joint_group,
		one_primitive_per_material_and_joint_group
	};

	dm_string			name{};
	dm_string			uri{};							// source-file if loaded from external file (fbx,obj,...), [todo] need to serialize, should be in instance?
	dm_array<dm_primitive>	primitives{};
														// [note][ademets] either have weights and joint influences in primitive struct, or in mesh. not sure, what's cleaner.
	//dm_array<dm_array<float>> weights{};
	//dm_array<dm_array<node*>> joints{};
	//dm_mesh*				mirrored_mesh{ nullptr };
};

struct dm_subdiv { };

struct dm_spline
{
	dm_array<dm_vec3>	points{};
	dm_array<float>		point_sizes{};
	//dm_array<vec3>	point_normals	{};
	//dm_array<float>	point_opacity	{};
	dm_material*		material{ nullptr };
};

struct dm_sprite {
	dm_texture*			texture;
	dm_vec2				uv;
	dm_primitive		hull;
	dm_material*		material;
};
struct dm_sdf { };
struct dm_voxel { };

struct dm_bounds
{
	enum struct dm_pivot_type
	{
		pt_origin,	// at (0,0,0) of mesh
		pt_center,	// at (max_bounds - min_bounds) / 2
		pt_custom
	};
	enum struct dm_bounds_type
	{
		bt_box			= 'DFBX',
		bt_sphere		= 'DFSP',
		bt_box_rounded	= 'DFBR',
		bt_capsule		= 'DFCP',
		bt_torus		= 'DFTR',
	};

	dm_pivot_type		pivot_type{ dm_pivot_type::pt_origin };
	dm_bounds_type		bounds_type{ dm_bounds_type::bt_box };

	dm_vec4				param{ 0,0,0,0 };					// size at time = 0.0f, or current if in simulation; w parameter for extra info: sphere: subdiv, box_rounded: radius, capsule: offset, torus: thickness,...
	dm_vec4				color{ 0,0,0,0 };
	dm_vec3				custom_pos{ 0,0,0 };
	dm_quat				custom_rot{ dm_quat::Identity };
};


struct dm_scene;

struct dm_instance
{
	enum dm_instance_type {
		default_box		= 'DFBX',
		//default_sphere	= 'DFSP',
		//default_box_rounded = 'DFBR',
		//default_capsule	= 'DFCP',
		//default_torus		= 'DFTR',
		it_bounds_mesh	= 'ITBS',
		it_custom_mesh	= 'CSMS',
		it_subdiv_mesh	= 'SBMS',
															// [note][ademets] a mesh or any instance can be animated without knowing about it?
		it_skinned_mesh	= 'SKMS',
		//it_morphed_mesh	= 'MPMS',
		//it_animated_mesh	= 'ANMS',
		it_spline		= 'SPLN',
		it_sprite		= 'SPRT',
		it_sdf			= 'SDFF',
		it_voxel		= 'VOXL'
	};

	dm_string			name{};
	dm_string			uri{};	// not yet serialized, makes it, so instance can be a external file like cube.obj
	dm_transform		transform{ dm_transform_IDENTITY };

	dm_bounds			bounds{};
	//bounds			min_bounds;
	//bounds			max_bounds;

	dm_mesh*			mesh{ nullptr };
	dm_material*		material{ nullptr };					// overwrites any material
	dm_scene*			scene{ nullptr };						// dm_scene-node (references other dm_scene

	//dm_subdiv*		subdiv_mesh{nullptr};
	//dm_array<spline*> splines{};
	//dm_sprite*		sprite{nullptr};
	//dm_sdf*			signed_distance_field{nullptr};
	//dm_voxel*			voxels{nullptr};

	dm_array<dm_transform>  sockets_snap_to{};
	dm_array<dm_transform>  sockets_snap_from{};

	// computed values
	dm_transform		bbox;
	dm_instance_type	type{ dm_instance::dm_instance_type::it_bounds_mesh };
	//instance_type getType() { return instance::instance_type::default_box; };
};


struct dm_environment {};

struct dm_joint
{
	dm_string			name{};									// [note][ademets] probably not needed, remove
	dm_vec3				constraints{ 0, 0, 0 };
	dm_mat4				inverse_bind_matrix{ dm_mat4_identity };
	dm_transform		ibt{ dm_transform::Identity };
	dm_transform		ibtm{ dm_mat4_identity };

	dm_bounds			collision_bounds{};
	dm_instance*		skinning_bounds{ nullptr };				// use for auto-skinning (inform bounds of joint, which vertixes to map to, overlaps and so on), can be null
	
	dm_node* parent_joint{ nullptr };
	dm_mat4				offset{ dm_mat4_identity };
	dm_transform		offset_transform{ dm_transform::Identity };
	
	// references
	dm_node*			_owning_node{ nullptr };				// [note][ademets] do we need that?
	//dm_instance*		bone_geometry;							// [note][ademets] maybe cleaner accessed with owning_node->instance, instead of local pointer?
	void*				_ozz_joint{ nullptr };

	// computed/helper - only here for easier access, maybe make getter, and setter that automatically iterate through sections
	dm_array<dm_primitive*>		_primitive_sections{};			// in unordered mode, primitives are not sorted by joint pairs, so one joint can incluence multiple sections
	dm_array<dm_array<uint32>>	_vertex_ids_in_section{};
	dm_array<dm_array<float>>	_weights_in_section{};
	dm_color					_color{};

	// [note][ademets]
	// right now use primitive* as reference, think about using ids or handler, once we 
	// allow for joint manipulation of splines, voxels, sdfs,...
	// dm_array<section_id> influenced_section; (section_id == uint32)
};

struct dm_skin;
struct dm_node
{
	enum dm_node_type {
		nt_node			= 'NODE',
		nt_camera		= 'CMRA',
		nt_instance		= 'INST',
		nt_skinned_instance = 'SKIT',
		nt_joint		= 'JONT',
		nt_skin			= 'SKIN',
		nt_environment	= 'ENVM',
		nt_scene_node	= 'SCEN',
	};

	dm_string	 		name{};
	dm_transform		transform{ dm_transform_IDENTITY };
	dm_node*			parent{ nullptr };

	// data
	//dm_scene*			scene{ nullptr };
	dm_camera*			camera{ nullptr };
	dm_instance*		instance{ nullptr };
	dm_joint*			joint{ nullptr };
	dm_skin*			skin{ nullptr };
	dm_environment*		environment{ nullptr };

	// computed
	dm_array<dm_node*>	children{};
	dm_node_type		type{ dm_node_type::nt_node };

	// helper
	dm_string _parent_name{};	// used by dm_fix_node_hierachy(_scene), to fix hierarchy in case node creation was out of order
};

struct dm_animation
{
	enum struct dm_animation_type { linear, step, bezier };
	//animation_type				type = animation_type::linear;

	dm_string name{};
	dm_string uri{};		// gltf buffer file

	double length{ 0.0f };
	double frames_per_second{ 30 };
	int frames{ -1 };

	template<typename T>
	struct dm_sampler {
		enum struct dm_interpolation_mode { linear, step, bezier };
		dm_array<float> in{};
		dm_array<T> out{};
		dm_interpolation_mode mode{ dm_interpolation_mode::linear };
	};
	template<typename t_target_type, typename t_value_type>
	struct dm_track {
		enum struct dm_track_type { translation, rotation, scale, weight, custom };
		t_target_type target{ nullptr };
		dm_sampler<t_value_type> values{};
		dm_track_type type{ dm_track_type::custom };
		//dm_string type_path{};	// used as parsing info, if type custom (e.g. "material/secondary_color"
		//buffer *buf;
		dm_track(dm_track_type type_) : type(type_) {};
	};
	typedef dm_track<dm_node*, dm_vec3> dm_node_translation_track;
	typedef dm_track<dm_node*, dm_vec3> dm_node_scale_track;
	typedef dm_track<dm_node*, dm_quat> dm_node_rotation_track;

	// node animations
	dm_array<dm_node_translation_track> node_translation_tracks{};
	dm_array<dm_node_scale_track> node_scale_tracks{};
	dm_array<dm_node_rotation_track> node_rotation_tracks{};

	// mesh animations
	// animated-mesh
	typedef dm_track<dm_instance*, dm_mesh*> dm_animated_mesh_track;
	dm_array<dm_animated_mesh_track> dm_animated_mesh_tracks{};
	// morphed-mesh
	typedef dm_track<dm_mesh*, dm_primitive> morphed_mesh_track;
	dm_array<morphed_mesh_track> dm_morphed_mesh_tracks{};
	// ...

	// general animation system:
	// - create handle system (dm_node_handle, dm_mesh_handle, dm_material_handle
	// - bind handle parameter to track
	// dm_track<dm_xxx_handle, "param_name", t_value_type>
	// dm_track<dm_xxx_handle, t_value_type> track_xxx("param_name_from_handle") -- want to change color of dm_material
	// parameter_tracks.add(dm_track<dm_material_handle, dm_color>(".color"))


	// material animations
	// color
	// ..params

	// spline animations
	// ...

	// voxel animations
	// ...

	// user channel animations
	// typedef track<dm_string, bool> bool_user_track;	// string: id (example: nodes/"root"/scripts/s"jump"/)
	// dm_array<bool_user_track> bool_user_tracks;

	// callback functions
	// ...

	// computed helper data
	void* _ozz_animation{ nullptr };
	void* _ozz_skeleton{ nullptr };
	//skin* _skin{ nullptr };

};

struct dm_command {
	dm_string		name{ "" };
	uint32_t		name_hashed{ 0 };	// generate from name
	dm_string		params{ "" };
	dm_array<uint8> params_data{};
	uint64_t		cmd_edit_number;	// compare with current scene_edit_number

	// computed/runtime
	bool			_is_cmd_applied_to_scene;	// ex: add_cube(...) -> is already applied to dm_scene, or not yet
};



struct dm_skin
{
	dm_string name{};
	dm_array<dm_node*> joints{};
	dm_array<dm_transform> joint_offsets{};	// applies offset to corresponding joint by index in joints array, use to create skin variations (big hands, long feet, armor displacements..)
	// computed	// [note][ademets] not sure if computed, or needs to be serialized
	dm_array<dm_mat4> inverse_bind_matrices{};

	void* _ozz_skeleton{ nullptr };
	//std::unique_ptr<ozz_skeleton> _ozz_skeleton_up;

	dm_skin() {};
};

struct dm_scene_meta_info;
struct dm_scene_state;

struct dm_ozz_data;

struct dm_scene
{
	//dm_scene_meta_info		scene_info;

	dm_ozz_data* ozz_data{ nullptr };

	dm_string				name{};
	dm_array<dm_camera*>	cameras{};
	dm_array<dm_mesh*>		meshes{};
	dm_array<dm_instance*>	instances{};
	dm_array<dm_material*>	materials{};
	dm_array<dm_texture*>	textures{};
	dm_array<dm_skin*>		skins{};	// [note][ademets] need to serialize / read
	dm_array<dm_joint*>		joints{};

	dm_array<dm_node*>		nodes{};	// node hierarchy, optional, if empty dm_scene will show all, otherwise only show tree in array
	dm_array<dm_animation*> animations{};	// animations, optional
	dm_array<dm_scene*>		scenes{};	// referenced scenes

	dm_array<dm_command*>	cmds{};	// format: json {"cmds": [{ "name":"add_mesh", "params":"{...}", "timestamp":(date) }]}
	//dm_array<script*>		scripts{};

	// computed
	dm_vec3					_bounding_box;
	dm_string				_filepath{};
	dm_string				_filename{};
	//dm_scene_state		_app_scene_state;

	template<typename T>
	struct edited_entity_record { enum struct edit_type { create, read, update, del } type; T* entity{ nullptr }; uint64 edit_index; T* old_entity{ nullptr }; dm_command* cmd{ nullptr }; };
	dm_array<edited_entity_record<dm_node*>> _node_edits;
	dm_array<edited_entity_record<dm_mesh*>> _mesh_edits;
	dm_array<edited_entity_record<dm_instance*>> _instance_edits;
};

struct dm_scene_meta_info 
{
	dm_string author{ "" };
	dm_string license{ "cc" };
	dm_string author_eth_address{ "" };
	dm_string uri{ "" };
	uint64	  creation_date{ 0 };
	uint64    last_edit_data{ 0 };
	uint32    version{ 0 }; // +1 for each remix 
	dm_string parent_uri{ "" };
	dm_vec3	  bounding_box;
	uint64    edit_count{ 0 }; // each edit should increase this number, also frame tick - can be used for syncing

	// use for preview data
	dm_string preview_animation_name{ "" };
	float     preview_animation_time{ 0.0f };
	dm_vec3   preview_bounding_box;

};

struct dm_scene_state 
{
	enum struct dm_scene_mode { none, at_start, playing, stopped, editing };

	dm_string				_app_name{ "" };
	void* _app_data{ nullptr };

	dm_scene_mode			_mode{ dm_scene_mode::none };	// at_start: frame 0, time 0
	int						_current_frame_number{ 0 };
	double					_current_frame_time{ 0.0 };			// interesting if between frames in animation
	dm_array<dm_animation*> _current_animations{};

	bool is_playing() { return _mode == dm_scene_mode::playing ? true : false; }
	bool is_at_start() { return !is_playing() && _current_frame_number == 0; }
	bool is_stopped() { return _mode == dm_scene_mode::stopped ? true : false; }
	bool is_editing() { return _mode == dm_scene_mode::editing ? true : false; }
};

// for grouping nodes (like mesh groups, spline groups, skeleton node,...)
// ex. mesh select: click once to select, select again to select whole group,
//                  multiple selects > group selection / ungroup
struct dm_node_group
{
	dm_string group_id{};
	dm_array<dm_node*> nodes{};
};
struct dm_animation_group
{
	dm_string			  name{};
	dm_string			  path{};
	dm_array<dm_animation* > animations{};
};
// group of scenes, would edit in higher level, if transform gets edited for dm_scene, it actually transforms the base node
struct dm_scene_group
{
	dm_string			group_name{};
	dm_array<dm_scene*>	scenes{};
	//dm_array<node*>	root_nodes;
};

// clang-format on
