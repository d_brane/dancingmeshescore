#pragma once

#include "dm_string.h"
#include "dm_container.h"


inline bool dm_does_file_exist(const dm_string& filepath) { return false; };
inline bool dm_does_folder_exist(const dm_string& path) { return false; };

inline bool dm_create_file(const dm_string& filepath) { return false; };
inline bool dm_create_folder(const dm_string& path) { return false; };

struct dm_path_entry {
	dm_string name{};
	dm_string group{};
	dm_string dir{};
	bool isAbsolute{ true };
};

inline bool dm_does_path_entry_exist(const dm_path_entry& entry) { return false; }

struct dm_app_config {
	dm_array<dm_path_entry> system_paths{};
	dm_array<dm_path_entry> import_paths{};
	dm_array<dm_path_entry> export_paths{};

	// computed
	dm_array<dm_string> files{};
	dm_map<dm_string, dm_array<dm_string>> grouped_files{};	// dmmap<group, filepath>
};

dm_app_config dm_setup_app_config(dm_string config_file);

