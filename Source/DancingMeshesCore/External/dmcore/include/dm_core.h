// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_core_types.h"
#include "dm_status.h"


inline dm_status dm_initialize(const dm_initialize_params* params) { return dm_status(); };
inline dm_status dm_shutdown() { return dm_status(); };

inline dm_platform* dm_platform_create(dm_platform_params params, dm_status& status) { return nullptr; };	// register bridge with dmcore
inline dm_status    dm_platform_release(dm_platform* platform) { return dm_status(); };
inline dm_status	dm_platform_tick(dm_platform* platform, float delta_time) { return dm_status(); };

//------------------------------------------------------------------------//
// SCENE FUNCTIONS
//------------------------------------------------------------------------//

struct dm_scene_update_params { float delta_time; };
dm_status dm_scene_update(dm_scene* scene, dm_scene_update_params* params);	// tick

struct dm_scene_from_json_params { dm_string& json_str; bool merge_in{ true }; };
dm_status dm_scene_from_json(dm_scene* scene, dm_string& str, bool merge_in);

inline dm_vec3 dm_scene_get_bounds(const dm_scene* scene) { return scene->_bounding_box; };

int dm_scene_get_meshes(dm_scene* scene, dm_mesh_handle* meshes);
//	int dm_scene_get_cameras(dm_camera_handle* cameras);
//	int dm_scene_get_instances(dm_instance_handle* instances);
//	int dm_scene_get_nodes(dm_node_handle* nodes);

inline dm_scene* dmc_create_scene(dm_context* ctx) { return nullptr; };

/////////////////////////
// node helper
dm_node* dm_node_create(const dm_string& name, dm_transform transform_, dm_node* parent = nullptr);	// :)
dm_node* dm_node_create(const dm_string& name, dm_transform transform_, dm_string &parentId);	// :)
dm_node* dm_node_copy(dm_node* node_);	// :)
void     dm_node_delete(dm_node*& node_, bool delete_children);	// :)
void     dm_node_clone(dm_node* in_node, dm_node* out_node, bool clone_children);	// :)

bool     dm_node_compare(const dm_node* node_a, const dm_node* node_b);

void     dm_node_move_relative(dm_node* joint, dm_transform delta_transform);	// :/
//	void	 dm_node_move_absolute(dm_node* node, dm_transform transform);

enum struct dm_flip_axis { None, X, Y, Z, XY, XZ, YZ };
void     dm_node_flip(dm_node* node_, dm_flip_axis flip_axis_, bool flip_child_nodes);	// :(
void     dm_node_mirror(dm_node* node_, dm_flip_axis flip_axis_, bool flip_child_nodes);	// :(
void     dm_node_reparent(dm_node* node_, dm_node* new_parent);	// :)
void     dm_node_normalize_scale(dm_node* node_, bool normalize_child_nodes);	// :(

dm_string          dm_node_array_to_string_verbose(const dm_array<dm_node*> nodes);
dm_array<dm_node*> dm_node_array_get_roots(const dm_array<dm_node*> nodes);
dm_array<dm_node*> dm_node_array_sort_depth_first(const dm_array<dm_node*>& nodes);


///////////////////////
// nodes -> dm_scene

void dm_scene_add_node(dm_scene* scene_, dm_node* node_ /*, bool add_children_nodes */);	// :)
//	void update_node(dm_scene* scene_, node* node_);
void dm_scene_remove_node(dm_scene* scene_, dm_node*& node_);	// :/ (works, but see comments about return value)
//	void reparent_node(dm_scene* scene_, node* node_, node* new_parent);
void dm_scene_clone_node(dm_scene* scene_, dm_node* in_node, dm_node* out_node, bool clone_children);	// :)
//	void normalize_node_scales(dm_scene* scene_, node* node_, bool normalize_child_nodes) {};
/// use node::_parent_name to create and fixup hierachy - useful in case where node was created before its parent, run as postprocess
bool dm_scene_fix_node_hierarchy(dm_scene* scn, bool flushParentIds, bool recomputeChildrenPointer);	// :)

///////////////////////
// joints -> dm_scene
struct dm_scene_add_joint_params { 
	dm_string name{}; 
	dm_transform transform{ dm_transform_IDENTITY };
	dm_node* parent{ nullptr };
	dm_instance::dm_instance_type type{ dm_instance::dm_instance_type::default_box };
	dm_transform instance_transform{ dm_transform_IDENTITY };
};
dm_node* dm_scene_add_joint(dm_scene* scene, dm_scene_add_joint_params params);

void dm_scene_primitive_apply_weights_from_nodes(dm_scene* scene, dm_node*& joint_node, bool clear_weights_in_joint_when_done = false);
void dm_scene_node_calculate_bounds(dm_scene* scene_, dm_node*& joint_node, bool recalculate_existing_bounds);

//void dm_scene_add_joint(dm_scene* scene_, dm_node* joint, dm_instance::dm_instance_type type);
//void dm_scene_add_joint(dm_scene* scene_, dm_node* joint) {}; // if name conflict, automatically adds _XX to name/id
//void dm_scene_add_joint(dm_scene* scene_, dm_transform transform_, node* parent, node* out_joint) {};  

//void remove_joint(dm_scene* scene_, dm_node* joint) {};
//void remove_joint(dm_scene* scene_, dm_string& joint_id) {};

//void normalize_joint_scale(dm_scene* scene_, dm_node* joint, bool reparent_child_joints)

//void reparent_joint(dm_scene* scene_, dm_node* joint, dm_node* new_parent) {};
//void reparent_joint(dm_scene* scene_, dm_node* joint, dm_string& new_parent_id) {};
//void reparent_joint(dm_scene* scene_, dm_string& joint_id, dm_node* new_parent) {};
//void reparent_joint(dm_scene* scene_, dm_string& joint_id, dm_string& new_parent_id) {};

//void clone_joint(dm_scene* scene_, const dm_node* in_joint, dm_node* out_joint, bool clone_children) {};

//void normalize_joint_scale(dm_scene* scene_, dm_node* in_joint, dm_node* out_joint, bool update_children) {};

///////////////////////
// dm_scene helper

dm_transform dm_transform_compute_relative_between(const dm_node* node_, const dm_node* parent_);	// :)

enum scene_reference_frame { lhs_y_up, rhs_z_up };
dm_scene* dm_scene_change_reference_frame(scene_reference_frame from, scene_reference_frame to, dm_scene* scene_in);	// :(
dm_transform dm_node_relative_to_local_transform(const dm_node* node_);	// :(
dm_transform dm_node_relative_to_world_transform(const dm_scene* scene_, const dm_node* node_);	// :(



void dm_scene_recalculate_node_children(dm_scene* scene_);	// :)

void dm_scene_recalculate_joint_bounds(dm_scene* dmscene);	// :/


inline uint32_t dm_scene_sizeof_simple(dm_scene* scene) { return sizeof(scene); }

uint32_t dm_scene_sizeof(dm_scene* scene);
	
void dm_scene_recalculate_inverse_bind_matrices(dm_scene* dmscene, dm_skin* skin, dm_node* mesh_node);

//[todo][alex] add dm_scene_fixup_skins() -- merge/seperate/add missing nodes/combine to single root (bool)
void dm_scene_merge_skins(dm_scene* dmscene);	// :/
	
struct dm_scene_fixup_skins_params { bool merge_skins{ true }; bool sort_skin_nodes{ true }; bool add_missing_nodes{ true }; bool combine_root_nodes{ true }; bool remove_control_bones{ true }; };
void dm_scene_fixup_skins(dm_scene* dmscene, dm_scene_fixup_skins_params params);




// ways to interface with references to data
// for example: - joint holding mesh reference
//              - node hierarchy (parent -> child relationship)
//
// 1) hold pointer
//    + super fast
//    - harder to keep safe; what happens when address changes because it was deleted and duplicated?
// 2) name/id .. as string
//    + easy 
//    + serializes well
//    - name conflicts
//    - slower
// 3) id/hash .. as guid/int
//    + quicker
//    - slower than pointers
//    - messy serialized results (maybe use name when serializing?, see usd)
//    - extra code
// 4) tuple<id/hash/name, pointer>
//    - more code, messier
//    - more syncing necessary?
//    + flexibility?
// 5) direct in-memory, minimize pointers in structs
//    + super save, functional friendly
//    - memory requirements probably higher, if not optimized in large scenes (maybe could use instancing in mesh data to optimize)
//    - slower, maybe a lot in large scenes
//    - sometimes not possible; ex.: node hierachy will still be handled as references via (1/2/3) or (4) (parent node -> child node relationship)
// 6) index-based using flat hierarchies (see gltf)
//    + index maps to point in array
//    - can easy brake when editing dm_scene a lot (need good implementation)
//    = use fixed virtual memory pool and split into pages for node data, mesh data, ...
//      each newly created node/mesh/animation/... maps to new space in virtual memory
//      so no wrong indices ever, no compress for cleanup after a while (gc for dead objects, that are marked for del) and redraw/remap dm_scene
//      -> custom space management neccessary, but kinda neat solution
//      -> optimize() function could swiffel data for ideal caching access, cleanup waste
//
// enum handler_mode { use_address, use_index };
// struct handler<T> { T* adr; uint32 index }; 
// typedef mesh_handler handler<mesh>;
// typedef node_handler handler<node>;
// T* get<T>(dm_array<handle<T>>& arr, handler handle, handler_mode)
// mesh* get_mesh(dm_array<mesh_handle>& meshes, uint index)
// mesh* get_mesh(dm_array<mesh_handle>& meshes, dm_string name)
// mesh* get_mesh(dm_array<mesh_handle>& meshes, ...)
//
// update_handler_indices(dm_scene& scene, dm_array<handle<T>>& arr);	// look, if indices still corect, otherwise update



// ----
// stm implementation
// dm_handle* dm_stm_register_var(dm_var_type var_type);
// dm_handle** dm_stm_register_var_array(dm_var_type type, uint32 num_of_elements);
// dm_handle** dm_stm_register_var_map(dm_var_type key, dm_var_type values, uint32 num_of_elements);
//
//
//struct dm_xxx_handle {
//	enum struct dm_accessor_type : uint8 { read_only, write_only, read_write } accesor_type;
//	enum struct dm_type_info : uint8 { stm_int, stm_float, stm_double, stm_node, stm_mesh, stm_texture, stm_material, stm_generic_struct } type; // stm_int_array,...?
//	bool is_array{ false };
//	size_t num_of_elements{ 0 };
//	uint64 hash_id;
//	void* payload{ nullptr };
//	//void** payload_array{ nullptr };
// };
// think about fat-pointers (http://libcello.org/learn/a-fat-pointer-library)
// before each pointer handle is the type saved, num of elements (if applicable), can probably be combined into 1 64bit var. - 8 bit for type info, 56 bit for num_of_elements
// this is for native types (int, float, double, int_array, float_array, double_array, mesh, texture, material)
//
// case: custom datastructs
// problem: typical datastructures are not just simple ints, doubles, ... but a mix of different vars
//          therefore a solution for generic data structs has to exist
//          for all native types the solution using simple metadata is sufficient, and 64 bit is more than enough to provide it
//          not so for complex generic data structures
// example:
// struct car { int num_of_doors; int num_of_ps; float current_gas_levels; float max_gas_levels; string name; ... }
// 
// metadata here would be a lot bigger. now we can assume that where there is own-there are many.
// so instead of packaging this up as a generic struct (AoS), we package it as a SoA.
// this keeps the metadata smaller and neglectible.
// 
// dm_stm_register_struct_begin(dm_string("car"), num_of_elements)
// dm_stm_register_struct_int(dm_string("num_of_doors", 4)
// dm_stm_register_float_array("passengers_heights", )
// dm_stm_register_reference_array("passenger", "passengers", 5);
// dm_stm_register_int(...)
// dm_stm_register_mesh(...)
// dm_stm_register_double_array(...)
// dm-stm_register_double(...)
// dm_stm_register_struct_end()
// 
// dm_stm_int_handle dm_stm_register_int
// dm_stm_float_handle dm_stm_register_float
// dm_stm_double_handle dm_stm_register_double
// dm_stm_char_handle dm_stm_register_char
// dm_stm_string_handle dm_stm_register_string
// dm_stm_int_array_handle dm_stm_register_int_array
// dm_stm_float_array_handle dm_stm_register_float_array
// dm_stm_double_array_handle dm_stm_register_double_array
// 
// native structs:
// 
// dm_node_handle dm_stm_register_node
// dm_mesh_handle dm_stm_register_mesh
// dm_instance_handle dm_stm_register_instance
// dm_material_handle dm_stm_register_material
// dm_texture_handle dm_stm_register_texture
// 
// dm_stm_release_node(dm_node_handle handle)
// dm_stm_release_xxx(dm_xxx_handle handle)


	// interface for CAMUD API
	// create: create new object, update all references/handles/index manager
	// access: passively read, get access to options
	// modify: change value from local cmd, mostly one parameter at a time - might call update to dispatch to others
	// update: change value from external cmd, or full object (ex: pushed via network, hand modified struct)
	//         update functions for references/handles/indices/...
	//         might use modify to apply update
	// delete: deletes said object/free the memory, handle all the references and index manager

	// musings: simple c style library - with dll/lib as implementation
	//            vs
	//          c interface style library - structs that hold func pointers and global var for access & minimal state
	// c interface can be used to create arrays of backend calls for pipelines natively (ex: 'camera_in+ml->scene' && 'scene->unreal&&net&&json')
	// unfortunately added complexity and easier possibility of getting lazy to save state, but positive is c compability & no hacks for pipelines

	//struct dm_camud_api_interface {

	// generic handle
	//	//template<class T>
	//	struct dm_handle {
	//		//T* handle_ptr;
	//		void* handle;
	//		dm_status status;
	//		bool is_valid() { return (handle != nullptr && status.is_ok()) ? true : false; };

	//		template<class T>
	//		T* handle_typed() { return (T*)handle };
	//	};

	//	using T = void*; // using T = dm_handle;
	//	using dm_scene_handle = T;  // using dm_scene_handle = dm_handle<dm_scene>;
	//	using dm_scene_handle = T;
	//	using dm_node_handle = T;
	//	using dm_mesh_handle = T;
	//	using dm_joint_handle = T;
	//	using dm_texture_handle = T;
	//	struct dm_create_options { bool auto_generate_name_on_collision = true; dm_string name_appendix = ".###"; };

	//	dm_scene_handle create_scene(void*& scene, dm_create_options options, dm_status& status_out);
	//	dm_scene_handle create_scene_from_scene(void*& created_scene, dm_scene* from_scene);
	//	dm_scene_handle access_root_scene();
	//	dm_scene_handle access_scene_by_name(dm_scene* scene, dm_string& scene_name);
	//	dm_scene_handle access_scene_by_id(int id);
	//	dm_scene_handle access_scene_by_hash(uint64 hash);
	//	dm_status		access_scene_by_query(dm_scene* scene, dm_scene_query_params params, void* query_cb_func, void* query_cb_func_params); 
	//	dm_status		modify_scene_name(dm_scene* scene, dm_string& scene_name);
	//	dm_status		modify_scene_parent(dm_scene* scene, dm_scene*& scene_parent);
	//	dm_status		update_scene(dm_scene* scene);
	//	dm_status		delete_scene(dm_scene* scene);

	//	dm_node_handle	create_node(dm_scene*& scene);
	//	dm_node_handle	access_node_by_name(dm_string& name);
	//	dm_node_handle	access_node_by_hash(uint64 hash);
	//	dm_node_handle	access_node_by_id(int id);
	//	dm_status		modify_node();
	//	dm_status		modify_node_name();	// -> in case calls update_node_hash() and update_node_id()
	//	dm_status		modify_node_parent();
	//	dm_status		modify_node_type();
	//	dm_status		update_node(dm_scene* scene, dm_node* node);
	//	dm_status		update_node_name();
	//	dm_status		update_node_hash();
	//	dm_status		update_node_id();
	//	dm_status		delete_node(dm_scene* scene, dm_node* node);

	//	dm_joint_handle create_joint();
	//	dm_joint_handle access_joint_by_name(dm_string& name);
	//	dm_joint_handle access_joint_by_id(int id);
	//	dm_joint_handle access_joint_by_hash(uint64);
	//	dm_status		modify_joint();
	//	dm_status		update_joint();
	//	dm_status		delete_joint();

	//	dm_mesh_handle* create_mesh(dm_scene*& scene, dm_create_options options);
	//	dm_status		modify_mesh_topology(...);
	//	dm_status		update_mesh_topology(...);

	//};

	// dm_add_mesh
	// dm_add_scene_node
	// dm_add_node
	// dm_add_camera
	// dm_add_skin()
	// dm_add_animation
	// dm_
	// dm_get_cameras()
	// dm_get_meshes()
	// dm_create
	// dm_remove
	// dm_access
	// dm_change

	/*
	dm_init();
	//platform = dm_platform_create();
	if (auto result = dm_platform_create()) {
		platform = (dm_platform*)result.value; // c
		platform = result.value; // c++
	}
	//platform.scene = dm_scene_create().value;
	if (result = dm_scene_create()) {
		platform.scene = result.value;
	}

	dm_scene_add_cube(platform.scene, {.scale{100.0f,100.0f,100.0f}, .material={.color=dm_color_RED}});
	scene.add_cube({...});
	scene.addCube(...);
	platform.addCube(...);

	dm_scene_add_node(platform.scene, {.transform={.position={100.0f,0.0f,0.0f}, .rotation={dm_math_PI, 0.0f, 0.0f}}, .name="base"});
	dm_scene_add_cmd(platform.scene, { ... } );

	dm_scene_add_joint_

	while (1) {
		dm_platform_tick(platform, dt);
	}
	dm_platform_shutdown();
	dm_shutdown();
	*/