#pragma once


#include "dm_scene.h"


inline dm_spline dm_spline_from_points(dm_array<dm_vec3>& points) { return dm_spline(); };

inline dm_spline dm_spline_simplify(dm_spline spline) { return dm_spline(); };

struct dm_spline_to_points_params { int num_of_points{ 10 }; };
inline dm_array<dm_vec3> dm_spline_to_points(dm_spline spline, dm_spline_to_points_params params) {};
