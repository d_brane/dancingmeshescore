// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_string.h"
#include "dm_container.h"


//#if (defined(__cplusplus) && __cplusplus >= 201703L) || (defined(_HAS_CXX17) && _HAS_CXX17 == 1)
//#define FEATURE_LEVEL_CPP_17
//#include <string_view>
//#endif

dm_string dm_base64_encode(dm_array<uint8_t> const& bytes);

#if defined(FEATURE_LEVEL_CPP_17) && !defined(DM_RUNTIME_UNREAL)
bool dm_base64_decode(std::string_view in, std::vector<uint8_t>& out)
#else
bool dm_base64_decode(dm_string const& in, dm_array<uint8_t>& out);
#endif
