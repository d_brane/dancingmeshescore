// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#include "dm_scene_types.h"
#include "dm_status.h"

#include <assimp/scene.h>

struct aiScene;

dm_status dm_assimp_load(dm_scene** scene, const dm_string path);
dm_status dm_assimp_save(const dm_scene* scene, const dm_string path);

namespace internal {
	dm_status dm_assimp_create_from_file(const aiScene** assimp_scene, const dm_string file);
	void dm_assimp_release(const aiScene* scene);

	void dm_scene_from_assimp(const aiScene* assimp_, dm_scene* scene_);
	void dm_scene_to_assimp(const dm_scene* scene_, aiScene* assimp_);

	// type conversions
	dm_mat4		 dm_mat4_from_assimp(const aiMatrix4x4& matrixIn);
	dm_vec2		 dm_vec2_from_assimp(const aiVector2D& vecIn);
	dm_vec3		 dm_vec3_from_assimp(const aiVector3D& vecIn);
	dm_quat		 dm_quat_from_assimp(const aiQuaternion& quatIn);
	dm_transform dm_transform_from_assimp(const aiMatrix4x4& matrixIn);
	dm_color	 dm_color_from_assimp(const aiColor4D& colorIn);
	dm_string	 dm_string_from_assimp(const aiString& stringIn);

	dm_node* dm_node_create_from_assimp(const aiNode* Node);
	dm_mesh* dm_mesh_create_from_assimp(const aiMesh* Mesh);
}