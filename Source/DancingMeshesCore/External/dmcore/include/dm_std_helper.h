// Copyright branedev, Inc. All Rights Reserved.
//#pragma once
//
//#include "dm_string.h"
//#include <string>
//
//
//inline std::string dm_string_to_std(const dm_string &str) 
//{
//#ifdef DM_RUNTIME_UNREAL
//	return TCHAR_TO_UTF8(*str);
//#else
//	return std::string(str);
//#endif
//}
//
//inline dm_string dm_string_from_std(const std::string& str)
//{
//#ifdef DM_RUNTIME_UNREAL
//	return str.c_str();
//#else
//	return str;
//#endif
//}