// Copyright branedev, Inc. All Rights Reserved.
#pragma once

#ifdef DM_RUNTIME_UNREAL
#include "Components/SceneComponent.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectGlobals.h"

// AActor::GetDefaultSubobjectByName seems to be broken, so use this custom implementation instead
template<class T> 
inline T* FindComponent(AActor* Actor, const FString& CompName)
{
	auto components = Actor->GetComponents();
	for (auto component : components)
	{
		if (component->GetFName() == (*CompName))
		{
			return Cast<T>(component);
		}
	}
	return nullptr;
}

inline USceneComponent* SpawnNewComponent(AActor* Owner, USceneComponent* ParentComponent, UClass* ComponentClassToSpawn, const FTransform& SpawnLocation, const FString& ComponentName)
{
	//Make sure it is a scene component since we are going to attach it to our root component  
	check(ComponentClassToSpawn->IsChildOf(USceneComponent::StaticClass()));

	// Construct an object of the class passed to us but return a point to USceneComponent  
	USceneComponent* Component = NewObject<USceneComponent>(Owner, ComponentClassToSpawn, (*ComponentName));

	//Attach the component and position it at the location given  
	Component->SetupAttachment(ParentComponent);
	Component->SetRelativeTransform(SpawnLocation);
	//Component->SetWorldTransform(SpawnLocation);
	Component->RegisterComponent();

	return Component;
}
#endif
